--===================================================================================
--andis_6_create_param_silent.sql
--Description     : Script used for silent parameters input
--Version date    : February 26, 2009
--
--===================================================================================
--                               (c) 2009 Bell ID B.V.
--===================================================================================



-- define ANDIS_6 schema
DEFINE ANDIS_6_SCHEMA = 'ANDIS_6';

--tablespaces 

DEFINE tbspace_data  = USERS;
DEFINE tbspace_temp = TEMP;
DEFINE tbspace_index = USERS;
DEFINE logdrive = './logs';
DEFINE connectstring = XE;
DEFINE dba_user = system;
DEFINE dba_password = system;

--*********DO NOT CHANGE THIS BELLOW
-- application user
DEFINE app_user_andis_6 = app_user_&&ANDIS_6_SCHEMA.;
-- define run andis_6 role
DEFINE RUN_ANDIS_6_SCHEMA = run_&&ANDIS_6_SCHEMA.;
--*********