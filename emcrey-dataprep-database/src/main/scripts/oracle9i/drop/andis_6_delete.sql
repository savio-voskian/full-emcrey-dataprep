--===================================================================================
--
--Description     : Script to insert conguration data for ANDiS 6 database 
--
--Version date    : April 16, 2009
--
--===================================================================================
--                               (c) 2009 Bell ID B.V.
--===================================================================================


SPOOL &&logdrive/andis_se_insert.log;
PROMPT ===========================================================================================;
PROMPT D E L E T E    A N D I S   6    C O N F I G U R A T I O N;
PROMPT ===========================================================================================;

SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERROR  EXIT 
WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT ;
PROMPT ;
PROMPT ==================================================================================;
PROMPT D R O P    A N D I S    6    D A T A B A S E;
PROMPT ==================================================================================;
PROMPT ;


-- define default ANDIS_6 schema for interactive input 

DEFINE ANDIS_6_SCHEMA = 'ANDIS_6';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line 
--*********************************************************************************** 
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1; 


--**********************************************************************************
-- start creating database objects 
--*********************************************************************************** 

SPOOL &&logdrive/andis_6_delete.log;
CONNECT &&dba_user/&&dba_password@&&connectstring;
ALTER SESSION SET CURRENT_SCHEMA = &&ANDIS_6_SCHEMA.;

delete from CAMS_VARIABLE_VALUE;
delete from CAMS_VARIABLE_CONFIG;
delete from CAMS_STATE_TRANSITION;
delete from CAMS_RECORD_CODE_TABLE;
delete from CMN_LICENSE;
delete from CAMS_DEFINITION_VARIABLE;
delete from CAMS_TYPE_CODE_TABLE;
delete from CMN_DATABASE_VERSION;
delete from CAMS_C_TYPE_VARIABLE;
delete from CAMS_C_STATE;
delete from CAMS_C_LIFECYCLE;
delete from CAMS_C_TYPE_DATA;
delete from CAMS_C_EVENT;
delete from CMN_BLOB;
delete from CAMS_ADDON_FILE;
delete from CAMS_C_TYPE_ADDON_FILE;
delete from CMN_X_USER_IN_METHOD;
delete from CMN_X_USER_IN_GROUP;
delete from CMN_X_METHOD_IN_SOURCE;
delete from CMN_X_GROUP_IN_ROLE;
delete from CMN_X_ACTION_IN_ROLE;
delete from CMN_ROLE;
delete from CMN_PASSWORD_HISTORY;
delete from CMN_GROUP_IN_SOURCE;
delete from CMN_GROUP;
delete from CMN_C_TYPE_AUTH_SOURCE;
delete from CMN_AUTH_METHOD;
delete from CMN_TABLE_NAME;
delete from CMN_USER;
delete from CMN_AUTH_SOURCE;
delete from CMN_ACTION;
delete from CMN_LANGUAGE;
DELETE FROM cmn_trace_entry;
DELETE FROM cmn_trace_entry_type;

PROMPT ===========================================================================================;
PROMPT E N D    D E L E T E    A N D I S    6     C O N F I G U R A T I O N;
PROMPT ===========================================================================================;
PROMPT;
spool off;
exit;

