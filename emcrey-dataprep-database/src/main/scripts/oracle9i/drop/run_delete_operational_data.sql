/*
This script is a development/test utility to delete operational data from the database.
The script relies on referential integrity constraints and the starting points shown at the end of the script.
It deletes data in reverse order by traversing the dependency tree.
WARNING: 
1) This script is only suitable for test/development environments it should not be used in production
or where there are large volumes of data.
2) Because the object relational mapping framework Hibernate assumes the only thing to impact the database is itself, it
may be necessary to restart java applications to ensure a clean system.

Usage:
  To run this script from SQL Plus you will need a parameter file that describes your database (schema names, passwords etc)
  WARNING: Please check this file before running to make sure you're changing the correct database!
    sqlplus /nolog @run_delete_operational_data.sql <parameter file>
  For example:
    sqlplus /nolog @run_delete_operational_data.sql ..\create\r6_create_param_silent.sql
  */
SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERROR  EXIT 
WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT ;
PROMPT ;
PROMPT ==================================================================================;
PROMPT DELETE OPERATIONAL DATA;
PROMPT ==================================================================================;
PROMPT ;


-- define default BELLID_R6_SCHEMA schema for interactive input 

DEFINE BELLID_R6_SCHEMA = 'TO BE PROVIDED FROM PARAMETERS';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line 
--*********************************************************************************** 
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1; 

SPOOL &&logdrive/run_delete_operational_data.log;

@@../create/connect_dba_user.sql

WHENEVER SQLERROR CONTINUE

SET SERVEROUTPUT ON


prompt
prompt Running delete script;
prompt ===========================
prompt

@@delete_operational_data.sql

prompt
prompt ACTION NEEDED:;
prompt Please review script output tables in &&logdrive/run_delete_operational_data.log. 
prompt a) If there is anything you did not expect to be deleted, answer N to the next question;
prompt b) If you are happy with the output answer Y to the next question
prompt ===========================
prompt
SPOOL OFF;

accept happyToCommit prompt 'Are you happy to commit Y/N?:'

@@commit_or_rollback.sql

PROMPT ;
PROMPT ;
PROMPT ==================================================================================;
PROMPT END DELETE OPERATIONAL DATA;
PROMPT ==================================================================================;
PROMPT ;
exit;
