/*
===================================================================================
r6_create_param.sql
Description     : Script to create and prepare Release 6 database 

Version date    : February 26, 2009

===================================================================================
                               (c) 2009 Bell ID B.V.
===================================================================================
*/




PROMPT ;
PROMPT -> If a default value has been set, then it is shown like [default_value].;
PROMPT -> Hit enter to use the default.;
PROMPT -> Enter path names without the last slash, like C:/ORACLE;
PROMPT ;
PROMPT Current location:
HOST cd;
ACCEPT logdrive        CHAR  DEFAULT 'C:\Oracle'		PROMPT '> Enter path where log files should be placed [C:\Oracle]:     '
PROMPT ;
ACCEPT connectstring   CHAR  DEFAULT 'CAMS'				PROMPT '> Enter name of database alias [CAMS]:                         '
ACCEPT dba_user        CHAR  DEFAULT 'DBA_USER'			PROMPT '> Enter user name of database administrator [DBA_USER]:        '
ACCEPT dba_password    CHAR  DEFAULT 'USER_DBA'			PROMPT '> Enter password of database administrator [USER_DBA]: ' HIDE

PROMPT ;
ACCEPT BELLID_R6_SCHEMA  CHAR  DEFAULT 'bellid_r6_xxx'	PROMPT '> Enter name of application schema [bellid_r6_xxx]:            '
PROMPT ;
ACCEPT BELLID_R6_SCHEMA_PASSWORD CHAR DEFAULT '&BELLID_R6_SCHEMA'	PROMPT '> Enter password of application schema [&BELLID_R6_SCHEMA]: ' HIDE
PROMPT ;
ACCEPT BELLID_R6_APP_USER_PASSWORD CHAR DEFAULT 'app_user_&BELLID_R6_SCHEMA'	PROMPT '> Enter password of application run schema [app_user_&BELLID_R6_SCHEMA]: ' HIDE
PROMPT ;
ACCEPT BELLID_R6_MI_USER_PASSWORD CHAR DEFAULT 'mi_user_&BELLID_R6_SCHEMA'	PROMPT '> Enter password of MI user schema [mi_user_&BELLID_R6_SCHEMA]: ' HIDE
PROMPT ;
ACCEPT OVERRIDE_SOLUTION_FLAVOURS  CHAR 				PROMPT '> Enter comma-separated list of solution flavours (will override the default) [DEFAULT]:'

CONNECT &&dba_user/&&dba_password@&&connectstring;
SELECT tablespace_name AS "Current tablespaces" FROM DBA_TABLESPACES;
DISCONNECT;

PROMPT ;
ACCEPT tbspace_data    CHAR  DEFAULT 'SCMS_DATA'		PROMPT '> Enter name of default tablespace [SCMS_DATA]:                '
ACCEPT tbspace_index   CHAR  DEFAULT 'SCMS_INDEX'		PROMPT '> Enter name of index tablespace [SCMS_INDEX]:                 '
ACCEPT tbspace_temp    CHAR  DEFAULT 'TEMP'				PROMPT '> Enter name of temporary tablespace [TEMP]:                   '
PROMPT ;
PROMPT ;
PAUSE Hit enter to create the Release 6 ENGINE database...;

--*********DO NOT CHANGE THIS BELOW
-- application user
DEFINE BELLID_R6_APP_USER = app_user_&&BELLID_R6_SCHEMA.;
-- management information user
DEFINE BELLID_R6_MI_USER = mi_user_&&BELLID_R6_SCHEMA.;
-- define run application role
DEFINE RUN_BELLID_R6_SCHEMA = run_&&BELLID_R6_SCHEMA.;
-- define run management information role
DEFINE READ_BELLID_R6_MI = miread_&&BELLID_R6_SCHEMA.;
--*********