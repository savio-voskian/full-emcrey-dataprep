SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERROR  EXIT
WHENEVER SQLERROR EXIT SQL.SQLCODE

-- define default schema placeholder
DEFINE BELLID_R6_SCHEMA = 'TO_BE_REPLACED_BY_PARAM';
-- define default password placeholders
DEFINE BELLID_R6_SCHEMA_PASSWORD = '';
DEFINE BELLID_R6_APP_USER_PASSWORD = '';
DEFINE BELLID_R6_MI_USER_PASSWORD = '';
-- define flag to say whether the MI Role is needed. Default is yes. This will be overridden by Jenkins only
DEFINE CREATE_MI_ROLE = 'Y';

-- define placeholder for the list of solution flavours to install. This will override the default settings for the project installation
DEFINE OVERRIDE_SOLUTION_FLAVOURS = 'DEFAULT';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line
--***********************************************************************************
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1;

@@connect_dba_user.sql

SPOOL &&logdrive/run_r6_delkmsop_data.log;

ALTER SESSION SET CURRENT_SCHEMA = &&BELLID_R6_SCHEMA.;

PROMPT ;
PROMPT ================================================================================;
PROMPT START OF REMOVE KMS OPERATIONAL DATA: SEITC DATABASE
PROMPT ================================================================================;
PROMPT ;

DELETE FROM KMS_HISTORYLOG;
DELETE FROM KMS_IOPROCEDURE_APPLICABILITY;
DELETE FROM KMS_IOKEYPROFILE;
DELETE FROM KMS_IOKEYSETPROFILE;
DELETE FROM KMS_IOSITE;
DELETE FROM KMS_KEY;
DELETE FROM KMS_KEYPROFILE;
DELETE FROM KMS_KEYSET;
DELETE FROM KMS_IOPROCEDURE;
DELETE FROM KMS_IODIRECTION;
DELETE FROM KMS_KEYSETPROFILE;
DELETE FROM KMS_KEY_SOURCE;
DELETE FROM KMS_KEY_TYPE;
DELETE FROM KMS_KEY_ROLE;
DELETE FROM KMS_KEY_ALGORITHM;
DELETE FROM KMS_CHAINING;
COMMIT; 

PROMPT ;
PROMPT ===============================================================================;
PROMPT END OF REMOVE KMS OPERATIONAL DATA: SEITC DATABASE
PROMPT ===============================================================================;
PROMPT ;

SPOOL OFF;

EXIT;