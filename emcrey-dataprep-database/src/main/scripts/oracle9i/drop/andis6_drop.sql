SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERROR  EXIT 
WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT ;
PROMPT ;
PROMPT ==================================================================================;
PROMPT D R O P    A N D I S    6    D A T A B A S E;
PROMPT ==================================================================================;
PROMPT ;


-- define default ANDIS_6 schema for interactive input 

DEFINE ANDIS_6_SCHEMA = 'ANDIS_6';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line 
--*********************************************************************************** 
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1; 


--**********************************************************************************
-- start creating database objects 
--*********************************************************************************** 

SPOOL &&logdrive/andis_6_drop.log;
CONNECT &&dba_user/&&dba_password@&&connectstring;
WHENEVER SQLERROR CONTINUE

prompt
prompt Drop user &&app_user_andis_6. 
prompt ===================================================
prompt
DROP USER &&app_user_andis_6. CASCADE;

prompt
prompt Drop user &&ANDIS_6_SCHEMA. and related objects
prompt ===================================================
prompt
DROP USER &&ANDIS_6_SCHEMA. CASCADE;

PROMPT ;
PROMPT ;
PROMPT ==================================================================================;
PROMPT E N D   D R O P    A N D I S    6     D A T A B A S E;
PROMPT ==================================================================================;
PROMPT ;
spool off;
exit;
