SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERROR  EXIT
WHENEVER SQLERROR EXIT SQL.SQLCODE

-- define default schema placeholder
DEFINE BELLID_R6_SCHEMA = 'TO_BE_REPLACED_BY_PARAM';
-- define default password placeholders
DEFINE BELLID_R6_SCHEMA_PASSWORD = '';
DEFINE BELLID_R6_APP_USER_PASSWORD = '';
DEFINE BELLID_R6_MI_USER_PASSWORD = '';
-- define flag to say whether the MI Role is needed. Default is yes. This will be overridden by Jenkins only
DEFINE CREATE_MI_ROLE = 'Y';

-- define placeholder for the list of solution flavours to install. This will override the default settings for the project installation
DEFINE OVERRIDE_SOLUTION_FLAVOURS = 'DEFAULT';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line
--***********************************************************************************
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1;

@@connect_dba_user.sql

SPOOL &&logdrive/run_r6_deleteop_data.log;

ALTER SESSION SET CURRENT_SCHEMA = &&BELLID_R6_SCHEMA.;

PROMPT ;
PROMPT ================================================================================;
PROMPT START OF REMOVE OPERATIONAL DATA: SEITC DATABASE
PROMPT ================================================================================;
PROMPT ;

DELETE FROM SEITC_TRANSACTION_DATA;
DELETE FROM SEITC_KEY_TOKEN_DATA;
DELETE FROM CAMS_FAILED_MESSAGE;
DELETE FROM CAMS_VARIABLE_VALUE WHERE ID_DEFINITION_VARIABLE IN (SELECT ID_DEFINITION_VARIABLE FROM CAMS_DEFINITION_VARIABLE WHERE ID_TABLE_VALUE IN (4, 16, 22, 94, 19));
DELETE FROM CAMS_APPLICATION_AT;
DELETE FROM CAMS_APPLICATION_ON_CARD;
DELETE FROM CAMS_X_CARD_ORGANIZATION;
DELETE FROM CAMS_CARD_AT;
DELETE FROM CAMS_CARD;
DELETE FROM CAMS_PRODUCTION_AT;
DELETE FROM CAMS_CARD_PRODUCTION_ORDER;
DELETE FROM CAMS_REQUEST;
DELETE FROM CAMS_MEMBERSHIP_AT;
DELETE FROM CAMS_MEMBERSHIP;
DELETE FROM CAMS_MEMBERSHIP;
DELETE FROM CAMS_CARDHOLDER_AT;
DELETE FROM CAMS_CARDHOLDER;
DELETE FROM CMN_AUDIT_TRAIL_SIGNATURE;
DELETE FROM CMN_AUDIT_TRAIL_RECORD;
DELETE FROM CMN_AUDIT_TRAIL;
UPDATE CAMS_NUMBERING SET NBR_CURRENT = NBR_FIRST;
COMMIT;

PROMPT ;
PROMPT ===============================================================================;
PROMPT END OF REMOVE OPERATIONAL DATA: SEITC DATABASE
PROMPT ===============================================================================;
PROMPT ;

SPOOL OFF;

EXIT;
