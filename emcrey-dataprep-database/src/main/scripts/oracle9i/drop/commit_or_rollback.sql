BEGIN
  if (UPPER('&&happyToCommit') = 'Y') then
    dbms_output.put_line('commiting the changes...');
    commit;
  else
    dbms_output.put_line('CANCELLED. rolling back the changes...');
    rollback;
  end if;
END;
/