--===================================================================================
--r6_create_param_silent.sql
--Description     : Script used for silent parameters input
--Version date    : February 26, 2009
--
--===================================================================================
--                               (c) 2009 Bell ID B.V.
--===================================================================================



-- define Release 6 schema
DEFINE BELLID_R6_SCHEMA = 'bellid_r6_mobile';

--tablespaces 

DEFINE tbspace_data  = USERS;
DEFINE tbspace_temp = TEMP;
DEFINE tbspace_index = USERS;
DEFINE logdrive = './logs';
DEFINE connectstring = XE;
DEFINE dba_user = system;
DEFINE dba_password = system;

--*********DO NOT CHANGE THIS BELOW
-- application user
DEFINE BELLID_R6_APP_USER = app_user_&&BELLID_R6_SCHEMA.;
-- management information user
DEFINE BELLID_R6_MI_USER = mi_user_&&BELLID_R6_SCHEMA.;
-- define run application role
DEFINE RUN_BELLID_R6_SCHEMA = run_&&BELLID_R6_SCHEMA.;
-- define run management information role
DEFINE READ_BELLID_R6_MI = miread_&&BELLID_R6_SCHEMA.;
--*********
