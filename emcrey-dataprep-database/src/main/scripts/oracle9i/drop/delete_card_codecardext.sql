SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERROR  EXIT
WHENEVER SQLERROR EXIT SQL.SQLCODE

-- define default schema placeholder
DEFINE BELLID_R6_SCHEMA = 'TO_BE_REPLACED_BY_PARAM';
-- define default password placeholders
DEFINE BELLID_R6_SCHEMA_PASSWORD = '';
DEFINE BELLID_R6_APP_USER_PASSWORD = '';
DEFINE BELLID_R6_MI_USER_PASSWORD = '';
-- define flag to say whether the MI Role is needed. Default is yes. This will be overridden by Jenkins only
DEFINE CREATE_MI_ROLE = 'Y';

-- define placeholder for the list of solution flavours to install. This will override the default settings for the project installation
DEFINE OVERRIDE_SOLUTION_FLAVOURS = 'DEFAULT';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line
--***********************************************************************************
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1;

@@connect_dba_user.sql

SPOOL &&logdrive/run_r6_delcard.log;

ACCEPT CODECARD_EXT        CHAR  DEFAULT 'CODECARD_EXT'			PROMPT '> Enter CODE_CARD_EXTERNAL value:        '

PROMPT ;
PROMPT ===============================================================================;
PROMPT START OF DELETE CARD DATA USING CODE CARD: SEITC DATABASE
PROMPT ===============================================================================;
PROMPT ;

ALTER SESSION SET CURRENT_SCHEMA = &&BELLID_R6_SCHEMA.;

DECLARE
  p_codecard       as   varchar2(100 char);
  v_idcardholder   as   number(19,0);
  v_idrequest      as   number(19,0);
  v_idmembership   as   number(19,0);
  v_membership_cnt as   number(19,0);
  v_cardholder_cnt as   number(19,0);
  v_idcard         as   number(19,0);
  v_sql            as   varchar2(1000 char);
BEGIN
   p_codecard := '&&CODECARD_EXT.';
   SELECT distinct a.id INTO v_idcard FROM CAMS_CARD a WHERE a.code_card_external= p_codecard;
   SELECT ID_CARDHOLDER INTO v_idcardholder FROM CAMS_CARD WHERE  ID = v_idcard;
   SELECT ID_REQUEST INTO v_idrequest FROM CAMS_CARD WHERE ID = v_idcard;
   SELECT ID_MEMBERSHIP INTO v_idmembership FROM CAMS_CARD WHERE ID = v_idcard ;
    -- check if cardholder is used by other cards
   SELECT COUNT(ID) INTO v_cardholder_cnt FROM CAMS_CARD  WHERE ID_CARDHOLDER = v_idcardholder;
   -- check if membership is used by other cards
   SELECT COUNT(ID_MEMBERSHIP) INTO v_membership_cnt FROM CAMS_CARD WHERE ID_MEMBERSHIP = v_idmembership;

   --REMOVE TOKEN RECORDS
   DELETE FROM SEITC_TRANSACTION_DATA WHERE KEY_TOKEN_ID IN (SELECT ID FROM SEITC_KEY_TOKEN_DATA WHERE ID_APPLICATION_ON_CARD IN (SELECT ID FROM CAMS_APPLICATION_ON_CARD WHERE ID_CARD = v_idcard));    
   DELETE FROM SEITC_KEY_TOKEN_DATA WHERE ID_APPLICATION_ON_CARD IN (SELECT ID FROM CAMS_APPLICATION_ON_CARD WHERE ID_CARD = v_idcard);
   
   DELETE FROM CAMS_APPLICATION_AT WHERE ID_APPLICATION_ON_CARD IN (SELECT ID FROM CAMS_APPLICATION_ON_CARD WHERE ID_CARD = v_idcard);
   -- REMOVE VARIABLE VALUE CAMS_APPLICATION_ON_CARD
   DELETE FROM CAMS_VARIABLE_VALUE WHERE VALUE_PK_COLUMN IN (SELECT ID FROM CAMS_APPLICATION_ON_CARD
   WHERE  ID_CARD = v_idcard) AND ID_DEFINITION_VARIABLE IN (SELECT ID FROM CAMS_DEFINITION_VARIABLE WHERE ID_TABLE_VALUE = 4);
   -- REMOVE CAMS_APPLICATION_ON_CARD
   DELETE FROM CAMS_APPLICATION_ON_CARD WHERE ID_CARD = v_idcard;
   -- REMOVE CAMS_CARD and related data
   DELETE FROM CAMS_VARIABLE_VALUE WHERE VALUE_PK_COLUMN = v_idcard AND ID_DEFINITION_VARIABLE IN (SELECT ID FROM CAMS_DEFINITION_VARIABLE WHERE ID_TABLE_VALUE = 16);
   -- REMOVE REQUEST
   DELETE FROM CAMS_CARD_AT WHERE ID_CARD =v_idcard;
   DELETE FROM CAMS_CARD WHERE ID = v_idcard;
   DELETE FROM CAMS_REQUEST WHERE ID = v_idrequest;
   -- REMOVE CAMS_MEMBERSHIP data
   IF v_membership_cnt = 1 THEN
     DELETE FROM CAMS_MEMBERSHIP WHERE ID != 1 AND ID = v_idmembership;
   END IF;
   -- REMOVE CAMS_CARDHOLDER and related data
   IF v_cardholder_cnt = 1 THEN
      SELECT COUNT(ID_CARDHOLDER) INTO v_membership_cnt FROM CAMS_MEMBERSHIP WHERE ID_CARDHOLDER = v_idcardholder;
      IF v_membership_cnt=0 THEN
	    DELETE FROM CAMS_VARIABLE_VALUE WHERE VALUE_PK_COLUMN = v_idcardholder AND ID_DEFINITION_VARIABLE IN (SELECT ID FROM CAMS_DEFINITION_VARIABLE WHERE ID_TABLE_VALUE = 22);
		DELETE FROM CAMS_CARDHOLDER_AT WHERE ID_CARDHOLDER=v_idcardholder;
        DELETE FROM CAMS_CARDHOLDER WHERE ID != 1 AND ID=v_idcardholder;
      END IF;
   END IF;
END;
/

COMMIT;

PROMPT ;
PROMPT ===============================================================================;
PROMPT END OF DELETE CARD DATA USING CODE CARD: SEITC DATABASE
PROMPT ===============================================================================;
PROMPT ;

SPOOL OFF;

EXIT;