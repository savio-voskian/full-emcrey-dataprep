--updates AES BELLID_KEK to be AES 32
UPDATE KMS_KEYPROFILE SET LENGTH=32, KEY_ALGORITHM=3 WHERE ID = 7512;

--updates AES keys to be stored under an AES ZMK (limitation of Thales payShield 9000)
UPDATE KMS_KEYPROFILE SET ID_STORAGEKEYPROFILE = 7512  WHERE ID = 7719;

--update PERSO_TK to have the MSK as storage key since its how 000 types are saved (if this changes to be again 00A type then this should be commented out or removed
--UPDATE KMS_KEYPROFILE SET ID_STORAGEKEYPROFILE = 1000 WHERE ID = 7602;
