------------------------------------------------------------------------------------------
--
-- Control file used to load files containing binary data (BLOB files)
-- to the table CMN_ANDIS_BLOB. 
-- See insert_blobdata.sql for usage
------------------------------------------------------------------------------------------
LOAD DATA
APPEND
INTO TABLE CMN_ANDIS_BLOB
FIELDS TERMINATED BY ','
(
 ID "NVL2(:ID, :ID, CMN_ANDIS_BLOB_SEQ.NEXTVAL)",
 GROUP_INDEX,
 MIME,
 file_name FILLER CHAR(100),
 BINARY_DATA LOBFILE (file_name) TERMINATED BY EOF
)
