prompt
prompt   ===================================================
prompt   A C S
prompt   ===================================================
prompt

prompt
prompt   Insert into CMN_ACTION
prompt   ===================================================
prompt
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (1,   'process.SourceManagement',           'Allows starting Sources Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (2,   'process.UserManagement',             'Allows starting User Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (3,   'process.GroupManagement',            'Allows starting Group Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (4,   'process.RoleManagement',             'Allows starting Role Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (5,   'process.ActionManagement',           'Allows starting Action Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (6,   'process.PasswordPolicyManagement',   'Allows starting Password Policy Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (7,   'process.SystemInfoBar',              'Allows viewing to system info bar');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (35,  'process.MethodManagement',           'Allows starting Method Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (9,   'entity.Source.C',                    'Allows Authentication Source Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (10,  'entity.Source.U',                    'Allows Authentication Source Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (11,  'entity.Source.D',                    'Allows Authentication Source Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (12,  'entity.User.C',                      'Allows User Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (13,  'entity.User.U',                      'Allows User Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (14,  'entity.User.D',                      'Allows User Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (15,  'entity.Group.C',                     'Allows Group Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (16,  'entity.Group.U',                     'Allows Group Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (17,  'entity.Group.D',                     'Allows Group Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (18,  'entity.Role.C',                      'Allows Role Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (19,  'entity.Role.U',                      'Allows Role Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (20,  'entity.Role.D',                      'Allows Role Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (21,  'entity.Action.C',                    'Allows Action Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (22,  'entity.Action.U',                    'Allows Action Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (23,  'entity.Action.D',                    'Allows Action Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (24,  'entity.Method.C',                    'Allows Method Reading');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (25,  'entity.Method.U',                    'Allows Method Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (26,  'entity.Method.D',                    'Allows Method Reading');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (27,  'process.SystemRestart',              'Allows System Restart');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (28,  'process.JobMonitor',                 'Allows Job Monitoring/Starting/Stopping');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (29,  'process.JobManagement',              'Allows Job Creation, Configuration and Deletion');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (30,  'process.JobSchedulerManagement',     'Allows Scheduler Starting/Stopping/Pausing');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (31,  'process.NtlmConfiguration',          'Allows NTLM Configuration');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (32,  'entity.ReportDefinition.C',          'Allows Report Definition Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (33,  'entity.ReportDefinition.U',          'Allows Report Definition Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (34,  'entity.ReportDefinition.D',          'Allows Report Definition Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (40,  'entity.PasswordPolicy.C',            'Allows Password Policy Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (41,  'entity.PasswordPolicy.U',            'Allows Password Policy Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (42,  'entity.PasswordPolicy.D',            'Allows Password Policy Deletion');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (100, 'process.KmsRequireLogin',            'Requires user to log when accessing KMS application');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (101, 'process.KmsKeyManagement',           'Allows starting Key Management and KeySet Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (102, 'process.KmsProfileManagement',       'Allows starting KeyProfile Management and KeySetProfile Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (103, 'process.KmsIOProfileManagement',     'Allows starting IOKeyProfile Management and IOKeySetProfile Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (104, 'process.KmsIOSiteManagement',        'Allows starting IOSite Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (105, 'process.KmsIOProcedureManagement',   'Allows starting IOProcedure Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (106, 'process.KmsHistoryLogManagement',    'Allows starting HistoryLog Management perspective');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (107, 'process.KmsConfiguration',           'Allows KMS configuration');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (108, 'process.KmsPkcs11Inspection',        'Allows viewing PKCS11 configuration');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (109, 'process.KmsMSKRotation',             'Allows MSK rotation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (110, 'process.KmsEMVProfilesWizard',       'Allows invocation of EMV prifiles wizard');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (111, 'process.KmsGPISDProfilesWizard',     'Allows invocation of GP ISD wizard');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (112, 'process.KmsGPSSDProfilesWizard',     'Allows invocation of GP SSD wizard');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (113, 'process.MultosProfilesWizard',       'Allows invocation of MULTOS profiles wizard');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (114, 'process.KmsGsm348ProfilesWizard',    'Allows invocation of GSM 348 wizard');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (115, 'process.KmsScp80ProfilesWizard',     'Allows invocation of GP SCP80 wizard');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (120, 'entity.ModifiableKey.C',             'Allows KmsKey Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (121, 'entity.ModifiableKey.U',             'Allows KmsKey Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (122, 'entity.ModifiableKey.D',             'Allows KmsKey Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (123, 'entity.ModifiableKeySet.C',          'Allows KmsKey Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (124, 'entity.ModifiableKeySet.U',          'Allows KmsKey Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (125, 'entity.ModifiableKeySet.D',          'Allows KmsKey Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (126, 'entity.ModifiableKeyProfile.C',      'Allows KeyProfile Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (127, 'entity.ModifiableKeyProfile.U',      'Allows KeyProfile Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (128, 'entity.ModifiableKeyProfile.D',      'Allows KeyProfile Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (129, 'entity.ModifiableKeySetProfile.C',   'Allows KeyProfile Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (130, 'entity.ModifiableKeySetProfile.U',   'Allows KeyProfile Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (131, 'entity.ModifiableKeySetProfile.D',   'Allows KeyProfile Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (132, 'entity.ModifiableIOKeyProfile.C',    'Allows KeyProfile Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (133, 'entity.ModifiableIOKeyProfile.U',    'Allows KeyProfile Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (134, 'entity.ModifiableIOKeyProfile.D',    'Allows KeyProfile Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (135, 'entity.ModifiableIOKeySetProfile.C', 'Allows KeyProfile Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (136, 'entity.ModifiableIOKeySetProfile.U', 'Allows KeyProfile Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (137, 'entity.ModifiableIOKeySetProfile.D', 'Allows KeyProfile Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (138, 'entity.ModifiableIOProcedure.C',     'Allows KeyProfile Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (139, 'entity.ModifiableIOProcedure.U',     'Allows KeyProfile Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (140, 'entity.ModifiableIOProcedure.D',     'Allows KeyProfile Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (141, 'entity.ModifiableIOSite.C',          'Allows KeyProfile Creation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (142, 'entity.ModifiableIOSite.U',          'Allows KeyProfile Update');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (143, 'entity.ModifiableIOSite.D',          'Allows KeyProfile Deleting');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (151, 'entity.ModifiableHistoryLog.U',      'Allows KeyProfile Validation');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (152, 'entity.ModifiableHistoryLog.R',      'Allows KeyProfile Reading');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (160, 'process.iic.Personalize',            'Allows starting personalization process');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (161, 'process.iic.event.CONFIRM',          'Allows firing CONFIRM event if personalization went OK');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (162, 'process.iic.event.RETRY',            'Allows firing RETRY event if personalization needs to be retried');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (163, 'process.iic.event.CANCEL',           'Allows firing CANCEL event if personalization can not be performed');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (164, 'process.iic.EndOfShiftReport',       'Allows starting end of shift report on IIC');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (165, 'process.iic.EndOfDayReport',         'Allows starting end of day report on IIC');
insert into CMN_ACTION (ID, NAME, DESCRIPTION) values (166, 'process.CertificateConfigurationOverviewPage', 'Allows configuration of CA in Certificate Configuration');

INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (167, 'process.iic.Preview',                'If set, allows a user to Pre view card''s');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (168, 'process.iic.ViewAudit.AnyUser',      'If set, allows the user to see the complete audit trail');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (169, 'process.iic.ViewAudit.User',         'If set, allows the user to see the audit trail of card''s he/she issued');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (170, 'process.iicm.view.serversettings',    'If set, allows a user to view the server settings');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (171, 'process.iicm.edit.serversettings',    'If set, allows a user to edit the server settings');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (172, 'process.iicm.view.produnits',         'If set, allows a user to view the production units');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (173, 'process.iicm.edit.produnits',         'If set, allows a user to edit production units');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (174, 'process.iicm.create.produnits',       'If set, allows a user to create a new production unit');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (175, 'process.iicm.delete.produnits',       'If set, allows a user to delete a production unit');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (176, 'process.iicm.view.reportsettings',    'If set, allows a user to view the report settings');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (177, 'process.iicm.edit.reportsettings',    'If set, allows a user to edit the report settings');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (178, 'process.iicm.delete.reportsettings',  'If set, allows a user to delete a report');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (179, 'process.StockCorrectionPage',         'Allows starting StockCorrectionPage');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (180, 'process.iic.event.PRE_CANCEL',        'Allows firing PRE_CANCEL event if personalization can not be performed');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (181, 'process.SetupKmsConfiguration',       'Allows KMS global setup configuration');

INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (182, 'process.ScheduledJobsOverviewPage',   'Allows access to ScheduledJobsOverviewPage');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (183, 'process.ScheduledJobEditPage',        'Allows access to ScheduledJobEditPage');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (184, 'process.HealthcheckConfigurationPage','Allows access to HealthcheckConfigurationPage');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (185, 'process.EnvironmentOverviewPage',     'Allows access to EnvironmentOverviewPage');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (186, 'process.cmnAuditLog',                 'Allows access to history log');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (187, 'process.AppLogFilesOverviewPage',     'Allows access to Application log Configuration');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (188, 'process.LifecycleAppletPage',         'Allows to access LifecycleAppletPage');
INSERT INTO CMN_ACTION (ID, NAME, DESCRIPTION) values (189, 'entity.Lifecycle.C',                  'Allows Lifecycle Creation');

prompt
prompt   Insert into CMN_C_TYPE_AUTH_SOURCE
prompt   ===================================================
prompt
insert into CMN_C_TYPE_AUTH_SOURCE (ID, NAME, DESCRIPTION) values (0, 'ACS', 'Authorisation Control System');
insert into CMN_C_TYPE_AUTH_SOURCE (ID, NAME, DESCRIPTION) values (1, 'LDAP', 'Lightweight Directory Access Protocol');
insert into CMN_C_TYPE_AUTH_SOURCE (ID, NAME, DESCRIPTION) values (3, 'KERBEROS', 'Kerberos Ticketing system');
insert into CMN_C_TYPE_AUTH_SOURCE (ID, NAME, DESCRIPTION) values (4, 'CARDHOLDER', 'Tokenholder Authorisation');
insert into CMN_C_TYPE_AUTH_SOURCE (ID, NAME, DESCRIPTION) values (5, 'JEE', 'JEE External Authentication Type');

prompt
prompt   Insert into CMN_AUTH_SOURCE
prompt   ===================================================
prompt
insert into CMN_AUTH_SOURCE (ID, ID_TYPE_SOURCE, NAME, DESCRIPTION, DT_DELETED, IND_IGNORE_CRL_NOT_FOUND, IND_IGNORE_CRL_NOT_VALID, MAX_LOGIN_ATTEMPTS, LOCKOUT_DURATION_IN_MINUTES, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS, PASSWORD_FORMAT, URL, USERNAME, PASSWORD, LDAP_DN_SEARCH_BASE, LDAP_SEARCH_FILTER, IND_SEARCH_SUBTREE, GROUP_SEARCH_BASE, GROUP_SEARCH_FILTER, GROUP_ATTRIBUTE)
       values (0, 0, 'ACS', 'Internal ACS database', NULL, NULL, NULL, 5, 1440, NULL, NULL, 'ACS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

prompt
prompt   Insert into CMN_USER
prompt   ===================================================
prompt
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (0, 0, 'SysAdmin', 1, current_timestamp, NULL, 'System', 'Administrator', 'Default System Administrator', 'SysAdmin', NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (1, 0, 'Job', 1, current_timestamp, NULL, 'Non-interactive', 'User', 'Non-interactive user account', 'Password', NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (2, 0, 'ScheduledReportJob', 1, current_timestamp, NULL, 'Non-interactive reporting', 'User', 'Non-interactive user account for scheduled reports', 'Password', NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (3, 0, 'Anonymous', 1, current_timestamp, NULL, 'Non-interactive', 'User', 'Non-interactive user account', 'Password', NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (101, 0, 'Kms',            1, current_timestamp, NULL, 'Key and Profile', 'Manager', 'Key and Profile Manager', 'kms',      NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (102, 0, 'KeyManager',     1, current_timestamp, NULL, 'Key',             'Manager', 'Key Manager',             'kms',      NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
       values (103, 0, 'ProfileManager', 1, current_timestamp, NULL, 'Profile',         'Manager', 'Profile Manager',         'kms',      NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
        VALUES(104, 0, 'IIM-Admin', 1, current_timestamp, NULL, 'IIM', 'Administrator', 'Manages the IIM configuration', 'Welcome', NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);
insert into CMN_USER (ID, ID_SOURCE, USERNAME, ID_LANGUAGE, DT_CREATED, DT_DELETED, FIRST_NAMES, LAST_NAME, DESCRIPTION, PASSWORD, DN, IND_PWD_NEVER_EXPIRES, IND_CHANGE_PWD_ONLOGIN, DT_SUSPENDED, DT_LAST_VALUE_CHANGE, DT_LAST_LOGIN, DT_LOCKED_OUT, NBR_FAILED_ATTEMPTS, SUSPEND_IF_INACITVE_IN_DAYS, DELETE_IF_INACITVE_IN_DAYS)
        VALUES(105, 0, 'IIM-Operator', 1, current_timestamp, NULL, 'IIM', 'Operator', 'Issues cards via IIM', 'Welcome', NULL, 1, 0, NULL, NULL, NULL, NULL, 0, 0, NULL);


prompt
prompt   Insert into CMN_AUTH_METHOD
prompt   ===================================================
prompt
insert into CMN_AUTH_METHOD (ID, NAME, DESCRIPTION, SECURITY_RANK) values (0,  'PASSWORD',    'Password',    5);
insert into CMN_AUTH_METHOD (ID, NAME, DESCRIPTION, SECURITY_RANK) values (2,  'CERTIFICATE', 'Certificate', 6);
insert into CMN_AUTH_METHOD (ID, NAME, DESCRIPTION, SECURITY_RANK) values (8,  'KERBEROS',    'KERBEROS',    9);
insert into CMN_AUTH_METHOD (ID, NAME, DESCRIPTION, SECURITY_RANK) values (9,  'NON_INTERACTIVE','Non-Interactive',    5);
insert into CMN_AUTH_METHOD (ID, NAME, DESCRIPTION, SECURITY_RANK) values (10, 'TOKEN',       'Token',       6);
insert into CMN_AUTH_METHOD (ID, NAME, DESCRIPTION, SECURITY_RANK) values (11, 'JEE','JEE', 5);

prompt
prompt   Insert into CMN_GROUP
prompt   ===================================================
prompt
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (1,   'SystemAdministrators', 'Users that manage users, groups, roles and actions within a domain',                             current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (2,   'NoninteractiveUsers',  'Users that can not log-in the system i.e. jobs accounts',                                        current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (3,   'CustomReportEditors',  'Users that manage custom report definitions',                                                    current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (101, 'KmsKeyManager',        'Manages Keys and KeySets',                                                                       current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (102, 'KmsProfileManager',    'Manages KeyProfiles, KeySetProfiles, IOSites, IOProcedures, IOKeyProfiles and IOKeySetProfiles', current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (103, 'KmsAdministrator',     'Performs KMS Setup, Configuration and Administration',                                           current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (120, 'IicOperators',         'Operates IIC, personalizes cards',                                                               current_timestamp, NULL);
insert into CMN_GROUP (ID, NAME, DESCRIPTION, DT_CREATED, DT_DELETED) values (121, 'IicAdministrators',    'Administraties and configures IIC',                                                              current_timestamp, NULL);

prompt
prompt   Insert into CMN_ROLE
prompt   ===================================================
prompt
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (1,   'SourceManagement',             'Manages Authentication Sources within a domain', 5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (2,   'UserManagement',               'Manages Users',                                  5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (3,   'GroupManagement',              'Manages Groups',                                 5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (4,   'RoleManagement',               'Manages Roles',                                  5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (5,   'ActionManagement',             'Manages Actions',                                5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (6,   'MethodManagement',             'Manages Method',                                 5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (7,   'SystemManagement',             'Manages system-wide Configuration',              5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (8,   'JobManagement',                'Manages Jobs',                                   5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (9,   'JobSchedulerManagement',       'Manages system-wide Job Scheduler',              5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (10,  'PasswordPolicyManagement',     'Manages Password Policy',                        5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (11,  'CustomReportManagement',       'Manages Custom Report Definitions',              5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (100, 'KmsRequireLogin',              'Enforces login screen',                          5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (101, 'KmsKeyManagement',             'Manages Keys',                                   5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (102, 'KmsKeySetManagement',          'Manages KeySets',                                5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (103, 'KmsKeyProfileManagement',      'Manages KeyProfiles',                            5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (104, 'KmsKeySetProfileManagement',   'Manages KeySetProfiles',                         5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (105, 'KmsKeyIOProfileManagement',    'Manages IO KeyProfiles',                         5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (106, 'KmsKeyIOSetProfileManagement', 'Manages IO KeySetProfiles',                      5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (107, 'KmsKeyIOProcedureManagement',  'Manages IO Procedures',                          5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (108, 'KmsKeyIOSiteManagement',       'Manages IO Sites',                               5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (109, 'KmsHistoryLogManagement',      'Manages Kms History Log',                        5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (120, 'IicOperation',                 'Operates IIC, personalizes cards',               5);
insert into CMN_ROLE (ID, NAME, DESCRIPTION, SECURITY_RANK) values (121, 'IicAdministration',            'Administraties and configures IIC',              5);

prompt
prompt   Insert into CMN_X_ACTION_IN_ROLE
prompt   ===================================================
prompt
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (1,   1);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (9,   1);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (10,  1);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (11,  1);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (2,   2);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (12,  2);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (13,  2);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (14,  2);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (3,   3);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (15,  3);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (16,  3);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (17,  3);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (4,   4);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (18,  4);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (19,  4);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (20,  4);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (5,   5);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (21,  5);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (22,  5);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (23,  5);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (25,  6);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (35,  6);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (27,  7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (31,  7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (7,   7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (166, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (28,  8);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (29,  8);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (28,  9);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (30,  9);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (6,  10);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (40, 10);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (41, 10);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (42, 10);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (32, 11);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (33, 11);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (34, 11);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (100, 100);
-- FIN-340: a user who can log into KMS, can see the system-info
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (7,   100);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (108, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (109, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (120, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (121, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (122, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 101);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (123, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (124, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (125, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 102);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (108, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (110, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (111, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (112, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (113, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (114, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (115, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (126, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (127, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (128, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 103);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (110, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (111, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (112, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (113, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (114, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (115, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (129, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (130, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (131, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 104);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (132, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (133, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (134, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 105);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (135, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (136, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (137, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 106);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (138, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (139, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (140, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 107);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (141, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (142, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (143, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 108);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (101, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (102, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (103, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (104, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (105, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (151, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 109);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (106, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (107, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (108, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (151, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (152, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (160, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (161, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (162, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (163, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (164, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (165, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (179, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (181, 7);

insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (167, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (169, 120);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (180, 120);

insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (168, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (170, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (171, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (172, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (173, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (174, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (175, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (176, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (177, 121);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (178, 121);

insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (182, 8);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (183, 8);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (184, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (185, 7);
insert into CMN_X_ACTION_IN_ROLE (ID_ACTION, ID_ROLE) values (187, 7);


prompt
prompt   Insert into CMN_X_GROUP_IN_ROLE
prompt   ===================================================
prompt
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (1,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (2,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (3,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (4,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (5,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (6,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (7,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (8,   1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (10,  1);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (11,  3);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (100, 101);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (100, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (100, 103);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (101, 101);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (102, 101);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (103, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (104, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (105, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (106, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (107, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (108, 102);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (109, 103);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (120, 120);
insert into CMN_X_GROUP_IN_ROLE (ID_ROLE, ID_GROUP) values (121, 121);

prompt
prompt   Insert into CMN_X_METHOD_IN_SOURCE
prompt   ===================================================
prompt
insert into CMN_X_METHOD_IN_SOURCE (ID_SOURCE, ID_METHOD) values (0, 0);
insert into CMN_X_METHOD_IN_SOURCE (ID_SOURCE, ID_METHOD) values (0, 9);

prompt
prompt   Insert into CMN_X_USER_IN_GROUP
prompt   ===================================================
prompt
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (0,   1);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (0,   3);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (1,   2);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (101, 101);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (101, 102);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (102, 101);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (103, 102);
insert into CMN_X_USER_IN_GROUP (ID_USER, ID_GROUP) values (0,   103);

insert into CMN_X_USER_IN_GROUP (ID_USER,ID_GROUP) values(104,121);
insert into CMN_X_USER_IN_GROUP (ID_USER,ID_GROUP) values(105,120);


prompt
prompt   Insert into CMN_X_USER_IN_METHOD
prompt   ===================================================
prompt
insert into CMN_X_USER_IN_METHOD (ID_USER, ID_METHOD) values (0,   0);
insert into CMN_X_USER_IN_METHOD (ID_USER, ID_METHOD) values (101, 0);
insert into CMN_X_USER_IN_METHOD (ID_USER, ID_METHOD) values (102, 0);
insert into CMN_X_USER_IN_METHOD (ID_USER, ID_METHOD) values (103, 0);
insert into CMN_X_USER_IN_METHOD (ID_USER, ID_METHOD) values (104, 0);
insert into CMN_X_USER_IN_METHOD (ID_USER, ID_METHOD) values (105, 0);
