--********************************************************************************************
-- This script will advance next value of sequences in the release 6 database to a value on
-- or after the last value used in the corresponding database table
--
-- Known side effects:
-- 1) Hibernate may have cached values. Recommend stop/start of application server after running this
--********************************************************************************************

prompt
prompt Advancing sequences for &&BELLID_R6_SCHEMA. 
prompt ===================================================
prompt

DECLARE
  objectOwner VARCHAR2(30):= '&&BELLID_R6_SCHEMA.';
  currentValue NUMBER;
  desiredValue NUMBER;
  CURSOR c_sequence
  IS
    SELECT sequence_owner,
      sequence_name
    FROM all_sequences a
    WHERE sequence_owner = upper(objectOwner);
-- Function to get the next value of a named sequence
FUNCTION getNextValue(
    ownerName    IN VARCHAR2,
    sequenceName IN VARCHAR2)
  RETURN NUMBER
IS
  queryString VARCHAR2(100);
  retVal      NUMBER;
BEGIN
  queryString := 'SELECT '|| ownerName || '.'|| sequenceName || '.NEXTVAL from dual';
  EXECUTE IMMEDIATE queryString INTO retVal;
  RETURN retVal;
END;
-- Function to get the current value for the table using the sequence
FUNCTION getTableValue(
    ownerName    IN VARCHAR2,
    sequenceName IN VARCHAR2)
  RETURN NUMBER
IS
  queryString VARCHAR2(100);
  retVal      NUMBER;
  tableName   VARCHAR2(100);
  CURSOR c_table
  IS
    SELECT CASE
    WHEN s.sequence_name = 'CAMS_C_BIOM_APPLET_PARAM_SEQ' THEN 'CAMS_C_BIOMETRIC_APPLET_PARAM' 
    WHEN s.sequence_name = 'CMN_C_TYPE_REPORT_DEF_SEQ' THEN 'CMN_C_TYPE_REPORT_DEFINITION' 
	WHEN s.sequence_name = 'CAMS_X_APP_CARDPROGRAM_SEQ' THEN 'CAMS_X_APPV_CARDPRG' 
	WHEN s.sequence_name = 'KMS_IOPROCEDURE_APPLIC_SEQ' THEN 'KMS_IOPROCEDURE_APPLICABILITY' 
	WHEN s.sequence_name = 'TRACE_ENTRY_TYPE_SEQ' THEN 'CMN_TRACE_ENTRY_TYPE'
	WHEN s.sequence_name = 'TRACE_ENTRY_SEQ' THEN 'CMN_TRACE_ENTRY' 
	WHEN s.sequence_name = 'CHANNEL_SEQ' THEN 'CMN_CHANNEL' 
	WHEN s.sequence_name = 'CHANNEL_MSG_SEQ' THEN 'CMN_CHANNEL_MSG' 	
	ELSE
      (SELECT c.table_name
      FROM all_tab_columns c
      WHERE c.owner     = ownerName
      AND c.column_name = 'ID'
      AND (c.table_name
        || '_SEQ') = s.sequence_name
      )
   END seq_table_name
   FROM all_sequences s
   WHERE sequence_owner = ownerName
   AND s.sequence_name  = sequenceName;
BEGIN
  OPEN c_table;
  FETCH c_table INTO tableName;
  IF tableName IS NULL THEN
    dbms_output.put_line('No table defined for sequence ' || ownerName || '.' || sequenceName);
    retVal := -1;
  ELSE
    queryString := 'SELECT NVL(MAX(id),0) from ' || ownerName || '.' || tableName;
    EXECUTE IMMEDIATE queryString INTO retVal;
  END IF;
  RETURN retVal;
END;
-- Main body
BEGIN
  FOR r_sequence IN c_sequence
  LOOP
    desiredValue       := getTableValue(r_sequence.sequence_owner, r_sequence.sequence_name);
    currentValue       := getNextValue(r_sequence.sequence_owner, r_sequence.sequence_name);
    WHILE (currentValue < desiredValue)
    LOOP
      currentValue := getNextValue(r_sequence.sequence_owner, r_sequence.sequence_name);
    END LOOP;
  END LOOP;
END;
/
