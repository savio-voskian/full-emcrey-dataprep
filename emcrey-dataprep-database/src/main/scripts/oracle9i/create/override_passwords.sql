--===================================================================================
--
--Description     : Script to set the password for a schema and application users
--
--Version date    : 07 December 2011
--
-- This script selects the values to be used for the application schema and
-- application run schema and stores these in script variables.
-- Unless overridden by a paramater, the default values will be set to the same
-- name as the schema (this value should NEVER be used in production: The value should
-- either be overridden or changed after install)
--===================================================================================
--                               (c) 2011 Bell ID B.V.
--===================================================================================

COLUMN schema_user_col NEW_VALUE BELLID_R6_SCHEMA_PASSWORD NOPRINT
COLUMN app_user_col NEW_VALUE BELLID_R6_APP_USER_PASSWORD NOPRINT
COLUMN mi_user_col NEW_VALUE BELLID_R6_MI_USER_PASSWORD NOPRINT
 
SELECT CASE 
         WHEN '&BELLID_R6_SCHEMA_PASSWORD' IS NULL 
         THEN '&BELLID_R6_SCHEMA' 
		 ELSE '&BELLID_R6_SCHEMA_PASSWORD' 
       END as schema_user_col,
       CASE 
         WHEN '&BELLID_R6_APP_USER_PASSWORD' IS NULL 
         THEN 'app_user_' || '&BELLID_R6_SCHEMA' 
		 ELSE '&BELLID_R6_APP_USER_PASSWORD' 
       END as app_user_col,
       CASE 
         WHEN '&BELLID_R6_MI_USER_PASSWORD' IS NULL 
         THEN 'mi_user_' || '&BELLID_R6_SCHEMA' 
		 ELSE '&BELLID_R6_MI_USER_PASSWORD' 
       END as mi_user_col
FROM dual;
