--===================================================================================
--insert_blobdata.sql
--Description     : Script to insert blob data using SQL Loader.
--                  Data is inserted in this order:
--                  1) Common 2) Solution specific 3) Project specific
--                  The control file insert_blobdata.ctl defines the insert rules.
--                  The files common_blobdata.txt, solution_blobdata.txt and project_blobdata.txt
--                  define lists of blob files to be inserted. These text files are comma-delimited.
--                  The text files contain fields of the form A,B,C,D
--                  Where:
--                  'A' is the id to use for the record: Optional. 
--                    Only fill this in if you need a specific value (only use values < 10000). Leave blank to automatically generate a value.
--                  'B' is the 'group index' to be used. Optional.
--                  'C' is the mime type to use for the record: Optional.
--                  'D' is the filename and path for the blob file (path relative to this directory using / for directory delimiter)
--                 See common_blobdata.txt for examples.
--                 The files solution_blobdata.txt and project_blobdata.txt in this directory are deliberately empty. These
--                 will be overwritten by solution and project-specific versions at build time.
--
--Version date    : August 26, 2011
--
--===================================================================================
--                               --(c) 2011 Bell ID B.V.
--===================================================================================

SPOOL &&logdrive/insert_blobdata.log;

PROMPT ===========================================================================================;
PROMPT INSERT common_blobdata for &&BELLID_R6_APP_USER USER
PROMPT ===========================================================================================;

host sqlldr &&BELLID_R6_APP_USER/&&BELLID_R6_APP_USER_PASSWORD@&&connectstring insert_blobdata.ctl data=common_blobdata.txt bad=&&logdrive/common_blobdata.bad log=&&logdrive/common_blobdata.log errors=0

PROMPT ===========================================================================================;
PROMPT INSERT solution_blobdata for &&BELLID_R6_APP_USER USER
PROMPT ===========================================================================================;

host sqlldr &&BELLID_R6_APP_USER/&&BELLID_R6_APP_USER_PASSWORD@&&connectstring insert_blobdata.ctl data=solution_blobdata.txt bad=&&logdrive/solution_blobdata.bad log=&&logdrive/solution_blobdata.log errors=0

PROMPT ===========================================================================================;
PROMPT INSERT project_blobdata for &&BELLID_R6_APP_USER USER
PROMPT ===========================================================================================;

host sqlldr &&BELLID_R6_APP_USER/&&BELLID_R6_APP_USER_PASSWORD@&&connectstring insert_blobdata.ctl data=project_blobdata.txt bad=&&logdrive/project_blobdata.bad log=&&logdrive/project_blobdata.log errors=0

SPOOL OFF;
