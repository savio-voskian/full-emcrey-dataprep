
CREATE OR REPLACE PROCEDURE addremoveconstr(tableName IN VARCHAR2, constName IN VARCHAR2, constDefinitionSql IN VARCHAR2, action  IN VARCHAR2) 
IS	iCount NUMBER;
BEGIN
iCount :=0;
   select count(*)
	into iCount from all_cons_columns a,all_constraints b 
   where a.owner=b.owner and a.constraint_name=b.constraint_name
   and a.table_name=b.table_name
   and a.table_name=UPPER(tableName)
   and a.constraint_name = UPPER(constName)
   and a.owner=UPPER('&&BELLID_R6_SCHEMA.');
	IF iCount > 0  AND action = 'DROP' THEN
	  EXECUTE IMMEDIATE constDefinitionSql;
	ELSIF iCount = 0  AND action = 'ADD' THEN
	  EXECUTE IMMEDIATE constDefinitionSql;
	ELSIF iCount = 0  AND action = 'DROP' THEN
	  dbms_output.put_line('Constriant is already dropped!');
	ELSIF iCount > 0   AND action = 'ADD' THEN
	  dbms_output.put_line('Constriant is already Added!');
	END IF;
  END;
/


CREATE OR REPLACE PROCEDURE addremoveColumn(tableName IN VARCHAR2, columnName IN VARCHAR2, columnDefinitionSql IN VARCHAR2, action IN VARCHAR2 ) IS
    iCount NUMBER;
  BEGIN
    SELECT COUNT(*) INTO iCount
	FROM all_tab_columns
	WHERE owner = UPPER('&&BELLID_R6_SCHEMA.')
	AND table_name = UPPER(tableName)
	AND column_name = UPPER(columnName);
	IF iCount > 0  AND action = 'MODIFY' THEN
	  EXECUTE IMMEDIATE columnDefinitionSql;
	ELSIF  iCount = 0  AND action = 'MODIFY' THEN
	  dbms_output.put_line('COLUMN ' || tableName || '.' || columnName || ' doesnt exists!');
    ELSIF  iCount = 0  AND action = 'ADD' THEN
	  EXECUTE IMMEDIATE columnDefinitionSql;
    ELSIF  iCount > 0  AND action = 'ADD' THEN
	  dbms_output.put_line('Column ' || tableName || '.' || columnName || ' already added');
    ELSIF  iCount > 0  AND action = 'DROP' THEN	  
	  EXECUTE IMMEDIATE columnDefinitionSql;
	ELSIF  iCount = 0  AND action = 'DROP' THEN
	  dbms_output.put_line('Column ' || tableName || '.' || columnName || ' already dropped.');
	ELSE
	  dbms_output.put_line('Invalid Parameters in call to addremoveColumn sub-program!!');
	END IF;
  END;
/

CREATE OR REPLACE PROCEDURE addremoveforeignkey(tableName IN VARCHAR2, columnName IN VARCHAR2, constDefinitionSql IN VARCHAR2, action  IN VARCHAR2 ) IS
   iCount NUMBER;
BEGIN
   select count(*)
	into iCount from all_cons_columns a,all_constraints b 
   where a.owner=b.owner and a.constraint_name=b.constraint_name
   and b.constraint_type='R'
   and a.table_name=b.table_name
   and a.table_name=UPPER(tableName)
   and a.column_name like '%'||UPPER(columnName)||'%'
   and a.owner=UPPER('&&BELLID_R6_SCHEMA.');
   IF iCount > 0  AND action = 'DROP' THEN
	  EXECUTE IMMEDIATE constDefinitionSql;
	ELSIF iCount = 0  AND action = 'ADD' THEN
	  EXECUTE IMMEDIATE constDefinitionSql;
	END IF;
  END;
/

CREATE OR REPLACE PROCEDURE drop_table( V_TABNAME IN VARCHAR2) IS
num NUMBER;
BEGIN
num :=0;
select count(*) into num from all_tables where table_name=V_TABNAME and owner=upper('&&BELLID_R6_SCHEMA.');
IF num>0 then 
execute immediate 'drop table &&BELLID_R6_SCHEMA..'||V_TABNAME;
ELSE 
 dbms_output.put_line('Table is already dropped!');
end if;
END;
/

CREATE OR REPLACE PROCEDURE drop_sequence( V_SEQNAME IN VARCHAR2) IS
num NUMBER;
BEGIN
num :=0;
select count(*) into num from all_sequences where sequence_name=V_SEQNAME and sequence_owner=upper('&&BELLID_R6_SCHEMA.');
IF num>0 then 
execute immediate 'drop sequence &&BELLID_R6_SCHEMA..'||V_SEQNAME;
ELSE 
 dbms_output.put_line('Sequence is already dropped!');
end if;
END;
/

CREATE OR REPLACE PROCEDURE addremoveindex(tableName IN VARCHAR2, indexName IN VARCHAR2, indexDefinitionSql IN VARCHAR2, action IN VARCHAR2 ) IS
iCount NUMBER;
BEGIN
 iCount :=0;
 SELECT COUNT(*) INTO iCount
 FROM all_indexes
 WHERE table_owner = UPPER('&&BELLID_R6_SCHEMA.')
 AND table_name = UPPER(tableName)
 AND index_name = UPPER(indexName);
 IF iCount > 0  AND action = 'DROP' THEN
   EXECUTE IMMEDIATE indexDefinitionSql;
 ELSIF iCount = 0  AND action = 'ADD' THEN
   EXECUTE IMMEDIATE indexDefinitionSql;
 ELSIF iCount = 0  AND action = 'DROP' THEN
   dbms_output.put_line('Index is already dropped!');
 ELSIF iCount > 0   AND action = 'ADD' THEN
   dbms_output.put_line('Index is already Added!');
 END IF;
END;
/
