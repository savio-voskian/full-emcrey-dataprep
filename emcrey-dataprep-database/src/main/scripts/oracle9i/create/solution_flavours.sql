
-- The default will be to switch off all available flavours so ind_active is 0. 
-- One solution flavour is defined to have CVN50 config added.
insert into cmn_module_flavour(module_name, flavour_name, ind_active) values ('MOB', 'CVN50', 0);

insert into cmn_module_flavour(module_name, flavour_name, ind_active) values ('MOB', 'EFTPOS', 0);
insert into cmn_module_flavour(module_name, flavour_name, ind_active) values ('MOB', 'MCBP', 1);
insert into cmn_module_flavour(module_name, flavour_name, ind_active) values ('MOB', 'MADA_HCE', 1);