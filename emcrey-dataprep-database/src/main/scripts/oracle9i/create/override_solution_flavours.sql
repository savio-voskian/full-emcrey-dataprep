--===================================================================================
--
--Description     : Script to override the default solution flavours chosen for a project
--
--Version date    : 07 December 2011
--
-- This script works by inspacting the parameter OVERRIDE_SOLUTION_FLAVOURS.
-- If override is needed, the user should set the parameter to:
-- a) Either 'ALL' to enable all flavours. This is unlikely in a production environment
--    but it does allow us to test that everything is compatible...)
-- b) Or a comma -separated list of the necessary flavours 
--    (for example 'FLAV1,FLAV2,FLAV3' or 'FLAV666').
--    All flavours that do not appear in the list will be disabled for installation.
--    All flavours that appear in the list will be enabled for installation.
-- c) Or leave the value at 'DEFAULT' and the values defined for the project will
--    not be changed.
--===================================================================================
--                               (c) 2011 Bell ID B.V.
--===================================================================================

DECLARE
  runFlavours VARCHAR2(100) :=  TRIM(UPPER(REPLACE('&&OVERRIDE_SOLUTION_FLAVOURS', ' ', '')));
BEGIN
  IF runFlavours = 'ALL' THEN
      -- Enable all flavours
      UPDATE cmn_module_flavour
	  SET ind_active = 1;
  ELSE
    -- Get rid of any extra ',' characters on the override string
    WHILE (runFlavours LIKE '%,') LOOP
      runFlavours := substr(runFlavours, 1, length(runFlavours) - 1);
    END LOOP;
    -- Decide whether to override
    IF (runFlavours = 'DEFAULT' OR runFlavours IS NULL) THEN
      dbms_output.put_line('No override defined. Using default solution flavours');
    ELSE
      -- Disable all flavours
      UPDATE cmn_module_flavour
	  SET ind_active = 0;
	  -- Re-enabled the override ones
      UPDATE cmn_module_flavour
	  SET ind_active = 1
	  WHERE runFlavours = UPPER(flavour_name)
	  OR runFlavours LIKE UPPER(flavour_name) || ',%'
	  OR runFlavours LIKE '%,' || UPPER(flavour_name) || ',%'
	  OR runFlavours LIKE '%,' || UPPER(flavour_name);
    END IF;
  END IF;
  COMMIT;
END;
/