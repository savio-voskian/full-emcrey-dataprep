------------------------------------------------------------------------------------------
--
-- Control file used to load files containing binary data (BLOB files)
-- to the table CMN_CONFIG. 
-- See insert_configdata.sql for usage
------------------------------------------------------------------------------------------
LOAD DATA
APPEND
INTO TABLE CMN_CONFIG
FIELDS TERMINATED BY ','
(
 ID "NVL2(:ID, :ID, CMN_CONFIG_SEQ.NEXTVAL)",
 NAME,
 file_name FILLER CHAR(100),
 BINARY_DATA LOBFILE (file_name) TERMINATED BY EOF
)
