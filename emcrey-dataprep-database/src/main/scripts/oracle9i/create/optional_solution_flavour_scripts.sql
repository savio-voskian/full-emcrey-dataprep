--===================================================================================
--
--Description     : Script to determine whether a solution 'flavour' is configured
--                  to run on this installation. If it is, the script named
--                     solution_flavour_xxx_yyy.sql
--                  will be run, where xxx is the module name and yyy the flavour name (e.g. ID and PIV).
--                  If the flavour is not configured, then the stub script solution_flavour_stub.sql will be run.
--
--Version date    : September 29, 2011
--
--===================================================================================
SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;
SET ECHO OFF;
SET PAGESIZE 0;
SET FEEDBACK OFF;
insert into cmn_module_flavour values ('NA','NA',1);
Spool solution_flavour_schema.sql
select case when module_name = 'NA' then '' else '@solution_flavour_' || lower(module_name) || '_' || lower(flavour_name)||'.sql' end from cmn_module_flavour where ind_active = 1 and flavour_name like '%_SCHEMA' order by rowid;
spool off;

Spool solution_flavour_insert.sql
select case when module_name = 'NA' then '' else '@solution_flavour_' || lower(module_name) || '_' || lower(flavour_name)||'.sql' end from cmn_module_flavour where ind_active = 1 and flavour_name like '%_INSERT' order by rowid;
spool off;

delete from cmn_module_flavour where flavour_name='NA';


SET FEEDBACK ON;