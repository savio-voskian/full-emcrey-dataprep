--===================================================================================
--connect_dba_user.sql
--Description     : Script to connect the user with DBA rights to the database.
--                  This is called from SQL 'run*.sql' scripts which have already had
--                  the necessary username and password definitions defined.
--
-- Usage:           This script will be called from SQL 'run*.sql' scripts which have already had
--                  the necessary username and password definitions defined.
--
--                  If username/passwords are defined in a secure external password store
--                  and not available to the user, the appropriate "CONNECT" line below
--                  must be uncommented.
--                  In this scenario, the dba_user and dba_password in the parameters file
--                  will be ignored. They can be set to any value.
--
--Version date    : August 26, 2011
--
--===================================================================================
--                               --(c) 2011 Bell ID B.V.
--===================================================================================

-- Comment out this line if using external password store:
CONNECT &&dba_user/&&dba_password@&&connectstring;

-- Uncomment this line if using external password store:
-- CONNECT /@&&connectstring;