--===================================================================================
--insert_blobdata.sql
--Description     : Script to insert cmn_config data using SQL Loader.
-- $Revision$
-- $Author$
-- $Date$
-- $URL$
-- TODO Move this to the cmn database build and correct below
--                  Data is inserted in this order:
--                  1) Common 2) Solution specific 3) Project specific
--                  The control file insert_configdata.ctl defines the insert rules.
--                  The files common_configdata.txt, solution_configdata.txt and project_configdata.txt
--                  define lists of config files to be inserted. These text files are comma-delimited.
--                  The text files contain fields of the form A,B,C,D
--                  Where:
--                  'A' is the id to use for the record: Optional. 
--                    Only fill this in if you need a specific value (only use values < 10000). Leave blank to automatically generate a value.
--                  'B' is the 'group index' to be used. Optional.
--                  'C' is the mime type to use for the record: Optional.
--                  'D' is the filename and path for the config file (path relative to this directory using / for directory delimiter)
--                 See common_configdata.txt for examples.
--                 The files solution_configdata.txt and project_configdata.txt in this directory are deliberately empty. These
--                 will be overwritten by solution and project-specific versions at build time.
--
--Version date    : August 26, 2011
--
--===================================================================================
--                               --(c) 2011 Bell ID B.V.
--===================================================================================

SPOOL &&logdrive/insert_configdata.log;

PROMPT ===========================================================================================;
PROMPT INSERT common_configdata for &&BELLID_R6_APP_USER USER
PROMPT ===========================================================================================;

host sqlldr &&BELLID_R6_APP_USER/&&BELLID_R6_APP_USER_PASSWORD@&&connectstring insert_configdata.ctl data=common_configdata.txt bad=&&logdrive/common_configdata.bad log=&&logdrive/common_configdata.log errors=0

PROMPT ===========================================================================================;
PROMPT INSERT solution_configdata for &&BELLID_R6_APP_USER USER
PROMPT ===========================================================================================;

host sqlldr &&BELLID_R6_APP_USER/&&BELLID_R6_APP_USER_PASSWORD@&&connectstring insert_configdata.ctl data=solution_configdata.txt bad=&&logdrive/solution_configdata.bad log=&&logdrive/solution_configdata.log errors=0

PROMPT ===========================================================================================;
PROMPT INSERT project_configdata for &&BELLID_R6_APP_USER USER
PROMPT ===========================================================================================;

host sqlldr &&BELLID_R6_APP_USER/&&BELLID_R6_APP_USER_PASSWORD@&&connectstring insert_configdata.ctl data=project_configdata.txt bad=&&logdrive/project_configdata.bad log=&&logdrive/project_configdata.log errors=0

SPOOL OFF;
