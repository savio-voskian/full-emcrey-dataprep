--===================================================================================
--Description     : Script to create the common schema objects used by all Release 6
--                  installations.
--Version	  : $Id: $
--Version date    : $Date: 2014-11-26 13:13:16 +0100 (Wed, 26 Nov 2014) $
--Last changed by : $Author: d.malenicic $
--Usage						:
--	This script will be called from run_r6_create.sql and should not be called stand-alone.
--===================================================================================
--                               (c) 2011 Bell ID B.V.
--===================================================================================

alter session set current_schema = &&BELLID_R6_SCHEMA.;


prompt
prompt Create table CMN_APPLET_DEFINITION
prompt ===================================================
prompt
create table CMN_APPLET_DEFINITION (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        WIDTH number(10,0) not null,
        HEIGHT number(10,0) not null,
        ARCHIVE varchar2(400 char),
        CODE varchar2(400 char) not null,
        CODEBASE varchar2(400 char),
        RESPONSE_MIME varchar2(50) not null,
        constraint PK_APPLET_DEFINITION primary key (ID) using index tablespace &&tbspace_index,
        constraint UK_APPLET_DEFINITION unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_APPLET_PARAMETER
prompt ===================================================
prompt
create table CMN_APPLET_PARAMETER (
        ID number(10,0) not null,
        ID_APPLET_DEFINITION number(10,0) not null,
        NAME varchar2(50 char) not null,
        constraint PK_APPLET_PARAMETER primary key (ID) using index tablespace &&tbspace_index,
        constraint UK_APPLET_PARAMETER unique (ID_APPLET_DEFINITION, NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_ANDIS_BLOB
prompt ===================================================
prompt
create table CMN_ANDIS_BLOB (
        ID number(19,0) not null,
        GROUP_INDEX number(10,0),
        MIME varchar2(100 char),
        BINARY_DATA blob not null,
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_CONFIG
prompt ===================================================
prompt
create table CMN_CONFIG (
        ID number(10,0) not null,
        NAME varchar2(255 char) not null,
        BINARY_DATA blob,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_COUNTRY_CODE
prompt ===================================================
prompt
create table CMN_COUNTRY_CODE (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(255 char),
        ABBREVIATION varchar2(20 char),
        CODE_ALPHA_2 varchar2(2 char) not null,
        CODE_ALPHA_3 varchar2(3 char) not null,
        CODE_NUMERIC varchar2(3 char) not null,
        IND_SHORTLIST_MEMBER number(1) not null,
        constraint PK_COUNTRY_CODE primary key (ID) using index tablespace &&tbspace_index,
        constraint UK_COUNTRY_CODE_NAME unique (NAME) using index tablespace &&tbspace_index,
        constraint UK_COUNTRY_CODE_2 unique (CODE_ALPHA_2) using index tablespace &&tbspace_index,
        constraint UK_COUNTRY_CODE_3 unique (CODE_ALPHA_3) using index tablespace &&tbspace_index,
        constraint UK_COUNTRY_CODE_NUMERIC unique (CODE_NUMERIC) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_CURRENCY
prompt ===================================================
prompt
create table CMN_CURRENCY (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null ,
        CODE_A varchar2(3 char) not null,
        CODE_N varchar2(3 char) not null,
        EXPONENT varchar(2 char) not null,
        DT_LAST_UPDATE timestamp not null,
        constraint PK_CURRENCY_CODE primary key (ID) using index tablespace &&tbspace_index ,
        constraint UK_CURRENCY_CODE_2 unique (CODE_A) using index tablespace &&tbspace_index ,
        constraint UK_CURRENCY_CODE_3 unique (CODE_N)USING INDEX tablespace &&tbspace_index
);

prompt
prompt Create table CMN_DUAL
prompt ===================================================
prompt
create table CMN_DUAL (
        ID number(1,0) not null,
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_DB_SCRIPT_AUDIT
prompt ===================================================
prompt
create table CMN_DB_SCRIPT_AUDIT (
        ID number(6,0) not null,
        TARGET_DB varchar2(35 char) not null,
        DB_VERSION varchar2(100 char) not null,
        SCRIPT varchar2(100 char) not null,
        DESCRIPTION varchar2(200 char) not null,
        REVISION number(10,0) not null,
        DT_EXECUTION timestamp not null,
        EXECUTOR varchar2(100 char) not null,
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_LANGUAGE
prompt ===================================================
prompt
create table CMN_LANGUAGE (
        ID number(10,0) not null,
        NAME varchar2(10 char) not null,
        DESCRIPTION varchar2(50 char),
        ISO3_LANG_CODE varchar2(3 char),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_LICENSE
prompt ===================================================
prompt
create table CMN_LICENSE (
        ID number(10,0) not null,
        NAME varchar2(30 char),
        CURRENT_VOLUME number(10,0) not null,
        primary key (ID) using index tablespace &&tbspace_index
);
prompt
prompt Create table CMN_MODULE_FLAVOUR
prompt ===================================================
prompt
create table CMN_MODULE_FLAVOUR (
        MODULE_NAME varchar2(50 char) not null,
        FLAVOUR_NAME varchar2(50 char) not null,
        IND_ACTIVE number(1,0),
        primary key (MODULE_NAME, FLAVOUR_NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_TABLE_NAME
prompt ===================================================
prompt
create table CMN_TABLE_NAME (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_AUTH_USER_DETAILS
prompt ===================================================
prompt
create table CMN_AUTH_USER_DETAILS (
        ID             number(19,0)  not null,
        USERNAME       varchar2(100) not null,
        APP_NAME       varchar2(100) not null,
        AUTH_SOURCE    varchar2(100) not null,
        AUTH_TIME      timestamp     not null,
        ID_USER_ENTITY number(19,0)  not null,
        IND_ACS_USER   number(1,0)   not null,
        IP_ADDRESS     varchar2(39 char),
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_C_REPORT_ENGINE
prompt ===================================================
prompt
create table CMN_C_REPORT_ENGINE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        TEMPLATE_FILE_EXTENSION varchar2(20 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_C_TYPE_REPORT_DEFINITION
prompt ===================================================
prompt
create table CMN_C_TYPE_REPORT_DEFINITION (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);


prompt
prompt Create table CMN_C_REPORT_FORMAT
prompt ===================================================
prompt
create table CMN_C_REPORT_FORMAT (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_REPORT_DEFINITION
prompt ===================================================
prompt
create table CMN_REPORT_DEFINITION (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(400 char) not null,
		ID_REPORT_ENGINE number(19,0) not null,
		ID_TYPE_DEFINITION number(19,0) not null,
		ID_REPORT_TEMPLATE number(19,0),
		ID_COMPILED_TEMPLATE number(19,0),
		ID_LOGO number(19,0),
        primary key (ID) using index tablespace &&tbspace_index,
		unique (NAME) using index tablespace &&tbspace_index,
		constraint FK_REP_DEF_ENGINE foreign key (ID_REPORT_ENGINE) references CMN_C_REPORT_ENGINE (id),
		constraint FK_REP_DEF_TYPE foreign key (ID_TYPE_DEFINITION) references CMN_C_TYPE_REPORT_DEFINITION (id),
		constraint FK_REP_REP_TEMPLATE foreign key (ID_REPORT_TEMPLATE) references CMN_ANDIS_BLOB (id),
		constraint FK_REP_DEF_COMPILED_TEMPLATE foreign key (ID_COMPILED_TEMPLATE) references CMN_ANDIS_BLOB (id),
		constraint FK_REP_DEF_LOGO foreign key (ID_LOGO) references CMN_ANDIS_BLOB (id)
);
-- ID_COMPILED_TEMPLATE to be removed as we do not need to store compiled report in should do compiling on the fly (see TSP-2202)

prompt
prompt Create table CMN_REPORT_CATEGORY
prompt ===================================================
prompt
create table CMN_REPORT_CATEGORY (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);


prompt
prompt Create table CMN_X_REPORT_DEF_CATEGORY
prompt ===================================================
prompt
create table CMN_X_REPORT_DEF_CATEGORY (
        ID_REPORT_DEFINITION number(19,0) not null,
        ID_REPORT_CATEGORY number(19,0) not null,
        primary key (ID_REPORT_DEFINITION,ID_REPORT_CATEGORY) using index tablespace &&tbspace_index,
        constraint FK_X_REP_DEF_CAT_DEF foreign key (ID_REPORT_DEFINITION) references CMN_REPORT_DEFINITION (id),
        constraint FK_X_REP_DEF_CAT_CAT foreign key (ID_REPORT_CATEGORY) references CMN_REPORT_CATEGORY (id)
);


prompt
prompt Create table CMN_X_REPORT_DEF_FORMAT
prompt ===================================================
prompt
create table CMN_X_REPORT_DEF_FORMAT (
        ID_REPORT_DEFINITION number(19,0) not null,
        ID_REPORT_FORMAT number(19,0) not null,
        primary key (ID_REPORT_DEFINITION,ID_REPORT_FORMAT) using index tablespace &&tbspace_index,
        constraint FK_X_REP_DEF_FMT_DEF foreign key (ID_REPORT_DEFINITION) references CMN_REPORT_DEFINITION (id),
        constraint FK_X_REP_DEF_FMT_FMT foreign key (ID_REPORT_FORMAT) references CMN_C_REPORT_FORMAT (id)
);


prompt
prompt Create table CMN_REPORT_RUN_AT
prompt ===================================================
prompt
create table CMN_REPORT_RUN_AT (
    ID number(19,0) not null,
    ID_REPORT_DEFINITION number(19,0) not null,
    ID_AUTH_USER_DETAILS number(10,0),
    ID_REPORT_FORMAT number(19,0) not null,
    DT_START_RUN timestamp not null,
    DT_END_RUN timestamp,
    IND_RUN_SUCCESS number(1,0),
    AUDIT_MESSAGE varchar2(255 char),
    primary key (ID) using index tablespace &&tbspace_index,
    constraint FK_REP_RUN_DEF foreign key (ID_REPORT_DEFINITION) references CMN_REPORT_DEFINITION (id),
    constraint FK_REP_RUN_AUTH_USER_DETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS (id),
    constraint FK_REP_RUN_FORMAT foreign key (ID_REPORT_FORMAT) references CMN_C_REPORT_FORMAT (id)
);

prompt
prompt Create table CMN_CHANNEL
prompt ===================================================
prompt
create table CMN_CHANNEL  (
        ID             number(19,0) not null,
        VER            number(10,0) not null,
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_CHANNEL_X_NAME
prompt ===================================================
prompt
create table CMN_CHANNEL_X_NAME  (
        NAME           varchar2(50 char) not null,
        CHANNEL_ID     number(19,0) not null,
        primary key (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_CHANNEL_MSG
prompt ===================================================
prompt
create table CMN_CHANNEL_MSG  (
        ID             number(19,0) not null,
        CHANNEL_ID     number(19,0) not null,
        IDX            number(10,0) not null,
        DATA           varchar2(4000 char) not null,
        primary key (ID) using index tablespace &&tbspace_index
);


prompt
prompt Create table CMN_APPLICATION_RESOURCE
prompt ===================================================
prompt
create table CMN_APPLICATION_RESOURCE (
        ID number(10,0) not null,
        LANGUAGE_ID number(10,0),
        MSGKEY varchar2(255 char),
        VAL varchar2(255 char),
        LAST_UPDATE timestamp,
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_TRACE_ENTRY_TYPE
prompt ===================================================
prompt

create table CMN_TRACE_ENTRY_TYPE (
        ID number(10,0) not null,
        NAME varchar2(127 char) not null,
        primary key (id) using index tablespace &&tbspace_index,
        unique (name) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_TRACE_ENTRY
prompt ===================================================
prompt

create table CMN_TRACE_ENTRY (
        ID number(19,0) not null,
        TS TIMESTAMP(6) not null,
        TYPE_ID number(10,0) not null,
        CTX_KEY number(19,0),
        COR_KEY number(19,0),
        SUBJECT_ID number(19,0),
        SUBJECT varchar2(127 char),
        DETAILS varchar2(1023 char),
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_HISTORY_LOG
prompt =========================================
prompt

CREATE TABLE CMN_HISTORY_LOG (
        ID             number(19, 0) NOT NULL,
        OPERATOR       varchar2(100 char)   NOT NULL,
        DT_ACTION      timestamp      NOT NULL,
        ACTION         varchar2(6 char)     NOT NULL,
        SUBJECT        varchar2(100 char)   NOT NULL,
        AUDIT_BEFORE   clob ,
        AUDIT_AFTER    clob ,
        FORMAT_VERSION number(19, 0) NOT NULL,
        CONSTRAINT PK_CMN_HISTORY_LOG PRIMARY KEY (ID)
) tablespace &&tbspace_data. ;

prompt Create table CMN_WORKFLOW_INSTANCE
prompt ===================================================
prompt
CREATE TABLE CMN_WORKFLOW_INSTANCE (
        ID              NUMBER(19,0) NOT NULL,
        VERSION         NUMBER(10,0) NOT NULL,
        PARENT_ID       NUMBER(19,0),
        WORKFLOW_NAME   VARCHAR2(64 CHAR) NOT NULL,
        WORKFLOW_STATUS NUMBER(10,0) NOT NULL,
        CURRENT_NODE    VARCHAR2(64 CHAR),
        CONTEXT_JSON    VARCHAR2(4000 CHAR),
        WORKFLOW_DUE    TIMESTAMP,
        CREATED         TIMESTAMP,
        UPDATED         TIMESTAMP,
        FINISHED        TIMESTAMP,
        ACTIVITY_DUE    TIMESTAMP,
        primary key (ID) using index tablespace &&tbspace_index
) tablespace &&tbspace_data. ;

prompt Create table CMN_WORKFLOW_DATA
prompt ===================================================
prompt
CREATE TABLE CMN_WORKFLOW_DATA (
        ID              NUMBER(19,0) NOT NULL,
        NAME            VARCHAR2(50 CHAR) NOT NULL,
        DESCRIPTION     VARCHAR2(255 CHAR),
        WORKFLOW        BLOB NOT NULL,
        constraint PK_WORKFLOW_DATA primary key (ID) using index tablespace &&tbspace_index
) tablespace &&tbspace_data. ;

alter table CMN_APPLET_PARAMETER        add constraint FK_APPLET_PARAM_DEF
        foreign key (ID_APPLET_DEFINITION) references CMN_APPLET_DEFINITION(id);

alter table CMN_APPLICATION_RESOURCE    add constraint FKAPPRESOURCE_LANGUAGEID
        foreign key (LANGUAGE_ID) references CMN_LANGUAGE(id);

alter table CMN_TRACE_ENTRY             add constraint fk_traceentry_typeid
        foreign key (type_id) references CMN_TRACE_ENTRY_TYPE(id);

alter table CMN_CHANNEL_X_NAME          add constraint FK_CHANNELNAME_ID
        foreign key (CHANNEL_ID) references CMN_CHANNEL(id) on delete cascade;

alter table CMN_CHANNEL_MSG             add constraint FK_CHANNELMSG_ID
        foreign key (CHANNEL_ID) references CMN_CHANNEL(id) on delete cascade;


CREATE INDEX ix_cmn_history_log_operator  ON CMN_HISTORY_LOG (OPERATOR) tablespace &&tbspace_data.;
CREATE INDEX ix_cmn_history_log_dt_action ON CMN_HISTORY_LOG (DT_ACTION) tablespace &&tbspace_data.;
CREATE INDEX ix_cmn_history_log_action    ON CMN_HISTORY_LOG (ACTION) tablespace &&tbspace_data.;
create index idx_traceentry_ctxkey on CMN_TRACE_ENTRY(ctx_key) tablespace &&tbspace_index;
create index idx_traceentry_typesubject on CMN_TRACE_ENTRY(type_id, subject_id) tablespace &&tbspace_index;
create index IDX_CHANNELNAME_ID on CMN_CHANNEL_X_NAME (CHANNEL_ID) tablespace &&tbspace_index;
create unique index IDX_CHANNEL_MSG on CMN_CHANNEL_MSG (CHANNEL_ID, IDX) tablespace &&tbspace_index;
create index IXFKAPPRESOURCE_LANGUAGEID on CMN_APPLICATION_RESOURCE ( LANGUAGE_ID ) tablespace &&tbspace_index;
create index IXFK_REP_DEF_TYPE on CMN_REPORT_DEFINITION ( ID_TYPE_DEFINITION ) tablespace &&tbspace_index;
create index IXFK_REP_DEF_ENGINE on CMN_REPORT_DEFINITION ( ID_REPORT_ENGINE ) tablespace &&tbspace_index;
create index IXFK_REP_DEF_LOGO on CMN_REPORT_DEFINITION ( ID_LOGO ) tablespace &&tbspace_index;
create index IXFK_REP_REP_TEMPLATE on CMN_REPORT_DEFINITION ( ID_REPORT_TEMPLATE ) tablespace &&tbspace_index;
create index IXFK_REP_DEF_COMPILED_TEMPLATE on CMN_REPORT_DEFINITION ( ID_COMPILED_TEMPLATE ) tablespace &&tbspace_index;
create index IXFK_REP_RUN_FORMAT on CMN_REPORT_RUN_AT ( ID_REPORT_FORMAT ) tablespace &&tbspace_index;
create index IXFK_REP_RUN_USER on CMN_REPORT_RUN_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFK_REP_RUN_DEF on CMN_REPORT_RUN_AT ( ID_REPORT_DEFINITION ) tablespace &&tbspace_index;
create index IXFK_X_REP_DEF_CAT_CAT on CMN_X_REPORT_DEF_CATEGORY ( ID_REPORT_CATEGORY ) tablespace &&tbspace_index;
create index IXFK_X_REP_DEF_FMT_FMT on CMN_X_REPORT_DEF_FORMAT ( ID_REPORT_FORMAT ) tablespace &&tbspace_index;

create sequence CMN_APPLET_DEFINITION_SEQ start with 10000 increment by 1;
create sequence CMN_APPLET_PARAMETER_SEQ start with 10000 increment by 1;
create sequence CMN_ANDIS_BLOB_SEQ start with 10000 increment by 50;
create sequence CMN_CONFIG_SEQ start with 10000 increment by 1;
create sequence CMN_COUNTRY_CODE_SEQ start with 10000 increment by 1;
create sequence CMN_CURRENCY_SEQ start with 10000 increment by 1;
create sequence CMN_DB_SCRIPT_AUDIT_SEQ start with 10000 increment by 1;
create sequence CMN_LANGUAGE_SEQ start with 10000 increment by 1;
create sequence CMN_LICENSE_SEQ start with 10000 increment by 1;
create sequence CMN_TABLE_NAME_SEQ start with 10000 increment by 1;
create sequence CMN_APPLICATION_RESOURCE_SEQ start with 10000 increment by 1;
create sequence CMN_C_REPORT_ENGINE_SEQ start with 10000 increment by 1;
create sequence CMN_C_TYPE_REPORT_DEF_SEQ start with 10000 increment by 1;
create sequence CMN_C_REPORT_FORMAT_SEQ start with 10000 increment by 1;
create sequence CMN_REPORT_DEFINITION_SEQ start with 10000 increment by 1;
create sequence CMN_REPORT_CATEGORY_SEQ start with 10000 increment by 1;
create sequence CMN_REPORT_RUN_AT_SEQ start with 10000 increment by 50;
create sequence TRACE_ENTRY_TYPE_SEQ START WITH 10 increment by 50;
create sequence TRACE_ENTRY_SEQ START WITH 10000 increment by 100;
create sequence CHANNEL_SEQ START WITH 1000 increment by 1;
create sequence CHANNEL_MSG_SEQ START WITH 10000 increment by 50;
CREATE SEQUENCE CMN_WORKFLOW_INSTANCE_SEQ START WITH 10000 INCREMENT BY 50;
create sequence CMN_CONFIG_AUDIT_SEQ START WITH 1000 increment by 1;
create sequence CMN_HISTORY_LOG_SEQ START WITH 10000 increment by 1;
CREATE SEQUENCE CMN_WORKFLOW_DATA_SEQ START WITH 10000 INCREMENT BY 1;
