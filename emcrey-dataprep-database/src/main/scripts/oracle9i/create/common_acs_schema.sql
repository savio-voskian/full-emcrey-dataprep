
prompt
prompt Create table CMN_ACTION
prompt ===================================================
prompt
create table CMN_ACTION (
        ID number(10,0) not null,
        NAME varchar2(255 char) not null,
        DESCRIPTION varchar2(400 char),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);
prompt
prompt Create table CMN_AUTH_METHOD
prompt ===================================================
prompt
create table CMN_AUTH_METHOD (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        SECURITY_RANK number(1,0) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_AUTH_SOURCE
prompt ===================================================
prompt
create table CMN_AUTH_SOURCE (
        ID number(10,0) not null,
        ID_TYPE_SOURCE number(2,0) not null,
        NAME varchar2(255 char) not null,
        DESCRIPTION varchar2(400 char),
        DT_DELETED timestamp,
        IND_IGNORE_CRL_NOT_FOUND number(1,0) default 1,
        IND_IGNORE_CRL_NOT_VALID number(1,0) default 1,
        MAX_LOGIN_ATTEMPTS number(2,0) default 5,
        LOCKOUT_DURATION_IN_MINUTES number(5,0) default 1800,
        PASSWORD_VALIDITY_IN_DAYS number(3,0) default 0,
        PASSWORD_HISTORY_IN_MONTHS number(2,0) default 0,
        SUSPEND_IF_INACITVE_IN_DAYS number(3,0) default 0,
        DELETE_IF_INACITVE_IN_DAYS number(3,0) default 0,
        PASSWORD_FORMAT varchar2(100 char),
        PASSWORD_FORMAT_ERROR_TEXT varchar2(400 char),
        URL varchar2(400 char),
        USERNAME varchar2(400 char),
        PASSWORD varchar2(128 char),
        LDAP_DN_SEARCH_BASE varchar2(400 char),
        LDAP_SEARCH_FILTER varchar2(400 char),
        IND_SEARCH_SUBTREE number(1,0) default 1,
        GROUP_SEARCH_BASE varchar2(400 char),
        GROUP_SEARCH_FILTER varchar2(400 char),
        GROUP_ATTRIBUTE varchar2(32 char),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);
-- Remove three database columns. [TD-307] 9c2f9a0b680


prompt
prompt Create table CMN_C_TYPE_AUTH_SOURCE
prompt ===================================================
prompt
create table CMN_C_TYPE_AUTH_SOURCE (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_GROUP
prompt ===================================================
prompt
create table CMN_GROUP (
        ID number(10,0) not null,
        NAME varchar2(255 char) not null,
        DESCRIPTION varchar2(400 char),
        DT_CREATED timestamp not null,
        DT_DELETED timestamp,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_GROUP_IN_SOURCE
prompt ===================================================
prompt
create table CMN_GROUP_IN_SOURCE (
        ID number(10,0) not null,
        SOURCE_GROUP_NAME varchar2(255 char) not null,
        ID_SOURCE number(10,0) not null,
        ID_GROUP number(10,0) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (SOURCE_GROUP_NAME, ID_SOURCE) using index tablespace &&tbspace_index,
        unique (ID_SOURCE, ID_GROUP) using index tablespace &&tbspace_index,
        unique (SOURCE_GROUP_NAME, ID_SOURCE, ID_GROUP) using index tablespace &&tbspace_index
);


prompt
prompt Create table CMN_ROLE
prompt ===================================================
prompt
create table CMN_ROLE (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        SECURITY_RANK number(1,0) default 5 not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_USER
prompt ===================================================
prompt
create table CMN_USER (
        ID number(10,0) not null,
        ID_SOURCE number(10,0) not null,
        USERNAME varchar2(255 char) not null,
        ID_LANGUAGE number(10,0) not null,
        DT_CREATED timestamp not null,
        DT_DELETED timestamp,
        FIRST_NAMES varchar2(50 char),
        LAST_NAME varchar2(80 char),
        DESCRIPTION varchar2(400 char),
        PASSWORD varchar2(128 char),
        DN varchar2(400 char),
        IND_PWD_NEVER_EXPIRES number(1,0) default 0 not null,
        IND_CHANGE_PWD_ONLOGIN number(1,0) default 0 not null,
        DT_SUSPENDED timestamp,
        DT_LAST_VALUE_CHANGE timestamp,
        DT_LAST_LOGIN timestamp,
        DT_LOCKED_OUT timestamp,
        NBR_FAILED_ATTEMPTS number(2,0) default 0 not null,
        SUSPEND_IF_INACITVE_IN_DAYS number(3,0),
        DELETE_IF_INACITVE_IN_DAYS number(3,0),
        EMAIL_ADDRESS varchar2(320 char),
        CODE_USER_EXTERNAL varchar2(100 char),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (ID_SOURCE, USERNAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_X_ACTION_IN_ROLE
prompt ===================================================
prompt
create table CMN_X_ACTION_IN_ROLE (
        ID_ACTION number(10,0) not null,
        ID_ROLE number(10,0) not null,
        primary key (ID_ROLE, ID_ACTION) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_X_GROUP_IN_ROLE
prompt ===================================================
prompt
create table CMN_X_GROUP_IN_ROLE (
        ID_GROUP number(10,0) not null,
        ID_ROLE number(10,0) not null,
        primary key (ID_GROUP, ID_ROLE) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_X_METHOD_IN_SOURCE
prompt ===================================================
prompt
create table CMN_X_METHOD_IN_SOURCE (
        ID_SOURCE number(10,0) not null,
        ID_METHOD number(10,0) not null,
        primary key (ID_SOURCE, ID_METHOD) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_X_USER_IN_GROUP
prompt ===================================================
prompt
create table CMN_X_USER_IN_GROUP (
        ID_USER number(10,0) not null,
        ID_GROUP number(10,0) not null,
        primary key (ID_USER, ID_GROUP) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_X_USER_IN_METHOD
prompt ===================================================
prompt
create table CMN_X_USER_IN_METHOD (
        ID_USER number(10,0) not null,
        ID_METHOD number(10,0) not null,
        primary key (ID_USER, ID_METHOD) using index tablespace &&tbspace_index
);

prompt
prompt Create table CMN_PASSWORD_HISTORY
prompt ===================================================
prompt
create table CMN_PASSWORD_HISTORY (
        ID number(10,0) not null,
        ID_USER number(10,0) not null,
        DT_VALUE_CHANGE timestamp not null,
        VALUE varchar2(128 char),
        primary key (ID) using index tablespace &&tbspace_index
);

alter table CMN_PASSWORD_HISTORY add constraint FKPASSWORDHISTORY_IDUSER foreign key (ID_USER) references CMN_USER(ID);
alter table CMN_AUTH_SOURCE add constraint FKAUTHSOURCE_IDTYPESOURCE foreign key (ID_TYPE_SOURCE) references CMN_C_TYPE_AUTH_SOURCE(ID);
alter table CMN_GROUP_IN_SOURCE add constraint FKGROUPINSOURCE_IDGROUP foreign key (ID_GROUP) references CMN_GROUP(ID);
alter table CMN_GROUP_IN_SOURCE add constraint FKGROUPINSOURCE_IDSOURCE foreign key (ID_SOURCE) references CMN_AUTH_SOURCE(ID);
alter table CMN_USER add constraint FKUSER_IDLANGUAGE foreign key (ID_LANGUAGE) references CMN_LANGUAGE(ID);
alter table CMN_USER add constraint FKUSER_IDSOURCE foreign key (ID_SOURCE) references CMN_AUTH_SOURCE(ID);
alter table CMN_X_ACTION_IN_ROLE add constraint FKXACTIONINROLE_IDACTION foreign key (ID_ACTION) references CMN_ACTION(ID);
alter table CMN_X_ACTION_IN_ROLE add constraint FKXACTIONINROLE_IDROLE foreign key (ID_ROLE) references CMN_ROLE(ID);
alter table CMN_X_GROUP_IN_ROLE add constraint FKXGROUPINROLE_IDROLE foreign key (ID_ROLE) references CMN_ROLE(ID);
alter table CMN_X_GROUP_IN_ROLE add constraint FKXGROUPINROLE_IDGROUP foreign key (ID_GROUP) references CMN_GROUP(ID);
alter table CMN_X_METHOD_IN_SOURCE add constraint FKXMETHODINSOURCE_IDMETHOD foreign key (ID_METHOD) references CMN_AUTH_METHOD(ID);
alter table CMN_X_METHOD_IN_SOURCE add constraint FKXMETHODINSOURCE_IDSOURCE foreign key (ID_SOURCE) references CMN_AUTH_SOURCE(ID);
alter table CMN_X_USER_IN_GROUP add constraint FKXUSERINGROUP_IDGROUP foreign key (ID_GROUP) references CMN_GROUP(ID);
alter table CMN_X_USER_IN_GROUP add constraint FKXUSERINGROUP_IDUSER foreign key (ID_USER) references CMN_USER(ID);
alter table CMN_X_USER_IN_METHOD add constraint FKXUSERINMETHOD_IDUSER foreign key (ID_USER) references CMN_USER(ID);
alter table CMN_X_USER_IN_METHOD add constraint FKXUSERINMETHOD_IDMETHOD foreign key (ID_METHOD) references CMN_AUTH_METHOD(ID);

create index idx_user_data_changed on CMN_PASSWORD_HISTORY (ID_USER, DT_VALUE_CHANGE) TABLESPACE &&tbspace_index;
create index idx_date_changed on CMN_PASSWORD_HISTORY (DT_VALUE_CHANGE) TABLESPACE &&tbspace_index;
create index idx_user on CMN_PASSWORD_HISTORY (ID_USER) TABLESPACE &&tbspace_index;		
create index IXFKAUTHSOURCE_IDTYPESOURCE on CMN_AUTH_SOURCE ( ID_TYPE_SOURCE ) TABLESPACE &&tbspace_index;
create index IXFKGROUPINSOURCE_IDGROUP on CMN_GROUP_IN_SOURCE ( ID_GROUP ) TABLESPACE &&tbspace_index;
create index IXFKUSER_IDLANGUAGE on CMN_USER ( ID_LANGUAGE ) TABLESPACE &&tbspace_index;
create index IXFKXACTIONINROLE_IDACTION on CMN_X_ACTION_IN_ROLE ( ID_ACTION ) TABLESPACE &&tbspace_index;
create index IXFKXGROUPINROLE_IDROLE on CMN_X_GROUP_IN_ROLE ( ID_ROLE ) TABLESPACE &&tbspace_index;
create index IDXGRPINROLE_IDGRP on CMN_X_GROUP_IN_ROLE ( ID_GROUP ) TABLESPACE &&tbspace_index;
create index IXFKXMETHODINSOURCE_IDMETHOD on CMN_X_METHOD_IN_SOURCE ( ID_METHOD ) TABLESPACE &&tbspace_index;
create index IXFKXUSERINGROUP_IDGROUP on CMN_X_USER_IN_GROUP ( ID_GROUP ) TABLESPACE &&tbspace_index;
create index IXFKXUSERINMETHOD_IDMETHOD on CMN_X_USER_IN_METHOD ( ID_METHOD ) TABLESPACE &&tbspace_index;

create sequence CMN_PASSWORD_HISTORY_SEQ start with 10000 increment by 50;
create sequence CMN_ACTION_SEQ start with 10000 increment by 1;
create sequence CMN_AUTH_SOURCE_SEQ start with 10000 increment by 1;
create sequence CMN_GROUP_IN_SOURCE_SEQ start with 10000 increment by 1;
create sequence CMN_GROUP_SEQ start with 10000 increment by 1;
create sequence CMN_AUTH_METHOD_SEQ start with 10000 increment by 1;
create sequence CMN_ROLE_SEQ start with 10000 increment by 1;
create sequence CMN_USER_SEQ start with 10000 increment by 1;
create sequence CMN_AUTH_USER_DETAILS_SEQ start with 50 increment by 50;
