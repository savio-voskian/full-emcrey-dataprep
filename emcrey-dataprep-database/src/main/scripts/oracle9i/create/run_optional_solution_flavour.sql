--===================================================================================
--
--Description     : Script to determine whether a solution 'flavour' is configured
--                  to run on this installation. If it is, the script named
--                     solution_flavour_xxx_yyy.sql
--                  will be run, where xxx is the module name and yyy the flavour name (e.g. ID and PIV).
--                  If the flavour is not configured, then the stub script solution_flavour_stub.sql will be run.
--
--Version date    : September 29, 2011
--
--===================================================================================
COLUMN module_name_col NEW_VALUE moduleName
COLUMN flavour_name_col NEW_VALUE flavourName
COLUMN flavour_script_col NEW_VALUE flavourScript
COLUMN flavour_script_col NEW_VALUE flavourScript
 
SELECT '&1' AS module_name_col,
       '&2' AS flavour_name_col,
	   CASE
         COUNT (*)
         WHEN 0 THEN  'common_no_insert'
         ELSE  'solution_flavour_' || lower('&1') || '_' || lower('&2')
       END AS flavour_script_col
FROM cmn_module_flavour
WHERE lower(module_name) = lower('&1')
AND lower(flavour_name) = lower('&2')
AND ind_active = 1;


@&flavourScript

