
prompt
prompt Create table CAMS_ADDRESS
prompt ===================================================
prompt
create table CAMS_ADDRESS (
        ID number(19,0) not null,
        STREET varchar2(50 char),
        NBR_HOUSE varchar2(20 char),
        NBR_HOUSE_SUFFIX varchar2(20 char),
        POSTAL_CODE varchar2(20 char),
        CITY varchar2(50 char),
        COUNTRY varchar2(50 char),
        ID_COUNTRY_CODE number(10,0),
        NBR_TELEPHONE varchar2(35 char),
        NBR_FAX varchar2(35 char),
        EMAIL_ADDRESS varchar2(320 char),
        NAME_CONTACT varchar2(50 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_APPLICATION
prompt ===================================================
prompt
create table CAMS_APPLICATION (
        ID number(19,0) not null,
        ID_PROVIDER number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        CODE_APPLICATION_EXTERNAL varchar2(32 char),
        ID_LOGO number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_APPLICATION_AT
prompt ===================================================
prompt
create table CAMS_APPLICATION_AT (
        ID                      number(19,0) not null,
        ID_APPLICATION_ON_CARD  number(19,0) not null,
        ID_FROM_STATE           number(19,0) not null,
        ID_TO_STATE             number(19,0) not null,
        ID_EVENT                number(19,0) not null,
        ID_AUTH_USER_DETAILS    number(19,0),
        DT_TIMESTAMP            timestamp not null,
        REASON                  varchar2(400 char),
        CHIPDATA_TLV            blob,
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_APPLICATION_ON_CARD
prompt ===================================================
prompt
create table CAMS_APPLICATION_ON_CARD (
        ID number(19,0) not null,
        ID_APPLICATION_VERSION number(19,0) not null,
        ID_CARD number(19,0) not null,
        ID_STATE number(19,0) not null,
        CODE_APPLICATION_EXTERNAL varchar2(32 char),
        GSM348_COUNTER number(15,0),
        SCP80_COUNTER number(15,0),
        SCP02_COUNTER number(10,0),
        DT_START_VALIDITY date,
        DT_END_VALIDITY date,
        DT_NOTIFICATION_SENT timestamp,
        COUNT_NOTIFICATION_SENT number(10,0),
        CHIPDATA_TLV blob,
        CHIPDATA_FORMATTED blob,
        EMPLOYEE_AFFILIATION    varchar2(50 char),
        AGENCY_CARD_SERIAL_NUMBER        varchar2(50 char),
        ISSUER_IDENTIFICATION            varchar2(50 char),
        ORG_AFFILIATION_ABBREVIATION     varchar2(50 char),
        ORGANIZATION_AFFILIATION_LINE1   varchar2(50 char),
        ORGANIZATION_AFFILIATION_LINE2   varchar2(50 char),
        AFFILIATION_COLOR                varchar2(50 char),
        CITIZENSHIP                      varchar2(50 char),
        EMERGENCY_RESPONSE_OFFICIAL      number(1,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table APPLICATION_PROVIDER
prompt ===================================================
prompt
create table CAMS_APPLICATION_PROVIDER (
        ID number(19,0) not null,
        ID_ADDRESS number(19,0),
        NAME varchar2(50 char) not null,
        EMAIL_ADDRESS varchar2(320 char),
        CODE_PROVIDER_EXTERNAL varchar2(10 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table APPLICATION_VERSION
prompt ===================================================
prompt
create table CAMS_APPLICATION_VERSION (
        ID number(19,0) not null,
        AV_TYPE varchar2(5 char) not null,
        ID_LIFECYCLE number(19,0) not null,
        ID_TYPE_APPLICATION number(19,0) not null,
        ID_CHIP_DEFINITION number(19,0),
        ID_APPLICATION number(19,0) not null,
        ID_APPLICATION_VERSION number(19,0),
        NBR_APPLICATION_VERSION varchar2(15 char) not null,
        DATE_START_APPLICATION date not null,
        DATE_END_APPLICATION date not null,
        IND_DEFAULT number(1,0),
        SIZE_APPLICATION number(19,0) not null,
        GP_PRIVILEGES raw(25),
        PACKAGE_AID raw(48),
        INSTANCE_AID raw(48),
        MODULE_AID raw(48),
        TAR raw(3),
        INSTALL_PARAMETER raw(2000),
        LOAD_PARAMETER raw(2000),
        APP_PRIVILEGES raw (2000),
        PKI_PARAMETERS varchar2(2000 char),
        MULTOS_INSTALL_PARAM varchar2(2000 char),
        IGNORE_ORIG_CARDHOLDER_NAME number(1,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_ADDON_FILE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        ID_TYPE_ADDON_FILE number(10,0) not null,
        DESCRIPTION varchar2(100 char),
        RESRC varchar2(255 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_EVENT
prompt ===================================================
prompt
create table CAMS_C_EVENT (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        CODE_EVENT_BIT number(19,0) not null,
        IND_READONLY number(1,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_LIFECYCLE
prompt ===================================================
prompt
create table CAMS_C_LIFECYCLE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        ID_TYPE_LIFECYCLE number(19,0) not null,
        ID_GRAPH_LAYOUT number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_STATE
prompt ===================================================
prompt
create table CAMS_C_STATE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        CODE_STATE_BIT number(19,0) not null,
        IND_ACTIVE number(1,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_TYPE_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_C_TYPE_ADDON_FILE (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(100 char),
        ID_TABLE_NAME number(10,0),
        INTERFACE varchar2(200 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_TYPE_DATA
prompt ===================================================
prompt
create table CAMS_C_TYPE_DATA (
        ID number(19,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_TYPE_NUMBER
prompt ===================================================
prompt
create table CAMS_C_TYPE_NUMBER (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_VARIABLE
prompt ===================================================
prompt
create table CAMS_C_TYPE_VARIABLE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_DEFINITION_VARIABLE
prompt ===================================================
prompt
create table CAMS_DEFINITION_VARIABLE (
        ID number(19,0) not null,
        ID_NUMBERING number(19,0),
        ID_TYPE_CODE_TABLE number(19,0),
        ID_TABLE_CONFIG number(10,0),
        ID_TABLE_VALUE number(10,0) not null,
        ID_TYPE_VARIABLE number(19,0) not null,
        NAME varchar2(100 char) not null,
        DESCRIPTION varchar2(100 char),
        VARIABLE_SIZE varchar2(20 char),
        VARIABLE_DEFAULT varchar2(4000 char),
        IND_VISIBLE number(1,0) not null,
        IND_MANDATORY number(1,0) not null,
        IND_READONLY number(1,0) not null,
        VARIABLE_ORDER number(10,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_EXTERNAL_APPL
prompt ===================================================
prompt
create table CAMS_EXTERNAL_APPL (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        ID_TRANSPORT varchar2(255 char),
        ID_SERIALIZER varchar2(255 char),
        ID_ENCRYPTION number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_FAILED_MESSAGE
prompt ===================================================
prompt
create table CAMS_FAILED_MESSAGE (
        ID number(19,0) not null,
        ID_EXTERNAL_APPL number(19,0) not null,
        DT_CREATED timestamp not null,
        DT_LAST_ATTEMPT timestamp,
        TRY_COUNT number(10,0) not null,
        MESSAGE_SERIALIZED number(1,0) not null,
        MESSAGE_ENCRYPTED number(1,0) not null,
        KEY_INDEX varchar2(255 char),
        IV varchar2(50 char),
        ENTITY_NAME varchar2(255 char),
        ENTITY_ID number(19,0),
        MESSAGE blob not null,
        SOURCE varchar2(50 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_NUMBERING
prompt ===================================================
prompt
create table CAMS_NUMBERING (
        ID               number(19,0) not null,
        ID_TYPE_NBR      number(19,0) not null,
        NAME             varchar2(35 char) not null,
        DESCRIPTION      varchar2(100 char),
        NBR_FIRST        number(19,0) not null,
        NBR_LAST         number(19,0) not null,
        NBR_CURRENT      number(19,0) not null,
        INCREMENT_SIZE   number(10,0) not null,
        MINIMUM_LENGTH   number(10,0) default 0 not null,
        PREFIX_NUMBERING varchar2(20 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_RECORD_CODE_TABLE
prompt ===================================================
prompt
create table CAMS_RECORD_CODE_TABLE (
        ID number(19,0) not null,
        ID_TYPE_CODE_TABLE number(19,0) not null,
        VARIABLE_VALUE varchar2(4000 char) not null,
        DESCRIPTION varchar2(100 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_STATE_TRANSITION
prompt ===================================================
prompt
create table CAMS_STATE_TRANSITION (
        ID number(19,0) not null,
        ID_EVENT number(19,0) not null,
        ID_LIFECYCLE number(19,0) not null,
        ID_STATE_CURRENT number(19,0) not null,
        ID_STATE_NEW number(19,0) not null,
        IND_MAN_PERMIT number(1,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_SUBSCRIPTION_RULE
prompt ===================================================
prompt
create table CAMS_SUBSCRIPTION_RULE (
        ID number(19,0) not null,
        NAME varchar2(50 char) not null,
        ID_LIFECYCLE number(19,0) not null,
        ID_EVENT number(19,0),
        ID_STATE_CURRENT number(19,0),
        ID_STATE_NEW number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_TYPE_CODE_TABLE
prompt ===================================================
prompt
create table CAMS_TYPE_CODE_TABLE (
        ID number(19,0) not null,
        DESCRIPTION varchar2(50 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (DESCRIPTION)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_VARIABLE_CONFIG
prompt ===================================================
prompt
create table CAMS_VARIABLE_CONFIG (
        ID number(19,0) not null,
        ID_DEFINITION_VARIABLE number(19,0) not null,
        VALUE_PK_COLUMN number(19,0) not null,
        VARIABLE_ORDER number(10,0),
        IND_MANDATORY number(1,0),
        IND_VISIBLE number(1,0),
        IND_READONLY number(1,0),
        VARIABLE_DEFAULT varchar2(4000 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (ID_DEFINITION_VARIABLE, VALUE_PK_COLUMN)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_VARIABLE_VALUE
prompt ===================================================
prompt
create table CAMS_VARIABLE_VALUE (
        ID                     number(19,0) not null,
        ID_DEFINITION_VARIABLE number(19,0) not null,
        VALUE_PK_COLUMN        number(19,0) not null,
        VARIABLE_VALUE         varchar2(4000 char),
        ID_BINARY_VALUE        number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (ID_DEFINITION_VARIABLE, VALUE_PK_COLUMN)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD
prompt ===================================================
prompt
create table CAMS_CARD (
        ID                      number(19,0) not null,
        ID_REQUEST              number(19,0) not null,
        ID_FIRM                 number(19,0),
        ID_PERSO_BUREAU         number(19,0),
        ID_CARDPROGRAM          number(19,0),
        ID_REASON_CARD          number(10,0),
        ID_STATE                number(19,0) not null,
        ID_DEPARTMENT           number(19,0),
        ID_MEMBERSHIP           number(19,0) not null,
        ID_TYPE_CARD            number(19,0) not null,
        ID_NAME_PREFERENCE      number(19,0),
        ID_CARDHOLDER           number(19,0),
        ID_CPO                  number(19,0),
        ID_CARD_ADMINISTRATOR   number(10,0),
        ID_QUOTA_ADMINISTRATOR  number(10,0),
        ID_CARD_WITHDRAWOR      number(10,0),
        ID_TARGET_GROUP         number(19,0),
        CARD_PRINTER_NAME       varchar2(255 char),
        MAGSTRIPE_TRACK2        varchar2(255 char),
        MAGSTRIPE_TRACK1        varchar2(255 char),
        CARRIER_DATA            blob,
        EMBOSSING_DATA          blob,
        DATE_END_EMPLOYMENT     date,
        DATE_START_EMPLOYMENT   date,
        CODE_CARD_EXTERNAL      varchar2(100 char),
        CODE_CARD               varchar2(20 char),
        NBR_CONTACTLESS_CHIP    varchar2(35 char),
        NBR_CONTACT_CHIP        varchar2(35 char),
        NBR_KEYSET_VERSION_CC   number(10,0),
        DATE_END_CARD_VALIDITY  date,
        DATE_CARD_DISTRIBUTION  date,
        CODE_PRODUCTION_BATCH   varchar2(35 char),
        NBR_KEYSET_VERSION_CLC  number(10,0),
        NAME_ON_CARD            varchar2(200 char),
        DT_LAST_UPDATE          timestamp,
        IND_SELECTED            number(1,0),
        IND_RETURN_CARD         number(1,0),
        IND_BLOCK_CC            number(1,0),
        IND_BLOCK_CLC           number(1,0),
        DT_NOTIFICATION_SENT    timestamp,
        COUNT_NOTIFICATION_SENT number(10,0),
        ID_ISSUER               number(19,0),
        ID_TYPE_IDENTIFICATION  number(19,0),
        NBR_IDENTIFICATION      varchar2(20 char),
        DT_ISSUANCE             date,
        FASC_N                  varchar2(50 char),
        UUID                    varchar2(32 char),
        TLV_DATA                blob,
        DIST_NAME               varchar2(255 char),
        PASS_CODE_TRIES         number(2,0),
        PASS_CODE_RETRIES       number(2,0),
        CODE_ACTIV_DATE         timestamp,
        PASS_CODE               varchar2(100 char),
        CODE_ACTIVATION         varchar2(50 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (CODE_CARD)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARDHOLDER
prompt ===================================================
prompt
create table CAMS_CARDHOLDER (
        ID                          number(19,0) not null,
        ID_TYPE_CARDHOLDER          number(19,0) not null,
        GENDER                      number(10,0),
        ID_ADDRESS                  number(19,0),
        ID_STATE                    number(19,0) not null,
        LAST_NAME                   varchar2(150 char) not null,
        CODE_CARDHOLDER_EXTERNAL    varchar2(100 char),
        CODE_CARDHOLDER             varchar2(20 char),
        LAST_NAME_PARTNER           varchar2(150 char),
        PREFIX_LAST_NAME            varchar2(15 char),
        INITIALS                    varchar2(20 char),
        FIRST_NAMES                 varchar2(150 char),
        DATE_BIRTH                  date,
        CITY_BIRTH                  varchar2(35 char),
        COUNTRY_BIRTH               varchar2(35 char),
        ID_COUNTRY_CODE_BIRTH       number(10,0),
        ID_ORGANIZATION             number(19,0),
        PREFIX_LAST_NAME_PARTNER    varchar2(15 char),
        TITLES_PREFIX               varchar2(50 char),
        TITLES_SUFFIX               varchar2(50 char),
        IND_SELECTED                number(1,0),
        NBR_OF_CARDS                number(10,0),
        PASSWORD                    varchar2(128 char),
        USER_ID                     varchar2(50 char),
        EMAIL_ADDRESS               varchar2(320 char),
        KNOWLEDGE_QUESTION          varchar2(250 char),
        KNOWLEDGE_ANSWER            varchar2(250 char),
        NBR_FAILED_LGN_ATTEMPTS     number(2,0) default 0 not null,
        NBR_FAILED_KNBA_ATTEMPTS    number(2,0) default 0 not null,
        DIST_NAME                   varchar2(255 char),
        ALT_NAMES                   varchar2(255 char),
        DOMAIN_NAME                 varchar2(150 char),
        TLV_DATA                    blob,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (CODE_CARDHOLDER)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_CARDHOLDER_AT
prompt ===================================================
prompt
create table CAMS_CARDHOLDER_AT (
        ID                   number(19,0) not null,
        ID_CARDHOLDER        number(19,0) not null,
        ID_FROM_STATE        number(19,0) not null,
        ID_TO_STATE          number(19,0) not null,
        ID_EVENT             number(19,0) not null,
        ID_AUTH_USER_DETAILS number(19,0),
        DT_TIMESTAMP         timestamp not null,
        REASON               varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;



prompt
prompt Create table CAMS_CARD_AUDIT_TRAIL
prompt ===================================================
prompt
create table CAMS_CARD_AT (
        ID                   number(19,0) not null,
        ID_CARD              number(19,0) not null,
        ID_FROM_STATE        number(19,0) not null,
        ID_TO_STATE          number(19,0) not null,
        ID_EVENT             number(19,0) not null,
        ID_AUTH_USER_DETAILS number(19,0),
        DT_TIMESTAMP         timestamp not null,
        REASON               varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD_FAMILY
prompt ===================================================
prompt
create table CAMS_CARD_FAMILY (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        IND_PRODUCE number(1,0),
        ID_PERSONALIZER number(10,0) not null,
        DAYS_IN_PRODUCTION number(10,0) not null,
        CARD_MAX_IN_BATCH number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_TYPE_CARD_IN_STOCK
prompt ===================================================
prompt
create table CAMS_TYPE_CARD_IN_STOCK (
        ID number(19,0) not null,
        NAME varchar2(100 char) not null,
        ID_SUPPLIER number(19,0) not null,
        ID_LIFECYCLE number(19,0) not null,
        ID_CARD_FAMILY number(19,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_CSTOCK_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_X_TYPE_CSTOCK_ADDON_FILE (
        ID_TYPE_CARD_IN_STOCK number(19,0) not null,
        ID_ADDON_FILE number(19,0) not null,
        primary key (ID_TYPE_CARD_IN_STOCK, ID_ADDON_FILE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD_IN_STOCK
prompt ===================================================
prompt
create table CAMS_CARD_IN_STOCK (
        ID number(19,0) not null,
        ID_STATE number(19,0),
        ID_BATCH number(19,0) not null,
        ID_TYPE_CARD_IN_STOCK number(19,0) not null,
        ID_STOCK_ADMINISTRATOR number(10,0) not null,
        CODE_CARD varchar2(20 char),
        NBR_CONTACTLESS_CHIP varchar2(35 char),
        NBR_CONTACT_CHIP varchar2(35 char),
        IND_SELECTED number(1,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NBR_CONTACTLESS_CHIP)  using index tablespace &&tbspace_index,
        unique (NBR_CONTACT_CHIP)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD_PRODUCTION_ORDER
prompt ===================================================
prompt
create table CAMS_CARD_PRODUCTION_ORDER (
        ID number(19,0) not null,
        ID_STATE number(19,0),
        ID_TYPE_CPO number(19,0),
        ID_STOCK_ORDER number(19,0),
        NUMBER_IN_PRODUCTION_ORDER number(10,0) not null,
        NUMBER_CARDS_READY number(10,0),
        NAME_PRODUCTION_ORDER varchar2(120 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD_STOCK_AT
prompt ===================================================
prompt
create table CAMS_CARD_STOCK_AT (
        ID                   number(19,0) not null,
        ID_CARD_IN_STOCK     number(19,0) not null,
        ID_FROM_STATE        number(19,0) not null,
        ID_TO_STATE          number(19,0) not null,
        ID_EVENT             number(19,0) not null,
        ID_AUTH_USER_DETAILS number(19,0),
        DT_TIMESTAMP         timestamp not null,
        REASON               varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CHIP_DEFINITION
prompt ===================================================
prompt
create table CAMS_CHIP_DEFINITION (
        ID number(19,0) not null,
        ID_OPERATING_SYSTEM number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        SIZE_CHIP number(19,0) not null,
        NBR_OP_VERSION varchar2(15 char),
        NAME_BAY varchar2(50 char),
        ATR varchar2(70 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_BIOMETRIC_DATA
prompt ===================================================
prompt
create table CAMS_C_TYPE_BIOMETRIC_DATA (
        ID number(19,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        ID_CAPTURE_APPLET_DEFINITION number(10,0),
        constraint PK_TYPE_BIOMETRIC_DATA primary key (ID)  using index tablespace &&tbspace_index,
        constraint UK_TYPE_BIOMETRIC_DATA unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_CARDHOLDER_BIOMETRIC
prompt ===================================================
prompt
create table CAMS_CARDHOLDER_BIOMETRIC (
        ID number(19,0) not null,
        ID_CARDHOLDER number(19,0) not null,
        ID_TYPE_BIOMETRIC_DATA number(19,0) not null,
        DT_CAPTURED timestamp not null,
        CAPTURE_USER_NAME varchar2(100 char),
        CAPTURE_USER_SOURCE varchar2(100 char),
        CAPTURE_USER_ID varchar2(20 char),
        ID_CAPTURE_EVENT_REASON number(10,0),
        REMARKS varchar2(400 char),
        ID_BINARY_DATA number(19,0),
        constraint PK_CARDHOLDER_BIOMETRIC primary key (ID)  using index tablespace &&tbspace_index,
        constraint UK_CARDHOLDER_BIOMETRIC unique (ID_CARDHOLDER, ID_TYPE_BIOMETRIC_DATA)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_BIOMETRIC_APPLET_PARAM
prompt ===================================================
prompt
create table CAMS_C_BIOMETRIC_APPLET_PARAM (
        ID number(10,0) not null,
        ID_APPLET_PARAMETER number(10,0) not null,
        ID_TYPE_BIOMETRIC_DATA number(19,0) not null,
        VALUE varchar2(255 char),
        constraint PK_TYPE_BIOMETRIC_PARAM primary key (ID)  using index tablespace &&tbspace_index,
        constraint UK_TYPE_BIOMETRIC_PARAM unique (ID_TYPE_BIOMETRIC_DATA, ID_APPLET_PARAMETER)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_BIOMETRIC_EVENT
prompt ===================================================
prompt
CREATE table CAMS_X_BIOMETRIC_EVENT (
        ID number(10,0) not null,
        ID_EVENT number(19,0) not null,
        ID_TYPE_BIOMETRIC_DATA number(19,0) not null,
        IND_SKIP_CAPTURE number(1,0) default 0 not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        constraint UK_TYPE_BIOMETRIC_EVENT unique (ID_TYPE_BIOMETRIC_DATA, ID_EVENT)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_X_EXTAPP_SUBSCRRULE
prompt ===================================================
prompt
create table CAMS_X_EXTAPP_SUBSCRRULE (
        ID_EXTERNAL_APPL number(19,0) not null,
        ID_SUBSCRIPTION_RULE number(19,0) not null,
        primary key (ID_EXTERNAL_APPL,ID_SUBSCRIPTION_RULE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_APP_OPTIONALITY
prompt ===================================================
prompt
create table CAMS_C_APP_OPTIONALITY (
        ID number(19,0) not null,
        NAME varchar2(75 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_CARDPROGRAM
prompt ===================================================
prompt
create table CAMS_C_CARDPROGRAM (
        ID number(19,0) not null,
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_CHIP_OS
prompt ===================================================
prompt
create table CAMS_C_CHIP_OS (
        ID number(19,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(100 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_CRYPTO_REQ
prompt ===================================================
prompt
create table CAMS_C_CRYPTO_REQ (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_NAME_PREFERENCE
prompt ===================================================
prompt
create table CAMS_C_NAME_PREFERENCE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        MAX_LENGTH number(10,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_REASON
prompt ===================================================
prompt
create table CAMS_C_REASON (
        ID number(10,0) not null,
        NAME varchar2(35 char) not null,
        ID_TYPE_REASON number(19,0) not null,
        DESCRIPTION varchar2(200 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_TYPE_APPLICATION
prompt ===================================================
prompt
create table CAMS_C_TYPE_APPLICATION (
        ID number(19,0) not null,
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_TYPE_CPO
prompt ===================================================
prompt
create table CAMS_C_TYPE_CPO (
        ID number(19,0) not null,
        ID_LIFECYCLE number(19,0),
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_C_TYPE_IDENTIFICATION
prompt ===================================================
prompt
create table CAMS_C_TYPE_IDENTIFICATION (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_ORGANIZATION
prompt ===================================================
prompt
create table CAMS_C_TYPE_ORGANIZATION (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        IND_NESTED  number(1,0),
        DESCRIPTION varchar2(100 char) not null,
        ID_TYPE_PARENT_ORGANIZATION number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_QUOTA_REDUCTION
prompt ===================================================
prompt
create table CAMS_C_TYPE_QUOTA_REDUCTION (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_REASON
prompt ===================================================
prompt
create table CAMS_C_TYPE_REASON (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_EMV_TEMPLATE
prompt ===================================================
prompt
create table CAMS_EMV_TEMPLATE (
        ID number(19,0) not null,
        ID_APPLICATION_VERSION number(19,0) not null,
        ID_PERSO_BUREAU number(19,0),
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(100 char) not null,
        CHIPDATA_TEMPLATE clob,
        CONFIGURATION_DATA varchar2(2000 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_ISSUER
prompt ===================================================
prompt
create table CAMS_ISSUER (
        ID                           number(19,0) not null,
        ID_ADDRESS                   number(19,0),
        DESCRIPTION                  varchar2(100 char),
        NAME                         varchar2(35 char) not null,
        ISSUER_IDENTIFICATION_NUMBER varchar2(25 char),
        ISSUER_TRACKING_NUMBER       varchar2(25 char),
        ID_LOGO                      number(19,0),
        KEYSET_REFERENCE             varchar2(50 char),
        ID_APPLICATION_VERSION       number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_KEYSTORE
prompt ===================================================
prompt
create table CAMS_KEYSTORE (
        ID number(19,0) not null,
        KEY_NAME varchar2(255 char) not null,
        KEY_VERSION number(19,0) not null,
        ID_STATE number(19,0),
        STORAGE_KEY_NAME varchar2(255 char),
        STORAGE_KEY_VERSION number(19,0),
        VALUE varchar2(2048 char),
        KCV varchar2(255 char),
        KCD varchar2(255 char),
        PART varchar2(10 char),
        KEY_TYPE number(19,0),
        LENGTH number(10,0),
        BATCH varchar2(20 char),
        VARIANT varchar2(255 char),
        DT_INSERT timestamp not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (KEY_NAME, KEY_VERSION)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_MEMBERSHIP
prompt ===================================================
prompt
create table CAMS_MEMBERSHIP (
        ID              number(19,0) not null,
        ID_STATE        number(19,0) not null,
        ID_TARGET_GROUP number(19,0) not null,
        ID_CARDHOLDER   number(19,0),
        ID_SRV_ENTITY   number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_MEMBERSHIP_AT
prompt ===================================================
prompt
create table CAMS_MEMBERSHIP_AT (
        ID                   number(19,0) not null,
        ID_MEMBERSHIP        number(19,0) not null,
        ID_FROM_STATE        number(19,0) not null,
        ID_TO_STATE          number(19,0) not null,
        ID_EVENT             number(19,0) not null,
        ID_AUTH_USER_DETAILS number(19,0),
        DT_TIMESTAMP         timestamp not null,
        REASON               varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_ORGANIZATION
prompt ===================================================
prompt
create table CAMS_ORGANIZATION (
        ID number(19,0) not null,
        ID_PARENT_ORGANIZATION number(19,0),
        ID_TYPE_ORGANIZATION number(19,0) not null,
        ID_ADDRESS number(19,0),
        ID_CORRESPONDENCE_ADDRESS number(19,0),
        CODE_ORGANIZATION varchar2(20 char),
        DESCRIPTION varchar2(100 char),
        NAME varchar2(400 char) not null,
        DT_DELETED timestamp,
        ID_LOGO number(19,0),
        VAT_NUMBER varchar2(20 char),
        TLV_DATA blob,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME, ID_PARENT_ORGANIZATION)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_PERSO_BUREAU
prompt ===================================================
prompt
create table CAMS_PERSO_BUREAU (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        ID_ADDRESS number(19,0),
        DESCRIPTION varchar2(100 char),
        CHIP_DATA_FORMAT varchar2(20 char),
        IND_ALLOW_EMPTY_DATA number(1,0),
        IND_ENCRYPT_DATA number(1,0),
        IIC_VERSION varchar2(25 char),
        IIC_WS_URL varchar2(255 char),
        NAME_TRANSPORT_KEY varchar2(25 char),
        NAME_3DES_TRANSPORT_KEY varchar2(25 char),
        NAME_RSA_TRANSPORT_KEY varchar2(25 char),
        NAME_TRANSPORT_KEY_PIN_BLOCK varchar2(25 char),
        CIPHER_MODE_ENTIRE_BLOCK varchar2(10 char),
        PADDING_METHOD_ENTIRE_BLOCK  varchar2(20 char),
        CIPHER_MODE_3DES varchar2(10 char),
        PADDING_METHOD_3DES varchar2(20 char),
        CIPHER_MODE_RSA varchar2(10 char),
        PADDING_METHOD_RSA varchar2(20 char),
        CIPHER_MODE_PIN_BLOCK varchar2(10 char),
        PADDING_METHOD_PIN_BLOCK varchar2(20 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_STOCK_LOCATION_TYPE
prompt ===================================================
prompt

create table CAMS_C_STOCK_LOCATION_TYPE (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        ID_LIFECYCLE number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

create table CAMS_STOCK_LOCATION (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        ID_ADDRESS number(19,0),
        ID_STATE number(19,0) not null,
        ID_STOCK_LOCATION_TYPE number(19,0) not null,
        ID_PERSO_BUREAU number(19,0) ,
        IND_WAREHOUSE number(1,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD_PRINTER
prompt ===================================================
prompt
create table CAMS_CARD_PRINTER (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        STATUS varchar2(100 char),
        ID_PERSO_BUREAU number(19,0),
        IND_ACTIVE number(1,0),
        DT_LAST_UPDATE timestamp not null,
        ID_ADDON_FILE number(19,0),
        SETTINGS varchar2(4000 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME, ID_PERSO_BUREAU)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_CARD_PRINTER_LOG
prompt ===================================================
prompt
create table CAMS_CARD_PRINTER_LOG (
        ID number(19,0) not null,
        ID_CARD_PRINTER number(19,0) not null,
        ID_CARD  number(19,0) not null,
        STATUS varchar2(100 char),
        ERROR_MSG varchar2(255 char),
        LOG_DATE timestamp not null,
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_DIO
prompt ===================================================
prompt
create table CAMS_C_TYPE_DIO (
        ID number(19,0) not null,
        ID_LIFECYCLE number(19,0),
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_PRODUCTION_AT
prompt ===================================================
prompt
create table CAMS_PRODUCTION_AT (
        ID                         number(19,0) not null,
        ID_CARD_PRODUCTION_ORDER   number(19,0),
        ID_FROM_STATE              number(19,0) not null,
        ID_TO_STATE                number(19,0) not null,
        ID_EVENT                   number(19,0) not null,
        ID_AUTH_USER_DETAILS       number(19,0),
        DT_TIMESTAMP               timestamp    not null,
        REASON                     varchar2(400 char),
        NUMBER_IN_PRODUCTION_ORDER number(10,0),
        NUMBER_CARDS_READY         number(10,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt Create table CAMS_C_STOCK_ITEM_TYPE
prompt ===================================================
prompt

create table CAMS_C_STOCK_ITEM_TYPE (
        ID number(19,0) not null,
        ID_LIFECYCLE number(19,0),
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt Create table CAMS_STOCK_ITEM
prompt ===================================================
prompt
create table CAMS_STOCK_ITEM (
        ID number(19,0) not null,
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        ID_LIFECYCLE number(19,0),
        ID_STATE number(19,0) not null,
        ID_STOCK_ITEM_TYPE number(19,0),
        DFLT_ORDER_SIZE number(10,0) not null,
        DFLT_ORDER_UNIT number(10,0) not null,
        LOW_WATER_MARK number(10,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt Create table CAMS_STOCK
prompt ===================================================
prompt
create table CAMS_STOCK (
        ID number(19,0) not null,
        ID_STOCK_ITEM number(19,0) not null,
        ID_STOCK_LOCATION number(19,0) not null,
        CURRENT_SIZE number(19,0) not null,
        DT_VALIDITY timestamp,
        DT_TIMESTAMP timestamp not null,
        LOW_WATER_MARK number(19,0) not null,
        ORDER_SIZE number(10,0) not null,
        ORDER_UNIT number(10,0) not null,
        IND_AUTO_ORDER_STOCK number(1,0) not null,
        RETIRED number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_STOCK_ORDER
prompt ===================================================
prompt
create table CAMS_STOCK_ORDER (
        ID number(19,0) not null,
        ID_STATE number(19,0) not null,
        ID_STOCK_ITEM number(19,0),
        ID_SUPPLIER number(19,0) ,
        ID_STOCK_LOCATION_FROM number(19,0) not null,
        ID_STOCK_LOCATION_TO number(19,0) not null,
        DT_CREATION_DATE timestamp not null,
        DT_CHANGE_DATE timestamp not null,
        AMOUNT number(6,0) not null,
        AMOUNT_DELIVERED number(6,0),
        AMOUNT_RETURNED number(6,0),
        ORDER_REFERENCE varchar2(50 char),
        REMARKS varchar2(200 char),
        CARRIER varchar2(200 char),
        ID_ORDERTYPE number(2,0) default 1 not null,
        USER_NAME  varchar2(100 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_STOCK_ORDER_AT
prompt ===================================================
prompt
create table CAMS_STOCK_ORDER_AT (
        ID                    number(19,0) not null,
        ID_STOCK_ORDER        number(19,0) not null,
        ID_ORDERTYPE number(2,0) default 1 not null,
        ID_FROM_STATE         number(19,0) not null,
        ID_TO_STATE           number(19,0) not null,
        ID_EVENT              number(19,0) not null,
        ID_AUTH_USER_DETAILS  number(19,0),
        AMOUNT                number(6,0) not null,
        AMOUNT_DELIVERED      number(6,0),
        DT_TIMESTAMP          timestamp not null,
        REASON                varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_STOCK_ITEM_AT
prompt ===================================================
prompt
create table CAMS_STOCK_ITEM_AT (
        ID                    number(19,0) not null,
        ID_STOCK_ITEM         number(19,0) not null,
        ID_FROM_STATE         number(19,0) not null,
        ID_TO_STATE           number(19,0) not null,
        ID_EVENT              number(19,0) not null,
        ID_AUTH_USER_DETAILS  number(19,0),
        DT_TIMESTAMP          timestamp not null,
        REASON                varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_STOCK_LOCATION_AT
prompt ===================================================
prompt
create table CAMS_STOCK_LOCATION_AT (
        ID                    number(19,0) not null,
        ID_STOCK_LOCATION     number(19,0) not null,
        ID_FROM_STATE         number(19,0) not null,
        ID_TO_STATE           number(19,0) not null,
        ID_EVENT              number(19,0) not null,
        ID_AUTH_USER_DETAILS  number(19,0),
        DT_TIMESTAMP          timestamp not null,
        REASON                varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_REQUEST
prompt ===================================================
prompt
create table CAMS_REQUEST (
        ID number(19,0) not null,
        ID_TYPE_IDENTIFICATION number(19,0),
        ID_REASON_REQUEST number(10,0) not null,
        ID_CARD_REQUESTOR number(10,0),
        ID_CARD_DISTRIBUTOR number(10,0),
        ID_CARD_AUTHORIZER number(10,0),
        DATE_PREFERRED_DISTRIBUTION date not null,
        ID_PREVIOUS_CARD varchar2(20 char),
        DT_REQUEST timestamp not null,
        IND_SELECTED number(1,0),
        NUMBER_OF_CARDS number(10,0) not null,
        NBR_IDENTIFICATION varchar2(20 char),
        IND_DELETED number(10,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_REQUEST_SETTINGS
prompt ===================================================
prompt
create table CAMS_REQUEST_SETTINGS (
        ID number(19,0) not null,
        PREFERRED_DISTRIBUTION_PERIOD number(10,0) not null,
        ID_DEFAULT_QUOTA_ADMINISTRATOR number(10,0),
        ID_DEFAULT_CARD_DISTRIBUTOR number(10,0) not null,
        ID_DEFAULT_CARD_ADMINISTRATOR number(10,0) not null,
        ID_DEFAULT_AUTHORIZER number(10,0),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_STOCK_BATCH
prompt ===================================================
prompt
create table CAMS_STOCK_BATCH (
        ID number(19,0) not null,
        ID_SUPPLIER number(19,0) not null,
        ID_LIFECYCLE number(19,0) not null,
        ID_CARD_FAMILY number(19,0) not null,
        NAME varchar2(100 char) not null,
        DT_DELIVERY timestamp not null,
        CODE_BATCH_EXTERNAL varchar2(20 char),
        NBR_CARDS_IN_BATCH number(19,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_SUPPLIER
prompt ===================================================
prompt
create table CAMS_SUPPLIER (
        ID number(19,0) not null,
        ID_ADDON_FILE number(19,0),
        ID_ADDRESS number(19,0),
        NAME varchar2(35 char) not null,
        DESCRIPTION varchar2(100 char),
        CODE_SUPPLIER_EXTERNAL varchar2(20 char),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_X_STOCK_ITEM_SUPPLIER
prompt ===================================================
prompt
create table CAMS_X_STOCK_ITEM_SUPPLIER (
        ID_STOCK_ITEM number(19,0) not null,
        ID_SUPPLIER number(19,0) not null,
        primary key (ID_STOCK_ITEM, ID_SUPPLIER)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_TARGET_GROUP
prompt ===================================================
prompt
create table CAMS_TARGET_GROUP (
        ID               number(19,0) not null,
        ID_LIFECYCLE     number(19,0) not null,
        ID_ISSUER        number(19,0),
        NAME             varchar2(35 char) not null,
        DESCRIPTION      varchar2(100 char),
        KEYSET_REFERENCE varchar2(50),
        DIST_NAME        varchar2(255 char),
        ALT_NAMES        varchar2(255 char),
        TLV_DATA         blob,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_TYPE_CARD
prompt ===================================================
prompt
create table CAMS_TYPE_CARD (
        ID number(19,0) not null,
        ID_NUMBERING number(19,0),
        ID_EVENT_CODE_GENERATION number(19,0),
        ID_LIFECYCLE number(19,0) not null,
        ID_STOCK_ITEM number(19,0),
        ID_CARD_FAMILY number(19,0) not null,
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        IND_CONTACT_CHIP number(1,0) not null,
        IND_CONTACTLESS_CHIP number(1,0) not null,
        IND_PINCODE number(1,0) not null,
        IND_ANONYMOUS number(1,0) not null,
        PHOTO_SIGNATURE_VALIDITY number(10,0),
        CARD_VALIDITY number(10,0),
        CARD_LAYOUT varchar2(50 char),
        MIDDLEWARE varchar2(50 char),
        CARD_MAXIMUM number(10,0) not null,
        ID_CARD_LAYOUT_IMAGE number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_TYPE_CARDHOLDER
prompt ===================================================
prompt
create table CAMS_TYPE_CARDHOLDER (
        ID number(19,0) not null,
        ID_NUMBERING number(19,0),
        ID_LIFECYCLE number(19,0) not null,
        DESCRIPTION varchar2(100 char),
        NAME varchar2(35 char) not null,
        IND_EXTERNAL_NUMBERING number(1,0) not null,
        IND_LINK_ORGANIZATION number(1,0),
        CHECK_UNIQUE_CARDHOLDER varchar2(2000 char),
        AUTH_GROUP varchar2(50 char),
        ID_METATYPE number(2,0) default 0 not null,
        TLV_DATA blob,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_SRV_ENTITY
prompt ===================================================
prompt
create table CAMS_SRV_ENTITY (
        ID                number(19,0) not null,
        NAME              varchar2(35 char) not null,
        DESCRIPTION       varchar2(100 char),
        DT_START_VALIDITY date,
        DT_END_VALIDITY   date,
        FQDN              varchar2(150 char),
        ID_STATE          number(19,0) not null,
        ID_TYPE           number(19,0) not null,
        ID_ADDRESS        number(19,0),
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (NAME)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_SRV_ENTITY_AT
prompt ===================================================
prompt
create table CAMS_SRV_ENTITY_AT (
        ID                    number(19,0) not null,
        ID_SRV_ENTITY         number(19,0) not null,
        ID_FROM_STATE         number(19,0) not null,
        ID_TO_STATE           number(19,0) not null,
        ID_EVENT              number(19,0) not null,
        ID_AUTH_USER_DETAILS  number(19,0) not null,
        DT_TIMESTAMP          timestamp    not null,
        REASON                varchar2(400 char),
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_APPV_CARDPRG
prompt ===================================================
prompt
create table CAMS_X_APPV_CARDPRG (
        ID number(19,0) not null,
        ID_APPLICATION_VERSION number(19,0) not null,
        ID_OPTIONALITY number(19,0),
        ID_CARDPROGRAM number(19,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_APP_VER_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_X_APP_VER_ADDON_FILE (
        ID_APPLICATION_VERSION number(19,0) not null,
        ID_ADDON_FILE number(19,0) not null,
        primary key (ID_APPLICATION_VERSION, ID_ADDON_FILE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_APP_VER_CRYPTO_REQ
prompt ===================================================
prompt
create table CAMS_X_APP_VER_CRYPTO_REQ (
        ID_CRYPTO_REQ number(19,0) not null,
        ID_APPLICATION_VERSION number(19,0) not null,
        ID_CRYPTO_REQUIREMENT number(19,0) not null,
        primary key (ID_APPLICATION_VERSION, ID_CRYPTO_REQUIREMENT)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_CARDPROGRAM_TYPE_CARD
prompt ===================================================
prompt
create table CAMS_X_CARDPROGRAM_TYPE_CARD (
        ID_CARDPROGRAM number(19,0) not null,
        ID_TYPE_CARD number(19,0) not null,
        primary key (ID_CARDPROGRAM, ID_TYPE_CARD)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_CARD_FAMILY_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_X_CARD_FAMILY_ADDON_FILE (
        ID_CARD_FAMILY number(19,0) not null,
        ID_ADDON_FILE number(19,0) not null,
        primary key (ID_CARD_FAMILY, ID_ADDON_FILE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_CARD_ORGANIZATION
prompt ===================================================
prompt
create table CAMS_X_CARD_ORGANIZATION (
        ID_CARD number(19,0) not null,
        ID_ORGANIZATION number(19,0) not null,
        primary key (ID_CARD, ID_ORGANIZATION)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_CRYPTO_REQ_CHIP_DEF
prompt ===================================================
prompt
create table CAMS_X_CRYPTO_REQ_CHIP_DEF (
        ID_CHIP_DEFINITION number(19,0) not null,
        ID_CRYPTO_REQUIREMENT number(19,0) not null,
        ID_CRYPTO_REQ number(19,0) not null,
        ID_CHIP_DEF number(19,0) not null,
        primary key (ID_CRYPTO_REQ, ID_CHIP_DEF)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_APPL_CHIP_OS
prompt ===================================================
prompt
create table CAMS_X_TYPE_APPL_CHIP_OS (
        ID_TYPE_APPLICATION number(19,0) not null,
        ID_OPERATING_SYSTEM number(19,0) not null,
        primary key (ID_OPERATING_SYSTEM, ID_TYPE_APPLICATION)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_CARD_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_X_TYPE_CARD_ADDON_FILE (
        ID_TYPE_CARD number(19,0) not null,
        ID_ADDON_FILE number(19,0) not null,
        primary key (ID_TYPE_CARD, ID_ADDON_FILE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_CARD_CHIP_DEF
prompt ===================================================
prompt
create table CAMS_X_TYPE_CARD_CHIP_DEF (
        ID_CHIP_DEFINITION number(19,0) not null,
        ID_TYPE_CARD number(19,0) not null,
        primary key (ID_CHIP_DEFINITION, ID_TYPE_CARD)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_CARD_C_REASON
prompt ===================================================
prompt
create table CAMS_X_TYPE_CARD_C_REASON (
        ID number(19,0) not null,
        ID_REASON number(10,0) not null,
        ID_TYPE_CARD number(19,0) not null,
        IND_RETURN_CARD number(1,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (ID_REASON, ID_TYPE_CARD)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_CARD_TARGET_GROUP
prompt ===================================================
prompt
create table CAMS_X_TYPE_CARD_TARGET_GROUP (
        ID_TARGET_GROUP number(19,0) not null,
        ID_TYPE_CARD number(19,0) not null,
        primary key (ID_TYPE_CARD, ID_TARGET_GROUP)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TYPE_CARD_TYPE_ORG
prompt ===================================================
prompt
create table CAMS_X_TYPE_CARD_TYPE_ORG (
        ID number(19,0) not null,
        ID_TYPE_CARD number(19,0) not null,
        ID_TYPE_ORGANIZATION number(19,0) not null,
        IND_MANDATORY number(1,0) not null,
        primary key (ID)  using index tablespace &&tbspace_index,
        unique (ID_TYPE_CARD, ID_TYPE_ORGANIZATION)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TG_EMV_TEMPLATE
prompt ===================================================
prompt
create table CAMS_X_TG_EMV_TEMPLATE (
        ID_TARGET_GROUP number(19,0) not null,
        ID_EMV_TEMPLATE number(19,0) not null,
        primary key (ID_TARGET_GROUP,ID_EMV_TEMPLATE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_TG_ADDON_FILE
prompt ===================================================
prompt
create table CAMS_X_TG_ADDON_FILE (
        ID_TARGET_GROUP number(19,0) not null,
        ID_ADDON_FILE number(19,0) not null,
        primary key (ID_TARGET_GROUP,ID_ADDON_FILE)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt ========================================================
prompt            The audit-trail tables that Envers manages
prompt ========================================================
prompt
prompt Create table CAMS_ADDRESS_AT
prompt ===================================================
prompt
create table CAMS_ADDRESS_AT(
        ID                number(19,0) not null,
        REV               number(19,0) not null,
        REVTYPE           number(2,0),
        STREET            varchar2(50 char),
        NBR_HOUSE         varchar2(20 char),
        NBR_HOUSE_SUFFIX  varchar2(20 char),
        POSTAL_CODE       varchar2(20 char),
        CITY              varchar2(50 char),
        COUNTRY           varchar2(50 char),
        NBR_TELEPHONE     varchar2(35 char),
        NBR_FAX           varchar2(35 char),
        EMAIL_ADDRESS     varchar2(320 char),
        NAME_CONTACT      varchar2(50 char),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_PERSO_BUREAU_AT
prompt ===================================================
prompt
create table CAMS_PERSO_BUREAU_AT (
        ID                    number(19,0) not null,
        REV                   number(19,0) not null,
        REVTYPE               number(2,0),
        NAME                  varchar2(35 char),
        ID_ADDRESS            number(19,0),
        DESCRIPTION           varchar2(100 char),
        CHIP_DATA_FORMAT      varchar2(20 char),
        ID_FROM_STATE         number(19,0) not null,
        ID_TO_STATE           number(19,0) not null,
        ID_EVENT              number(19,0) not null,
        IND_ALLOW_EMPTY_DATA  number(1,0),
        IND_ENCRYPT_DATA      number(1,0),
        IND_WAREHOUSE         number(1,0),
        NAME_TRANSPORT_KEY    varchar2(25 char),
        DES_MODE              varchar2(10 char),
        IIC_VERSION           varchar2(25 char),
        IIC_WS_URL            varchar2(255 char),
        CIPHER_MODE_3DES      varchar2(10 char),
        PADDING_METHOD_PRIVATE_KEY varchar2(20 char),
        primary key (ID, REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_LIFECYCLE_AT
prompt ===================================================
prompt
create table CAMS_C_LIFECYCLE_AT (
        ID                number(19,0) not null,
        REV               number(19,0) not null,
        REVTYPE           number(2,0),
        NAME              varchar2(35 char),
        DESCRIPTION       varchar2(100 char),
        ID_TYPE_LIFECYCLE number(19,0),
        ID_GRAPH_LAYOUT   number(10,0),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_STATE_TRANSITION_AT
prompt ===================================================
prompt
create table CAMS_STATE_TRANSITION_AT (
        ID               number(19,0) not null,
        REV              number(19,0) not null,
        REVTYPE          number(2,0),
        ID_EVENT         number(19,0) ,
        ID_LIFECYCLE     number(19,0) ,
        ID_STATE_CURRENT number(19,0) ,
        ID_STATE_NEW     number(19,0) ,
        IND_MAN_PERMIT   number(1,0),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_STATE_AT
prompt ===================================================
prompt
create table CAMS_C_STATE_AT (
        ID              number(19,0) not null,
        REV             number(19,0) not null,
        REVTYPE         number(2,0),
        NAME            varchar2(35 char) ,
        DESCRIPTION     varchar2(100 char),
        CODE_STATE_BIT  number(19,0),
        IND_ACTIVE      number(1,0) ,
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_EVENT_AT
prompt ===================================================
prompt
create table CAMS_C_EVENT_AT (
        ID              number(19,0) not null,
        REV             number(19,0) not null,
        REVTYPE         number(2,0),
        NAME            varchar2(35 char),
        DESCRIPTION     varchar2(100 char),
        CODE_EVENT_BIT  number(19,0),
        IND_READONLY    number(1,0),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_TYPE_DATA_AT
prompt ===================================================
prompt
create table CAMS_C_TYPE_DATA_AT (
        ID          number(19,0) not null,
        REV         number(19,0) not null,
        REVTYPE     number(2,0),
        NAME        varchar2(50 char),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_ISSUER_AT
prompt ===================================================
prompt
create table CAMS_ISSUER_AT (
        ID                           number(19,0) not null,
        REV                          number(19,0) not null,
        REVTYPE                      number(2,0),
        ID_ADDRESS                   number(19,0),
        DESCRIPTION                  varchar2(100 char),
        NAME                         varchar2(35 char),
        ISSUER_IDENTIFICATION_NUMBER varchar2(25 char),
        ISSUER_TRACKING_NUMBER       varchar2(25 char),
        KEYSET_REFERENCE             varchar2(50 char) ,
        ID_APPLICATION_VERSION       number(19,0),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_NUMBERING_AT
prompt ===================================================
prompt
create table CAMS_NUMBERING_AT (
        ID               number(19,0) not null,
        REV              number(19,0) not null,
        REVTYPE          number(2,0),
        ID_TYPE_NBR      number(19,0),
        NAME             varchar2(35 char),
        DESCRIPTION      varchar2(100 char),
        NBR_FIRST        number(19,0),
        NBR_LAST         number(19,0),
        NBR_CURRENT      number(19,0),
        INCREMENT_SIZE   number(10,0),
        PREFIX_NUMBERING varchar2(20 char),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_C_CARDPROGRAM_AT
prompt ===================================================
prompt
create table CAMS_C_CARDPROGRAM_AT (
        ID           number(19,0) not null,
        REV          number(19,0) not null,
        REVTYPE      number(2,0),
        DESCRIPTION  varchar2(100 char),
        NAME         varchar2(35 char),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_APPV_CARDPRG_AT
prompt ===================================================
prompt
create table CAMS_X_APPV_CARDPRG_AT (
        ID                     number(19,0) not null,
        REV                    number(19,0) not null,
        REVTYPE                number(2,0),
        ID_APPLICATION_VERSION number(19,0),
        ID_OPTIONALITY         number(19,0),
        ID_CARDPROGRAM         number(19,0),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table APPLICATION_VERSION_AT
prompt ===================================================
prompt
create table CAMS_APPLICATION_VERSION_AT (
        ID                        number(19,0) not null,
        REV                       number(19,0) not null,
        REVTYPE                   number(2,0),
        ID_LIFECYCLE              number(19,0),
        ID_TYPE_APPLICATION       number(19,0),
        ID_APPLICATION            number(19,0),
        NBR_APPLICATION_VERSION   varchar2(15 char),
        DATE_START_APPLICATION    date,
        DATE_END_APPLICATION      date,
        IND_DEFAULT               number(1,0),
        SIZE_APPLICATION          number(19,0),
        PACKAGE_AID               raw(48),
        INSTANCE_AID              raw(48),
        MODULE_AID                raw(48),
        INSTALL_PARAMETER         raw(2000),
        LOAD_PARAMETER            raw(2000),
        PKI_PARAMETERS            varchar2(2000 char),
        primary key (ID,REV)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_SECURITY_CONFIG
prompt ===================================================
prompt
create table CAMS_SECURITY_CONFIG (
        ID                     number(19,0) not null,
        NAME                   varchar2(50 char) not null,
        DESCRIPTION            varchar2(100 char),
        KEYSET_REFERENCE       varchar2(50 char) not null,
        ID_APPLICATION_VERSION number(19,0) not null,
        BIN                     varchar2(15 char),
        ID_TARGET_GROUP        number(19,0),
        KIC                    raw(1),
        KID                    raw(1),
        SPI                    raw(2),
        SECURITY_CONFIG_TYPE   varchar2(100 char),
        primary key (ID),
        unique (NAME)
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_SEC_CONFIG_TYPE
prompt ===================================================
prompt
create table CAMS_SEC_CONFIG_TYPE (
        ID                     number(19,0) not null,
        NAME                   varchar2(50 char) not null,
        primary key (ID),
        unique (NAME)
)  tablespace &&tbspace_data. ;

prompt
prompt Create table CAMS_X_SEC_CONFIG_TYPE_CARD
prompt ===================================================
prompt
create table CAMS_X_SEC_CONFIG_TYPE_CARD (
        ID_SECURITY_CONFIG     number(19,0) not null,
        ID_TYPE_CARD           number(19,0) not null,
        primary key (ID_SECURITY_CONFIG, ID_TYPE_CARD)
)  tablespace &&tbspace_data. ;


prompt
prompt Create table CAMS_X_APPVER_APPVER
prompt ===================================================
prompt
create table CAMS_X_APPVER_APPVER  (
        ID_APPLICATION_VERSION             number(19,0) not null,
        ID_APPLICATION_VERSION_DEP         number(19,0) not null,
        primary key (ID_APPLICATION_VERSION, ID_APPLICATION_VERSION_DEP)  using index tablespace &&tbspace_index
)  tablespace &&tbspace_data. ;



prompt
prompt Create view CAMS_CPO_STATS_VIEW
prompt ===================================================
prompt
create view CAMS_CPO_STATS_VIEW as
            select O.ID as ID, O.NAME_PRODUCTION_ORDER as NAME, O.ID_STATE as STATE,
            /* total cards */
            (select count(*)
            from CAMS_CARD C
            where C.ID_CPO = O.ID
            ) as TOTAL_CARDS,
            /* failed cards */
            (select count(*)
            from CAMS_CARD C, CAMS_C_STATE S
            where C.ID_CPO = O.ID and C.ID_STATE = S.ID and S.NAME = 'Failed'
            ) as FAILED_CARDS,
            /* cards with failed applications */
            (select count(*)
            from CAMS_CARD C, CAMS_APPLICATION_ON_CARD AOC, CAMS_C_STATE S
            where C.ID_CPO = O.ID
            and AOC.ID_CARD = C.ID
            and AOC.ID_STATE = S.ID and S.NAME = 'Failed'
            ) as CARDS_WITH_FAILED_APPS,
            /* produced */
            (select PAT.DT_TIMESTAMP
            from CAMS_PRODUCTION_AT PAT, CAMS_C_STATE S
            where PAT.ID_CARD_PRODUCTION_ORDER = O.ID and
            PAT.ID_TO_STATE = S.ID and S.NAME = 'Produced'
            ) AS PRODUCED,
            /* Persobureau, selecting the first card it's persobureau */
            (select distinct PB.NAME
            from CAMS_CARD C, CAMS_PERSO_BUREAU PB
            where C.ID_CPO = O.ID and C.ID_PERSO_BUREAU = PB.ID
            ) AS PERSOBUREAU,
            /* cards awaiting rsponds */
            (select count(*)
            from CAMS_CARD C, CAMS_C_STATE S
            where C.ID_CPO = O.ID and C.ID_STATE = S.ID and S.NAME = 'In Production'
            ) as CARDS_AWAITING_RESPONDS,
            /* last responds at */
            (select distinct max(C.DT_LAST_UPDATE)
            from CAMS_CARD C
            where C.ID_CPO = O.ID
            ) as LAST_RESPONDS_AT,
            /* last responds at */
            (select max(PAT.DT_TIMESTAMP)
            from CAMS_PRODUCTION_AT PAT
            where PAT.ID_CARD_PRODUCTION_ORDER = O.ID
            ) as LAST_MODIFIED
            from CAMS_CARD_PRODUCTION_ORDER O;

prompt
prompt Adding Foreign Key Constraints
prompt ===================================================
prompt
alter table CAMS_ADDRESS add constraint FK_ADDRESS_COUNTRY_CODE foreign key (ID_COUNTRY_CODE) references CMN_COUNTRY_CODE(ID);
alter table CAMS_ADDON_FILE add constraint FKADNFILE_IDTYPEADNFILE foreign key (ID_TYPE_ADDON_FILE) references CAMS_C_TYPE_ADDON_FILE(ID);
alter table CAMS_APPLICATION add constraint FKAPP_IDPROVIDER foreign key (ID_PROVIDER) references CAMS_APPLICATION_PROVIDER(ID);

prompt ===========
prompt Alter table CAMS_APPLICATION_AT

alter table CAMS_APPLICATION_AT add constraint FKAPPAUD_IDAPPONCARD foreign key (ID_APPLICATION_ON_CARD) references CAMS_APPLICATION_ON_CARD(ID);
alter table CAMS_APPLICATION_AT add constraint FKAPPAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);
alter table CAMS_APPLICATION_AT add constraint FK_APPLICATION_TO_STATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_APPLICATION_AT add constraint FK_AOCAT_USRDETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_APPLICATION_ON_CARD add constraint FKAPPONCARD_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_APPLICATION_ON_CARD add constraint FKAPPONCARD_IDAPPVER foreign key (ID_APPLICATION_VERSION) references CAMS_APPLICATION_VERSION(ID);
alter table CAMS_APPLICATION_ON_CARD add constraint FKAPPONCARD_IDCARD foreign key (ID_CARD) references CAMS_CARD(ID);
alter table CAMS_APPLICATION_PROVIDER add constraint FKAPPPROVIDER_IDADDRESS foreign key (ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_APPLICATION_VERSION add constraint FKAPPVER_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_APPLICATION_VERSION add constraint FKAPPVER_IDCHIPDEF foreign key (ID_CHIP_DEFINITION) references CAMS_CHIP_DEFINITION(ID);
alter table CAMS_APPLICATION_VERSION add constraint FKAPPVER_IDTYPEAPP foreign key (ID_TYPE_APPLICATION) references CAMS_C_TYPE_APPLICATION(ID);
alter table CAMS_APPLICATION_VERSION add constraint FKAPPVER_IDAPP foreign key (ID_APPLICATION) references CAMS_APPLICATION(ID);

alter table CAMS_CARD add constraint FKCARD_IDMEMBERSHIP foreign key (ID_MEMBERSHIP) references CAMS_MEMBERSHIP(ID);
alter table CAMS_CARD add constraint FKCARD_IDCARDWITHDRAWOR foreign key (ID_CARD_WITHDRAWOR) references CMN_GROUP(ID);
alter table CAMS_CARD add constraint FKCARD_IDCARDHOLDER foreign key (ID_CARDHOLDER) references CAMS_CARDHOLDER(ID);
alter table CAMS_CARD add constraint FKCARD_IDREQUEST foreign key (ID_REQUEST) references CAMS_REQUEST(ID);
alter table CAMS_CARD add constraint FKCARD_IDFIRM foreign key (ID_FIRM) references CAMS_ORGANIZATION(ID);
alter table CAMS_CARD add constraint FKCARD_IDDEPARTMENT foreign key (ID_DEPARTMENT) references CAMS_ORGANIZATION(ID);
alter table CAMS_CARD add constraint FKCARD_IDQUOTAADMIN foreign key (ID_QUOTA_ADMINISTRATOR) references CMN_GROUP(ID);
alter table CAMS_CARD add constraint FKCARD_IDCPO foreign key (ID_CPO) references CAMS_CARD_PRODUCTION_ORDER(ID);
alter table CAMS_CARD add constraint FKCARD_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_CARD add constraint FKCARD_IDNAMEPREFERENCE foreign key (ID_NAME_PREFERENCE) references CAMS_C_NAME_PREFERENCE(ID);
alter table CAMS_CARD add constraint FKCARD_IDISSUER foreign key (ID_ISSUER) references CAMS_ISSUER(ID);
alter table CAMS_CARD add constraint FKCARD_IDTARGETGROUP foreign key (ID_TARGET_GROUP) references CAMS_TARGET_GROUP(ID);
alter table CAMS_CARD add constraint FKCARD_IDCARDPROGRAM foreign key (ID_CARDPROGRAM) references CAMS_C_CARDPROGRAM(ID);
alter table CAMS_CARD add constraint FKCARD_IDPERSOBUREAU foreign key (ID_PERSO_BUREAU) references CAMS_PERSO_BUREAU(ID);
alter table CAMS_CARD add constraint FKCARD_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARD add constraint FKCARD_IDCARDADMIN foreign key (ID_CARD_ADMINISTRATOR) references CMN_GROUP(ID);
alter table CAMS_CARD add constraint FKCARD_IDREASONCARD foreign key (ID_REASON_CARD) references CAMS_C_REASON(ID);
alter table CAMS_CARD add constraint FKCARD2IDTYPE foreign key (ID_TYPE_IDENTIFICATION) references CAMS_C_TYPE_IDENTIFICATION(ID);

alter table CAMS_CARDHOLDER add constraint FKCARDHOLDER_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARDHOLDER add constraint FKCARDHOLDER_IDADDRESS foreign key (ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_CARDHOLDER add constraint FKCARDHOLDER_IDTCHOLDER foreign key (ID_TYPE_CARDHOLDER) references CAMS_TYPE_CARDHOLDER(ID);
alter table CAMS_CARDHOLDER add constraint FK_CARDHOLDER_COUNTRY_CODE foreign key (ID_COUNTRY_CODE_BIRTH) references CMN_COUNTRY_CODE(ID);
alter table CAMS_CARDHOLDER add constraint FKCARDHOLDER_IDORGANIZATION foreign key (ID_ORGANIZATION) references CAMS_ORGANIZATION(ID);

prompt ===========
prompt Alter table CAMS_CARDHOLDER_AT

alter table CAMS_CARDHOLDER_AT add constraint FKCARDHOLDERAUD_IDCARDHOLDER foreign key (ID_CARDHOLDER) references CAMS_CARDHOLDER(ID);
alter table CAMS_CARDHOLDER_AT add constraint FKCARDHOLDERAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARDHOLDER_AT add constraint FK_CARDHOLDER_FROM_STATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARDHOLDER_AT add constraint FK_TOKENHAT_USRDETAILS foreign key (ID_AUTH_USER_DETAILS ) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_CARD_AT add constraint FKCARDAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);

prompt  ============
prompt alter Table CAMS_CARD_AT
alter table CAMS_CARD_AT add constraint FK_CARD_FROM_STATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARD_AT add constraint FK_TOKENAT_USRDETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_CARD_AT add constraint FKCARDAUD_IDCARD foreign key (ID_CARD) references CAMS_CARD(ID);
alter table CAMS_TYPE_CARD_IN_STOCK add constraint FK_TYPE_IN_STOCK_SUPPLIER foreign key (ID_SUPPLIER) references CAMS_SUPPLIER(ID);
alter table CAMS_TYPE_CARD_IN_STOCK add constraint FK_TYPE_IN_STOCK_LIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_TYPE_CARD_IN_STOCK add constraint FK_TYPE_IN_STOCK_CARD_FAMILY foreign key (ID_CARD_FAMILY) references CAMS_CARD_FAMILY(ID);

alter table CAMS_CARD_IN_STOCK add constraint FKCARDINSTOK_IDSTOKADMIN foreign key (ID_STOCK_ADMINISTRATOR) references CMN_GROUP(ID);
alter table CAMS_CARD_IN_STOCK add constraint FKCARDINSTOK_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARD_IN_STOCK add constraint FKCARDINSTOK_IDBATCH foreign key (ID_BATCH) references CAMS_STOCK_BATCH(ID);
alter table CAMS_CARD_IN_STOCK add constraint FK_TYPE_CARD_IN_STOCK_TYPE foreign key (ID_TYPE_CARD_IN_STOCK) references CAMS_TYPE_CARD_IN_STOCK(ID);
alter table CAMS_CARD_PRODUCTION_ORDER add constraint FKCARDPRODORDER_IDTYPECPO foreign key (ID_TYPE_CPO) references CAMS_C_TYPE_CPO(ID);
alter table CAMS_CARD_PRODUCTION_ORDER add constraint FKCARDPRODORDER_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARD_PRODUCTION_ORDER add constraint FKCARDPRODORDER_IDSTCKORD foreign key (ID_STOCK_ORDER) references CAMS_STOCK_ORDER(ID);

prompt  ============
prompt alter Table CAMS_CARD_STOCK_AT
alter table CAMS_CARD_STOCK_AT add constraint FKCARDSTOKAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARD_STOCK_AT add constraint FK_CARD_STOCK_FROM_STATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_CARD_STOCK_AT add constraint FKCARDSTOKAUD_IDCARDINSTOK foreign key (ID_CARD_IN_STOCK) references CAMS_CARD_IN_STOCK(ID);
alter table CAMS_CARD_STOCK_AT add constraint FK_STOCKAT_USERDETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_CHIP_DEFINITION add constraint FKCHIPDEF_IDOS foreign key (ID_OPERATING_SYSTEM) references CAMS_C_CHIP_OS(ID);
alter table CAMS_C_LIFECYCLE add constraint FKCLIFECYCLE_IDTYPELIFECYCLE foreign key (ID_TYPE_LIFECYCLE) references CAMS_C_TYPE_DATA(ID);
alter table CAMS_C_LIFECYCLE add constraint FKLCGRAPHLAYOUT foreign key (ID_GRAPH_LAYOUT) references CMN_ANDIS_BLOB(ID);
alter table CAMS_C_REASON add constraint FKCREASON_IDTYPEREASON foreign key (ID_TYPE_REASON) references CAMS_C_TYPE_REASON(ID);
alter table CAMS_C_TYPE_ADDON_FILE add constraint FKCTYPEADNFILE_IDTBLNAME foreign key (ID_TABLE_NAME) references CMN_TABLE_NAME(ID);
alter table CAMS_C_TYPE_CPO add constraint FKCTYPECPO_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_DEFINITION_VARIABLE add constraint FKDEFVAR_IDTYPEVAR foreign key (ID_TYPE_VARIABLE) references CAMS_C_TYPE_VARIABLE(ID);
alter table CAMS_DEFINITION_VARIABLE add constraint FKDEFVAR_IDNUMBERING foreign key (ID_NUMBERING) references CAMS_NUMBERING(ID);
alter table CAMS_DEFINITION_VARIABLE add constraint FKDEFVAR_IDTBLVALUE foreign key (ID_TABLE_VALUE) references CMN_TABLE_NAME(ID);
alter table CAMS_DEFINITION_VARIABLE add constraint FKDEFVAR_IDTBLCONFIG foreign key (ID_TABLE_CONFIG) references CMN_TABLE_NAME(ID);
alter table CAMS_DEFINITION_VARIABLE add constraint FKDEFVAR_IDTYPECODETBL foreign key (ID_TYPE_CODE_TABLE) references CAMS_TYPE_CODE_TABLE(ID);
alter table CAMS_EMV_TEMPLATE add constraint FKEMVTEMPLATE_IDAPPVER foreign key (ID_APPLICATION_VERSION) references CAMS_APPLICATION_VERSION(ID);
alter table CAMS_EMV_TEMPLATE add constraint FKEMV_TEMPLATE_IDPERSOBUREAU foreign key (ID_PERSO_BUREAU) references CAMS_PERSO_BUREAU(ID);
alter table CAMS_FAILED_MESSAGE add constraint FKFAILEDMESSAGE_IDEXTAPPL foreign key (ID_EXTERNAL_APPL) references CAMS_EXTERNAL_APPL(ID);
alter table CAMS_ISSUER add constraint FKISSUER_IDADDRESS foreign key (ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_ISSUER add constraint FKISSUERLOGO foreign key (ID_LOGO) references CMN_ANDIS_BLOB(ID);
alter table CAMS_KEYSTORE add constraint FKKEYSTORE_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_MEMBERSHIP add constraint FKMEMBERSHIP_IDCARDHOLDER foreign key (ID_CARDHOLDER) references CAMS_CARDHOLDER(ID);
alter table CAMS_MEMBERSHIP add constraint FKMBRSHIP2SRVENTITY foreign key (ID_SRV_ENTITY) references CAMS_SRV_ENTITY(ID);
alter table CAMS_MEMBERSHIP add constraint FKMEMBERSHIP_IDTARGETGROUP foreign key (ID_TARGET_GROUP) references CAMS_TARGET_GROUP(ID);
alter table CAMS_MEMBERSHIP add constraint FKMEMBERSHIP_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);

prompt  ============
prompt alter Table CAMS_MEMBERSHIP_AT
alter table CAMS_MEMBERSHIP_AT add constraint FKMEMBERSHIPAUD_IDMEMBERSHIP foreign key (ID_MEMBERSHIP) references CAMS_MEMBERSHIP(ID);
alter table CAMS_MEMBERSHIP_AT add constraint FKMEMBERSHIPAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);
alter table CAMS_MEMBERSHIP_AT add constraint FK_MEMBERSHIP_FROM_STATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_MEMBERSHIP_AT add constraint FK_MBRSHAT_USRDETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_NUMBERING add constraint FKNUMBERING_IDTYPENBR foreign key (ID_TYPE_NBR) references CAMS_C_TYPE_NUMBER(ID);
alter table CAMS_ORGANIZATION add constraint FKORG_IDPARENTORG foreign key (ID_PARENT_ORGANIZATION) references CAMS_ORGANIZATION(ID);
alter table CAMS_ORGANIZATION add constraint FKORG_IDADDRESS foreign key (ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_ORGANIZATION add constraint FK_ORGANIZATION_CORR_ADDR foreign key (ID_CORRESPONDENCE_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_ORGANIZATION add constraint FKORG_IDTYPEORG foreign key (ID_TYPE_ORGANIZATION) references CAMS_C_TYPE_ORGANIZATION(ID);
alter table CAMS_ORGANIZATION add constraint FKORGANIZATIONLOGO foreign key (ID_LOGO) references CMN_ANDIS_BLOB(ID);

prompt  ============
prompt alter Table CAMS_PRODUCTION_AT

alter table CAMS_PRODUCTION_AT add constraint FKPRODAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);
alter table CAMS_PRODUCTION_AT add constraint FK_PRODUCTION_FROM_STATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_PRODUCTION_AT add constraint FKPRODAUD_IDCARDPRODORDER foreign key (ID_CARD_PRODUCTION_ORDER) references CAMS_CARD_PRODUCTION_ORDER(ID);
alter table CAMS_PRODUCTION_AT add constraint FK_TPOAT_USRDETAILS foreign key (ID_AUTH_USER_DETAILS ) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_RECORD_CODE_TABLE add constraint FKRECORDCODETBL_IDTYPECODETBL foreign key (ID_TYPE_CODE_TABLE) references CAMS_TYPE_CODE_TABLE(ID);
alter table CAMS_REQUEST add constraint FKREQUEST_IDCARDAUTHORIZER foreign key (ID_CARD_AUTHORIZER) references CMN_GROUP(ID);
alter table CAMS_REQUEST add constraint FKREQUEST_IDCARDREQUESTOR foreign key (ID_CARD_REQUESTOR) references CMN_GROUP(ID);
alter table CAMS_REQUEST add constraint FKREQUEST_IDCARDDISTRIBUTOR foreign key (ID_CARD_DISTRIBUTOR) references CMN_GROUP(ID);
alter table CAMS_REQUEST add constraint FKREQUEST_IDREASONREQUEST foreign key (ID_REASON_REQUEST) references CAMS_C_REASON(ID);
alter table CAMS_REQUEST add constraint FKREQUEST_IDTYPEIDENTIFICATION foreign key (ID_TYPE_IDENTIFICATION) references CAMS_C_TYPE_IDENTIFICATION(ID);
alter table CAMS_REQUEST_SETTINGS add constraint FKREQSET_IDDFLTCARDDISTRIBUTOR foreign key (ID_DEFAULT_CARD_DISTRIBUTOR) references CMN_GROUP(ID);
alter table CAMS_REQUEST_SETTINGS add constraint FKREQSET_IDDFLTCARDADMIN foreign key (ID_DEFAULT_CARD_ADMINISTRATOR) references CMN_GROUP(ID);
alter table CAMS_REQUEST_SETTINGS add constraint FKREQSET_IDDFLTQUOTAADMIN foreign key (ID_DEFAULT_QUOTA_ADMINISTRATOR) references CMN_GROUP(ID);
alter table CAMS_REQUEST_SETTINGS add constraint FKREQSET_IDDFLTAUTHORIZER foreign key (ID_DEFAULT_AUTHORIZER) references CMN_GROUP(ID);
alter table CAMS_STATE_TRANSITION add constraint FKSTATETRANSITION_IDSTATENEW foreign key (ID_STATE_NEW) references CAMS_C_STATE(ID);
alter table CAMS_STATE_TRANSITION add constraint FKSTATETRANSITION_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_STATE_TRANSITION add constraint FKSTATETRANSITION_IDEVENT foreign key (ID_EVENT) references CAMS_C_EVENT(ID);
alter table CAMS_STATE_TRANSITION add constraint FKSTATETRANSITION_STATECURRENT foreign key (ID_STATE_CURRENT) references CAMS_C_STATE(ID);
alter table CAMS_STOCK_BATCH add constraint FKSTOKBATCH_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_STOCK_BATCH add constraint FKSTOKBATCH_IDCARDFAM foreign key (ID_CARD_FAMILY) references CAMS_CARD_FAMILY(ID);
alter table CAMS_STOCK_BATCH add constraint FKSTOKBATCH_IDSUPPLIER foreign key (ID_SUPPLIER) references CAMS_SUPPLIER(ID);
alter table CAMS_SUBSCRIPTION_RULE add constraint FKSUBSCRRULE_IDSTATENEW foreign key (ID_STATE_NEW) references CAMS_C_STATE(ID);
alter table CAMS_SUBSCRIPTION_RULE add constraint FKSUBSCRRULE_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_SUBSCRIPTION_RULE add constraint FKSUBSCRRULE_IDEVENT foreign key (ID_EVENT) references CAMS_C_EVENT(ID);
alter table CAMS_SUBSCRIPTION_RULE add constraint FKSUBSCRRULE_IDSTATECURRENT foreign key (ID_STATE_CURRENT) references CAMS_C_STATE(ID);
alter table CAMS_SUPPLIER add constraint FKSUPPLIER_IDADNFILE foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE(ID);
alter table CAMS_SUPPLIER add constraint FKSUPPLIER_IDADDRESS foreign key (ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_TARGET_GROUP add constraint FKTARGETGROUP_IDISSUER foreign key (ID_ISSUER) references CAMS_ISSUER(ID);
alter table CAMS_TARGET_GROUP add constraint FKTARGETGROUP_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_STOCK_LOCATION add constraint FK86E359F927799150 foreign key (ID_STOCK_LOCATION_TYPE) references CAMS_C_STOCK_LOCATION_TYPE(ID);
alter table CAMS_STOCK add constraint FKSTOCK_IDSTOCKTYPE foreign key (ID_STOCK_ITEM) references CAMS_STOCK_ITEM(ID);
alter table CAMS_STOCK add constraint FKSTOCK_IDSTOCKLOCATION foreign key (ID_STOCK_LOCATION) references CAMS_STOCK_LOCATION(ID);
alter table CAMS_STOCK_ORDER add constraint FKSTOCKORDER_IDSTOCKTYPE foreign key (ID_STOCK_ITEM) references CAMS_STOCK_ITEM(ID);
alter table CAMS_STOCK_ORDER add constraint FKSTOCKORDER_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_STOCK_ORDER add constraint FK7E5B8B6A7AE26DE6 foreign key (ID_STOCK_LOCATION_FROM) references CAMS_STOCK_LOCATION(ID);
alter table CAMS_STOCK_ORDER add constraint FK7E5B8B6A859C2FF7 foreign key (ID_STOCK_LOCATION_TO) references CAMS_STOCK_LOCATION(ID);
alter table CAMS_STOCK_ORDER add constraint FKSTOCKORDERSUPPLIER foreign key (ID_SUPPLIER) references CAMS_SUPPLIER(ID);

prompt  ============
prompt alter Table CAMS_STOCK_ORDER_AT
alter table CAMS_STOCK_ORDER_AT add constraint FKSTOCKORDERAUD_IDTOSTATE foreign key (ID_TO_STATE) references CAMS_C_STATE(ID);
alter table CAMS_STOCK_ORDER_AT add constraint FKSTOCKORDERAUD_IDEVENT foreign key (ID_EVENT) references CAMS_C_EVENT(ID);
alter table CAMS_STOCK_ORDER_AT add constraint FKSTOCKORDERAUD_IDFROMSTATE foreign key (ID_FROM_STATE) references CAMS_C_STATE(ID);
alter table CAMS_STOCK_ORDER_AT add constraint FK_STOCKORD_USERDETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_STOCK_ORDER_AT add constraint FKSTOCKORDERAU_IDSTOCKORDER foreign key (ID_STOCK_ORDER) references CAMS_STOCK_ORDER(ID);
alter table CAMS_STOCK_LOCATION_AT add constraint FKSTOCKLOCAU_IDSTOCKLOC foreign key (ID_STOCK_LOCATION) references CAMS_STOCK_LOCATION(ID);
alter table CAMS_STOCK_ITEM_AT add constraint FKSTOCKITEMAUD_IDSTOCKITEM foreign key (ID_STOCK_ITEM) references CAMS_STOCK_ITEM(ID);
alter table CAMS_C_STOCK_ITEM_TYPE add constraint FKSTOCKITEMTYPE_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_STOCK_ITEM add constraint FKSTOCKITEM_IDSTOCKITEMTYPE foreign key (ID_STOCK_ITEM_TYPE) references CAMS_C_STOCK_ITEM_TYPE(ID);
alter table CAMS_STOCK_ITEM add constraint FKSTOCKITEM_IDSTATE foreign key (ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_STOCK_ITEM add constraint FKSTOCKITEM_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_TYPE_CARD add constraint FKTC_IDSTOCKITEM foreign key (ID_STOCK_ITEM) references CAMS_STOCK_ITEM(ID);
alter table CAMS_TYPE_CARD add constraint FKTC_IDNUMBERING foreign key (ID_NUMBERING) references CAMS_NUMBERING(ID);
alter table CAMS_TYPE_CARD add constraint FKTC_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_TYPE_CARD add constraint FKTC_IDCARDFAM foreign key (ID_CARD_FAMILY) references CAMS_CARD_FAMILY(ID);
alter table CAMS_TYPE_CARD add constraint FKTC_IDEVENTCODEGENERATION foreign key (ID_EVENT_CODE_GENERATION) references CAMS_C_EVENT(ID);
alter table CAMS_TYPE_CARD add constraint FKTYPECARDLAYOUT foreign key (ID_CARD_LAYOUT_IMAGE) references CMN_ANDIS_BLOB(ID);
alter table CAMS_TYPE_CARDHOLDER add constraint FKTCHOLDER_IDNUMBERING foreign key (ID_NUMBERING) references CAMS_NUMBERING(ID);
alter table CAMS_TYPE_CARDHOLDER add constraint FKTCHOLDER_IDLIFECYCLE foreign key (ID_LIFECYCLE) references CAMS_C_LIFECYCLE(ID);
alter table CAMS_VARIABLE_CONFIG add constraint FKVARCONFIG_IDDEFVAR foreign key (ID_DEFINITION_VARIABLE) references CAMS_DEFINITION_VARIABLE(ID);
alter table CAMS_VARIABLE_VALUE add constraint FKVARVALUE_IDDEFVAR foreign key (ID_DEFINITION_VARIABLE) references CAMS_DEFINITION_VARIABLE(ID);
alter table CAMS_VARIABLE_VALUE add constraint FKBINARYVALUE foreign key (ID_BINARY_VALUE) references CMN_ANDIS_BLOB(ID);
alter table CAMS_SRV_ENTITY add constraint SRVENTITYADDRESS foreign key(ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_SRV_ENTITY add constraint SRVENTITYSTATE foreign key(ID_STATE) references CAMS_C_STATE(ID);
alter table CAMS_SRV_ENTITY add constraint SRVENTITYTYPE foreign key(ID_TYPE) references CAMS_TYPE_CARDHOLDER(ID);
alter table CAMS_X_APPV_CARDPRG add constraint FKXAPPVCARDPRG_IDOPTIONALITY foreign key (ID_OPTIONALITY) references CAMS_C_APP_OPTIONALITY(ID);
alter table CAMS_X_APPV_CARDPRG add constraint FKXAPPVCARDPRG_IDCARDPROGRAM foreign key (ID_CARDPROGRAM) references CAMS_C_CARDPROGRAM(ID);
alter table CAMS_X_APPV_CARDPRG add constraint FKXAPPVCARDPRG_IDAPPVER foreign key (ID_APPLICATION_VERSION) references CAMS_APPLICATION_VERSION(ID);
alter table CAMS_X_APP_VER_ADDON_FILE add constraint FKXAPPVERADNFILE_IDAPPVER foreign key (ID_APPLICATION_VERSION) references CAMS_APPLICATION_VERSION(ID);
alter table CAMS_X_APP_VER_ADDON_FILE add constraint FKXAPPVERADNFILE_IDADNFILE foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE(ID);
alter table CAMS_X_STOCK_ITEM_SUPPLIER add constraint FKX_STOCK_ITEM_IDSUPPLIER foreign key (ID_SUPPLIER) references CAMS_SUPPLIER(ID);
alter table CAMS_X_STOCK_ITEM_SUPPLIER add constraint FKX_SUPPLIER_IDSTOCKTYPE foreign key (ID_STOCK_ITEM) references CAMS_STOCK_ITEM(ID);
alter table CAMS_X_APP_VER_CRYPTO_REQ add constraint FKXAPPVERCRYPREQ_IDCRYPREQT foreign key (ID_CRYPTO_REQUIREMENT) references CAMS_C_CRYPTO_REQ(ID);
alter table CAMS_X_APP_VER_CRYPTO_REQ add constraint FKXAPPVERCRYPREQ_IDAPPVER foreign key (ID_APPLICATION_VERSION) references CAMS_APPLICATION_VERSION(ID);
alter table CAMS_X_APP_VER_CRYPTO_REQ add constraint FKXAPPVERCRYPREQ_IDCRYPREQ foreign key (ID_CRYPTO_REQ) references CAMS_C_CRYPTO_REQ(ID);
alter table CAMS_X_CARDPROGRAM_TYPE_CARD add constraint FKXCARDPROGRAMTC_IDCARDPROGRAM foreign key (ID_CARDPROGRAM) references CAMS_C_CARDPROGRAM(ID);
alter table CAMS_X_CARDPROGRAM_TYPE_CARD add constraint FKXCARDPROGRAMTC_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_X_CARD_FAMILY_ADDON_FILE add constraint FKXCARDFAMADNFILE_IDCARDFAM foreign key (ID_CARD_FAMILY) references CAMS_CARD_FAMILY(ID);
alter table CAMS_X_CARD_FAMILY_ADDON_FILE add constraint FKXCARDFAMADNFILE_IDADNFILE foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE(ID);
alter table CAMS_X_CARD_ORGANIZATION add constraint FKXCARDORG_IDORG foreign key (ID_ORGANIZATION) references CAMS_ORGANIZATION(ID);
alter table CAMS_X_CARD_ORGANIZATION add constraint FKXCARDORG_IDCARD foreign key (ID_CARD) references CAMS_CARD(ID);
alter table CAMS_X_CRYPTO_REQ_CHIP_DEF add constraint FKXCRYPREQCHIPDEF_IDCHIPDEFN foreign key (ID_CHIP_DEFINITION) references CAMS_CHIP_DEFINITION(ID);
alter table CAMS_X_CRYPTO_REQ_CHIP_DEF add constraint FKXCRYPREQCHIPDEF_IDCRYPREQT foreign key (ID_CRYPTO_REQUIREMENT) references CAMS_C_CRYPTO_REQ(ID);
alter table CAMS_X_CRYPTO_REQ_CHIP_DEF add constraint FKXCRYPREQCHIPDEF_IDCHIPDEF foreign key (ID_CHIP_DEF) references CAMS_CHIP_DEFINITION(ID);
alter table CAMS_X_CRYPTO_REQ_CHIP_DEF add constraint FKXCRYPREQCHIPDEF_IDCRYPREQ foreign key (ID_CRYPTO_REQ) references CAMS_C_CRYPTO_REQ(ID);
alter table CAMS_X_TG_EMV_TEMPLATE add constraint FK_CAMS_TG_EMV_TEMPLATE_ID_TG foreign key (ID_TARGET_GROUP) references CAMS_TARGET_GROUP(ID);
alter table CAMS_X_TG_EMV_TEMPLATE add constraint FK_CAMS_TG_EMV_TEMPLATE_ID_ET foreign key (ID_EMV_TEMPLATE) references CAMS_EMV_TEMPLATE(ID);
alter table CAMS_X_TG_ADDON_FILE add constraint FK_CAMS_TG_ADDON_FILE_ID_TG foreign key (ID_TARGET_GROUP) references CAMS_TARGET_GROUP(ID);
alter table CAMS_X_TG_ADDON_FILE add constraint FK_CAMS_TG_ADDON_FILE_ID_AOF foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE(ID);
alter table CAMS_X_TYPE_APPL_CHIP_OS add constraint FKXTYPEAPPLCHIPOS_IDOS foreign key (ID_OPERATING_SYSTEM) references CAMS_C_CHIP_OS(ID);
alter table CAMS_X_TYPE_APPL_CHIP_OS add constraint FKXTYPEAPPLCHIPOS_IDTYPEAPP foreign key (ID_TYPE_APPLICATION) references CAMS_C_TYPE_APPLICATION(ID);
alter table CAMS_X_TYPE_CARD_ADDON_FILE add constraint FKXTCADNFILE_IDADNFILE foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE(ID);
alter table CAMS_X_TYPE_CARD_ADDON_FILE add constraint FKXTCADNFILE_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_X_TYPE_CARD_CHIP_DEF add constraint FKXTCCHIPDEF_IDCHIPDEF foreign key (ID_CHIP_DEFINITION) references CAMS_CHIP_DEFINITION(ID);
alter table CAMS_X_TYPE_CARD_CHIP_DEF add constraint FKXTCCHIPDEF_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_X_TYPE_CARD_C_REASON add constraint FKXTCCREASON_IDREASON foreign key (ID_REASON) references CAMS_C_REASON(ID);
alter table CAMS_X_TYPE_CARD_C_REASON add constraint FKXTCCREASON_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_X_TYPE_CARD_TARGET_GROUP add constraint FKXTCTARGETGROUP_IDTARGETGROUP foreign key (ID_TARGET_GROUP) references CAMS_TARGET_GROUP(ID);
alter table CAMS_X_TYPE_CARD_TARGET_GROUP add constraint FKXTCTARGETGROUP_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_C_TYPE_ORGANIZATION add constraint FK_C_TYPE_ORGANIZATION_PARENT foreign key (ID_TYPE_PARENT_ORGANIZATION) references CAMS_C_TYPE_ORGANIZATION(ID);
alter table CAMS_X_TYPE_CARD_TYPE_ORG add constraint FKXTCTYPEORG_IDTYPEORG foreign key (ID_TYPE_ORGANIZATION) references CAMS_C_TYPE_ORGANIZATION(ID);
alter table CAMS_X_TYPE_CARD_TYPE_ORG add constraint FKXTCTYPEORG_IDTC foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);
alter table CAMS_X_EXTAPP_SUBSCRRULE add constraint FKXEXTAPPSUBSCRRULE_IDEXTAPPL foreign key (ID_EXTERNAL_APPL) references CAMS_EXTERNAL_APPL(ID);
alter table CAMS_X_EXTAPP_SUBSCRRULE add constraint FKXEXTAPPSUBSCRRULE_SUBSCRRULE foreign key (ID_SUBSCRIPTION_RULE) references CAMS_SUBSCRIPTION_RULE(ID);
alter table CAMS_SECURITY_CONFIG add constraint FK_SEC_CONFIG_TARGET_GROUP foreign key (ID_TARGET_GROUP) references CAMS_TARGET_GROUP(ID);
alter table CAMS_SECURITY_CONFIG add constraint FK_SEC_CONFIG_APP_VERSION foreign key (ID_APPLICATION_VERSION) references CAMS_APPLICATION_VERSION(ID);
alter table CAMS_SECURITY_CONFIG add constraint FK_SEC_CONFIG_TYPE foreign key (SECURITY_CONFIG_TYPE) references CAMS_SEC_CONFIG_TYPE (NAME);
alter table CAMS_X_SEC_CONFIG_TYPE_CARD add constraint FK_X_SEC_CONFIG_TYPE_CARD_SEC foreign key (ID_SECURITY_CONFIG) references CAMS_SECURITY_CONFIG(ID);
alter table CAMS_X_SEC_CONFIG_TYPE_CARD add constraint FK_X_SEC_CONFIG_TYPE_CARD_TYPE foreign key (ID_TYPE_CARD) references CAMS_TYPE_CARD(ID);

alter table CAMS_C_TYPE_BIOMETRIC_DATA add
(  constraint FK_TYPE_BIOMETRIC_APPLET foreign key (ID_CAPTURE_APPLET_DEFINITION) references CMN_APPLET_DEFINITION (id));

alter table CAMS_C_BIOMETRIC_APPLET_PARAM add
(  constraint FK_BIOMETRIC_APPLET_P_PARAM foreign key (ID_APPLET_PARAMETER) references CMN_APPLET_PARAMETER (id),
  constraint FK_BIOMETRIC_APPLET_P_TYPE foreign key (ID_TYPE_BIOMETRIC_DATA) references CAMS_C_TYPE_BIOMETRIC_DATA (id));

alter table CAMS_CARDHOLDER_BIOMETRIC add
(  constraint FK_CH_BIOMETRIC_CH foreign key (ID_CARDHOLDER) references CAMS_CARDHOLDER (id),
  constraint FK_CH_BIOMETRIC_TYPE foreign key (ID_TYPE_BIOMETRIC_DATA) references CAMS_C_TYPE_BIOMETRIC_DATA (id),
  constraint FK_CH_BIOMETRIC_REASON foreign key (ID_CAPTURE_EVENT_REASON) references CAMS_C_REASON (id),
  constraint FK_CH_BINARY_DATA_BLOB foreign key (ID_BINARY_DATA) references CMN_ANDIS_BLOB (id));

alter table CAMS_X_BIOMETRIC_EVENT add
(  constraint FK_BIOMETRIC_EVENT_EV foreign key (ID_EVENT) references CAMS_C_EVENT (id),
  constraint FK_BIOMETRIC_EVENT_TYPE foreign key (ID_TYPE_BIOMETRIC_DATA) references CAMS_C_TYPE_BIOMETRIC_DATA (id));

alter table CAMS_PERSO_BUREAU add constraint FKPERSOBUREAU_IDADDRESS foreign key (ID_ADDRESS) references CAMS_ADDRESS(ID);
alter table CAMS_CARD_PRINTER add constraint FKCARDPRINTER_IDPERSOBUREAU foreign key (ID_PERSO_BUREAU) references CAMS_PERSO_BUREAU(ID);
alter table CAMS_CARD_PRINTER add constraint FKCARDPRINTER_IDADNFILE foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE(ID);
alter table CAMS_CARD_PRINTER_LOG add constraint FKCARDPRINTERLOG_IDCARDPRINTER foreign key (ID_CARD_PRINTER) references CAMS_CARD_PRINTER(ID);
alter table CAMS_CARD_PRINTER_LOG add constraint FKCARDPRINTERLOG_IDCARD foreign key (ID_CARD) references CAMS_CARD(ID);
alter table CAMS_APPLICATION add constraint FKAPP_IDLOGO foreign key (ID_LOGO) references CMN_ANDIS_BLOB(ID);
alter table CAMS_SRV_ENTITY_AT add constraint FK_SRVENTAT_USERDETAILS foreign key (ID_AUTH_USER_DETAILS) references CMN_AUTH_USER_DETAILS(ID);
alter table CAMS_X_TYPE_CSTOCK_ADDON_FILE add constraint FK_JO7RL1OG5OY1OSOPYIGN1OODR foreign key (ID_TYPE_CARD_IN_STOCK) references CAMS_TYPE_CARD_IN_STOCK (ID);
alter table CAMS_X_TYPE_CSTOCK_ADDON_FILE add constraint FK_SMPQ5M6WA0EMB2I8F951D8GN5 foreign key (ID_ADDON_FILE) references CAMS_ADDON_FILE (ID);

prompt
prompt Creating foreign key indexes
prompt ===================================================
prompt

create index IXFKADNFILE_IDTYPEADNFILE on CAMS_ADDON_FILE ( ID_TYPE_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFK_ADDRESS_COUNTRY_CODE on CAMS_ADDRESS ( ID_COUNTRY_CODE ) tablespace &&tbspace_index;
create index IXFKAPP_IDLOGO on CAMS_APPLICATION ( ID_LOGO ) tablespace &&tbspace_index;
create index IXFKAPP_IDPROVIDER on CAMS_APPLICATION ( ID_PROVIDER ) tablespace &&tbspace_index;
create index IXFKAPPAUD_IDAPPONCARD on CAMS_APPLICATION_AT ( ID_APPLICATION_ON_CARD ) tablespace &&tbspace_index;
create index IXFK_AOCAT_USRDETAILS on CAMS_APPLICATION_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFK_APPLICATION_TO_STATE on CAMS_APPLICATION_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFKAPPAUD_IDTOSTATE on CAMS_APPLICATION_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKAPPONCARD_IDSTATE on CAMS_APPLICATION_ON_CARD ( ID_STATE ) tablespace &&tbspace_index;
create index IXFKAPPONCARD_IDCARD on CAMS_APPLICATION_ON_CARD ( ID_CARD ) tablespace &&tbspace_index;
create index IXFKAPPONCARD_IDAPPVER on CAMS_APPLICATION_ON_CARD ( ID_APPLICATION_VERSION ) tablespace &&tbspace_index;
create index IXFKAPPPROVIDER_IDADDRESS on CAMS_APPLICATION_PROVIDER ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXFKAPPVER_IDTYPEAPP on CAMS_APPLICATION_VERSION ( ID_TYPE_APPLICATION ) tablespace &&tbspace_index;
create index IXFKAPPVER_IDAPP on CAMS_APPLICATION_VERSION ( ID_APPLICATION ) tablespace &&tbspace_index;
create index IXFKAPPVER_IDLIFECYCLE on CAMS_APPLICATION_VERSION ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKAPPVER_IDCHIPDEF on CAMS_APPLICATION_VERSION ( ID_CHIP_DEFINITION ) tablespace &&tbspace_index;
create index IXFKCARD_IDPERSOBUREAU on CAMS_CARD ( ID_PERSO_BUREAU ) tablespace &&tbspace_index;
create index IXFKCARD_IDTC on CAMS_CARD ( ID_TYPE_CARD ) tablespace &&tbspace_index;
create index IXFKCARD_IDCARDWITHDRAWOR on CAMS_CARD ( ID_CARD_WITHDRAWOR ) tablespace &&tbspace_index;
create index IXFKCARD_IDMEMBERSHIP on CAMS_CARD ( ID_MEMBERSHIP ) tablespace &&tbspace_index;
create index IXFKCARD_IDCARDADMIN on CAMS_CARD ( ID_CARD_ADMINISTRATOR ) tablespace &&tbspace_index;
create index IXFKCARD_IDDEPARTMENT on CAMS_CARD ( ID_DEPARTMENT ) tablespace &&tbspace_index;
create index IXFKCARD_IDCARDHOLDER on CAMS_CARD ( ID_CARDHOLDER ) tablespace &&tbspace_index;
create index IXFKCARD_IDTARGETGROUP on CAMS_CARD ( ID_TARGET_GROUP ) tablespace &&tbspace_index;
create index IXFKCARD_IDNAMEPREFERENCE on CAMS_CARD ( ID_NAME_PREFERENCE ) tablespace &&tbspace_index;
create index IXFKCARD_IDCPO on CAMS_CARD ( ID_CPO ) tablespace &&tbspace_index;
create index IXFKCARD_IDISSUER on CAMS_CARD ( ID_ISSUER ) tablespace &&tbspace_index;
create index IXFKCARD_IDREQUEST on CAMS_CARD ( ID_REQUEST ) tablespace &&tbspace_index;
create index IXFKCARD_IDCARDPROGRAM on CAMS_CARD ( ID_CARDPROGRAM ) tablespace &&tbspace_index;
create index IXFKCARD_IDQUOTAADMIN on CAMS_CARD ( ID_QUOTA_ADMINISTRATOR ) tablespace &&tbspace_index;
create index IXFKCARD_IDFIRM on CAMS_CARD ( ID_FIRM ) tablespace &&tbspace_index;
create index IXFKCARD2IDTYPE on CAMS_CARD ( ID_TYPE_IDENTIFICATION ) tablespace &&tbspace_index;
create index IXFKCARD_IDSTATE on CAMS_CARD ( ID_STATE ) tablespace &&tbspace_index;
create index IXFKCARD_IDREASONCARD on CAMS_CARD ( ID_REASON_CARD ) tablespace &&tbspace_index;
create index IDXCARD_CODECARDEXTERNAL on CAMS_CARD (CODE_CARD_EXTERNAL) tablespace &&tbspace_index;
--create index IDXCARD_CODE_CARD on CAMS_CARD( CODE_CARD ) tablespace &&tbspace_index;
create index IXFK_CARD_FROM_STATE on CAMS_CARD_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFKCARDAUD_IDCARD on CAMS_CARD_AT ( ID_CARD ) tablespace &&tbspace_index;
create index IXFK_TOKENAT_USRDETAILS on CAMS_CARD_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFKCARDAUD_IDTOSTATE on CAMS_CARD_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKCARDHOLDER_IDADDRESS on CAMS_CARDHOLDER ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXFKCARDHOLDER_IDTCHOLDER on CAMS_CARDHOLDER ( ID_TYPE_CARDHOLDER ) tablespace &&tbspace_index;
create index IXFK_CARDHOLDER_COUNTRY_CODE on CAMS_CARDHOLDER ( ID_COUNTRY_CODE_BIRTH ) tablespace &&tbspace_index;
create index IXFKCARDHOLDER_IDORGANIZATION on CAMS_CARDHOLDER ( ID_ORGANIZATION ) tablespace &&tbspace_index;
create index IXFKCARDHOLDER_IDSTATE on CAMS_CARDHOLDER ( ID_STATE ) tablespace &&tbspace_index;
--create index IDXCARDHOLDER_CODE_CARDH on CAMS_CARDHOLDER( CODE_CARDHOLDER ) tablespace &&tbspace_index;
create index IXFK_CARDHOLDER_FROM_STATE on CAMS_CARDHOLDER_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFKCARDHOLDERAUD_IDTOSTATE on CAMS_CARDHOLDER_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKCARDHOLDERAUD_IDCARDHOLDER on CAMS_CARDHOLDER_AT ( ID_CARDHOLDER ) tablespace &&tbspace_index;
create index IXFK_TOKENHAT_USRDETAILS on CAMS_CARDHOLDER_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFK_CH_BIOMETRIC_TYPE on CAMS_CARDHOLDER_BIOMETRIC ( ID_TYPE_BIOMETRIC_DATA ) tablespace &&tbspace_index;
create index IXFK_CH_BIOMETRIC_REASON on CAMS_CARDHOLDER_BIOMETRIC ( ID_CAPTURE_EVENT_REASON ) tablespace &&tbspace_index;
create index IXFK_CH_BINARY_DATA_BLOB on CAMS_CARDHOLDER_BIOMETRIC ( ID_BINARY_DATA ) tablespace &&tbspace_index;
create index IXFK_TYPE_CARD_IN_STOCK_TYPE on CAMS_CARD_IN_STOCK ( ID_TYPE_CARD_IN_STOCK ) tablespace &&tbspace_index;
create index IXFKCARDINSTOK_IDBATCH on CAMS_CARD_IN_STOCK ( ID_BATCH ) tablespace &&tbspace_index;
create index IXFKCARDINSTOK_IDSTOKADMIN on CAMS_CARD_IN_STOCK ( ID_STOCK_ADMINISTRATOR ) tablespace &&tbspace_index;
create index IXFKCARDINSTOK_IDSTATE on CAMS_CARD_IN_STOCK ( ID_STATE ) tablespace &&tbspace_index;
create index IXFKCARDPRINTER_IDPERSOBUREAU on CAMS_CARD_PRINTER ( ID_PERSO_BUREAU ) tablespace &&tbspace_index;
create index IXFKCARDPRINTER_IDADNFILE on CAMS_CARD_PRINTER ( ID_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFKCARDPRINTERLOG_IDCARDPRINT on CAMS_CARD_PRINTER_LOG ( ID_CARD_PRINTER ) tablespace &&tbspace_index;
create index IXFKCARDPRINTERLOG_IDCARD on CAMS_CARD_PRINTER_LOG ( ID_CARD ) tablespace &&tbspace_index;
create index IXFKCARDPRODORDER_IDTYPECPO on CAMS_CARD_PRODUCTION_ORDER ( ID_TYPE_CPO ) tablespace &&tbspace_index;
create index IXFKCARDPRODORDER_IDSTATE on CAMS_CARD_PRODUCTION_ORDER ( ID_STATE ) tablespace &&tbspace_index;
create index IDXCARDPRODORDER_NAME on CAMS_CARD_PRODUCTION_ORDER ( NAME_PRODUCTION_ORDER ) tablespace &&tbspace_index;
create index IXFK_STOCKAT_USERDETAILS on CAMS_CARD_STOCK_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFK_CARD_STOCK_FROM_STATE on CAMS_CARD_STOCK_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFKCARDSTOKAUD_IDTOSTATE on CAMS_CARD_STOCK_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKCARDSTOKAUD_IDCARDINSTOK on CAMS_CARD_STOCK_AT ( ID_CARD_IN_STOCK ) tablespace &&tbspace_index;
create index IXFK_BIOMETRIC_APPLET_P_PARAM on CAMS_C_BIOMETRIC_APPLET_PARAM ( ID_APPLET_PARAMETER ) tablespace &&tbspace_index;
create index IXFKCHIPDEF_IDOS on CAMS_CHIP_DEFINITION ( ID_OPERATING_SYSTEM ) tablespace &&tbspace_index;
create index IXFKLCGRAPHLAYOUT on CAMS_C_LIFECYCLE ( ID_GRAPH_LAYOUT ) tablespace &&tbspace_index;
create index IXFKCLIFECYCLE_IDTYPELIFECYCLE on CAMS_C_LIFECYCLE ( ID_TYPE_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKCREASON_IDTYPEREASON on CAMS_C_REASON ( ID_TYPE_REASON ) tablespace &&tbspace_index;
create index IDXREASON_DESC on CAMS_C_REASON ( DESCRIPTION ) tablespace &&tbspace_index;
create index IDXTYPEREASON_DESC on CAMS_C_TYPE_REASON ( DESCRIPTION ) tablespace &&tbspace_index;
create index IXFKCTYPEADNFILE_IDTBLNAME on CAMS_C_TYPE_ADDON_FILE ( ID_TABLE_NAME ) tablespace &&tbspace_index;
create index IXFK_TYPE_BIOMETRIC_APPLET on CAMS_C_TYPE_BIOMETRIC_DATA ( ID_CAPTURE_APPLET_DEFINITION ) tablespace &&tbspace_index;
create index IXFKCTYPECPO_IDLIFECYCLE on CAMS_C_TYPE_CPO ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFK_C_TYPE_ORGANIZATION_PAREN on CAMS_C_TYPE_ORGANIZATION ( ID_TYPE_PARENT_ORGANIZATION ) tablespace &&tbspace_index;
create index IXFKDEFVAR_IDTBLCONFIG on CAMS_DEFINITION_VARIABLE ( ID_TABLE_CONFIG ) tablespace &&tbspace_index;
create index IXFKDEFVAR_IDTYPECODETBL on CAMS_DEFINITION_VARIABLE ( ID_TYPE_CODE_TABLE ) tablespace &&tbspace_index;
create index IXFKDEFVAR_IDTYPEVAR on CAMS_DEFINITION_VARIABLE ( ID_TYPE_VARIABLE ) tablespace &&tbspace_index;
create index IXFKDEFVAR_IDTBLVALUE on CAMS_DEFINITION_VARIABLE ( ID_TABLE_VALUE ) tablespace &&tbspace_index;
create index IXFKDEFVAR_IDNUMBERING on CAMS_DEFINITION_VARIABLE ( ID_NUMBERING ) tablespace &&tbspace_index;
create index IXFKEMVTEMPLATE_IDAPPVER on CAMS_EMV_TEMPLATE ( ID_APPLICATION_VERSION ) tablespace &&tbspace_index;
create index IXFKISSUER_IDADDRESS on CAMS_ISSUER ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXFKISSUERLOGO on CAMS_ISSUER ( ID_LOGO ) tablespace &&tbspace_index;
create index IXFKKEYSTORE_IDSTATE on CAMS_KEYSTORE ( ID_STATE ) tablespace &&tbspace_index;
create index IXFKMEMBERSHIP_IDTARGETGROUP on CAMS_MEMBERSHIP ( ID_TARGET_GROUP ) tablespace &&tbspace_index;
create index IXFKMBRSHIP2SRVENTITY on CAMS_MEMBERSHIP ( ID_SRV_ENTITY ) tablespace &&tbspace_index;
create index IXFKMEMBERSHIP_IDSTATE on CAMS_MEMBERSHIP ( ID_STATE ) tablespace &&tbspace_index;
create index IXFKMEMBERSHIP_IDCARDHOLDER on CAMS_MEMBERSHIP ( ID_CARDHOLDER ) tablespace &&tbspace_index;
create index IXFK_MBRSHAT_USRDETAILS on CAMS_MEMBERSHIP_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFK_MEMBERSHIP_FROM_STATE on CAMS_MEMBERSHIP_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFKMEMBERSHIPAUD_IDTOSTATE on CAMS_MEMBERSHIP_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKMEMBERSHIPAUD_IDMEMBERSHIP on CAMS_MEMBERSHIP_AT ( ID_MEMBERSHIP ) tablespace &&tbspace_index;
create index IXFKNUMBERING_IDTYPENBR on CAMS_NUMBERING ( ID_TYPE_NBR ) tablespace &&tbspace_index;
create index IXFKORGANIZATIONLOGO on CAMS_ORGANIZATION ( ID_LOGO ) tablespace &&tbspace_index;
create index IXFKORG_IDPARENTORG on CAMS_ORGANIZATION ( ID_PARENT_ORGANIZATION ) tablespace &&tbspace_index;
create index IXFK_ORGANIZATION_CORR_ADDR on CAMS_ORGANIZATION ( ID_CORRESPONDENCE_ADDRESS ) tablespace &&tbspace_index;
create index IXFKORG_IDADDRESS on CAMS_ORGANIZATION ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXFKORG_IDTYPEORG on CAMS_ORGANIZATION ( ID_TYPE_ORGANIZATION ) tablespace &&tbspace_index;
create index IXFKPERSOBUREAU_IDADDRESS on CAMS_PERSO_BUREAU ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXFKPRODAUD_IDCARDPRODORDER on CAMS_PRODUCTION_AT ( ID_CARD_PRODUCTION_ORDER ) tablespace &&tbspace_index;
create index IXFK_TPOAT_USRDETAILS on CAMS_PRODUCTION_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFK_PRODUCTION_FROM_STATE on CAMS_PRODUCTION_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFKPRODAUD_IDTOSTATE on CAMS_PRODUCTION_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKRECORDCODETBL_IDTYPECODETB on CAMS_RECORD_CODE_TABLE ( ID_TYPE_CODE_TABLE ) tablespace &&tbspace_index;
create index IXFKREQUEST_IDREASONREQUEST on CAMS_REQUEST ( ID_REASON_REQUEST ) tablespace &&tbspace_index;
create index IXFKREQUEST_IDCARDDISTRIBUTOR on CAMS_REQUEST ( ID_CARD_DISTRIBUTOR ) tablespace &&tbspace_index;
create index IXFKREQUEST_IDCARDAUTHORIZER on CAMS_REQUEST ( ID_CARD_AUTHORIZER ) tablespace &&tbspace_index;
create index IXFKREQUEST_IDCARDREQUESTOR on CAMS_REQUEST ( ID_CARD_REQUESTOR ) tablespace &&tbspace_index;
create index IXFKREQUEST_IDTYPEIDENTIFICATI on CAMS_REQUEST ( ID_TYPE_IDENTIFICATION ) tablespace &&tbspace_index;
create index IXFKREQSET_IDDFLTAUTHORIZER on CAMS_REQUEST_SETTINGS ( ID_DEFAULT_AUTHORIZER ) tablespace &&tbspace_index;
create index IXFKREQSET_IDDFLTCARDDISTRIBUT on CAMS_REQUEST_SETTINGS ( ID_DEFAULT_CARD_DISTRIBUTOR ) tablespace &&tbspace_index;
create index IXFKREQSET_IDDFLTQUOTAADMIN on CAMS_REQUEST_SETTINGS ( ID_DEFAULT_QUOTA_ADMINISTRATOR ) tablespace &&tbspace_index;
create index IXFKREQSET_IDDFLTCARDADMIN on CAMS_REQUEST_SETTINGS ( ID_DEFAULT_CARD_ADMINISTRATOR ) tablespace &&tbspace_index;
create index IXSRVENTITYTYPE on CAMS_SRV_ENTITY ( ID_TYPE ) tablespace &&tbspace_index;
create index IXSRVENTITYADDRESS on CAMS_SRV_ENTITY ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXSRVENTITYSTATE on CAMS_SRV_ENTITY ( ID_STATE ) tablespace &&tbspace_index;
create index IXFK_SRVENTAT_USERDETAILS on CAMS_SRV_ENTITY_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFKSTATETRANSITION_IDSTATENEW on CAMS_STATE_TRANSITION ( ID_STATE_NEW ) tablespace &&tbspace_index;
create index IXFKSTATETRANSITION_STATECURRE on CAMS_STATE_TRANSITION ( ID_STATE_CURRENT ) tablespace &&tbspace_index;
create index IXFKSTATETRANSITION_IDLIFECYCL on CAMS_STATE_TRANSITION ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKSTOCK_IDSTOCKITEM on CAMS_STOCK ( ID_STOCK_ITEM ) tablespace &&tbspace_index;
create index IXFKSTOCK_IDSTOCKLOCATION on CAMS_STOCK ( ID_STOCK_LOCATION ) tablespace &&tbspace_index;
create index IXFKSTOKBATCH_IDSUPPLIER on CAMS_STOCK_BATCH ( ID_SUPPLIER ) tablespace &&tbspace_index;
create index IXFKSTOKBATCH_IDLIFECYCLE on CAMS_STOCK_BATCH ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKSTOKBATCH_IDCARDFAM   on CAMS_STOCK_BATCH ( ID_CARD_FAMILY ) tablespace &&tbspace_index;
create index IXFKSTOCKORDER_LOCATION_FROM on CAMS_STOCK_ORDER ( ID_STOCK_LOCATION_FROM ) tablespace &&tbspace_index;
create index IXFKSTOCKORDER_LOCATION_TO   on CAMS_STOCK_ORDER ( ID_STOCK_LOCATION_TO ) tablespace &&tbspace_index;
create index IXFKSTOCKORDER_IDSTOCKITEM   on CAMS_STOCK_ORDER ( ID_STOCK_ITEM ) tablespace &&tbspace_index;
create index IXFKSTOCKORDER_IDSTATE on CAMS_STOCK_ORDER ( ID_STATE ) tablespace &&tbspace_index;
create index IXFKSTOCKORDERAUD_IDTOSTATE on CAMS_STOCK_ORDER_AT ( ID_TO_STATE ) tablespace &&tbspace_index;
create index IXFKSTOCKORDERAUD_IDSTOKORDER on CAMS_STOCK_ORDER_AT ( ID_STOCK_ORDER ) tablespace &&tbspace_index;
create index IXFKSTOCKORDERAUD_IDEVENT on CAMS_STOCK_ORDER_AT ( ID_EVENT ) tablespace &&tbspace_index;
create index IXFKSTOCKORDERAUD_IDFROMSTATE on CAMS_STOCK_ORDER_AT ( ID_FROM_STATE ) tablespace &&tbspace_index;
create index IXFK_STOCKORD_USERDETAILS on CAMS_STOCK_ORDER_AT ( ID_AUTH_USER_DETAILS ) tablespace &&tbspace_index;
create index IXFKSTOCKITEM_IDLIFECYCLE on CAMS_STOCK_ITEM ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKSUBSCRRULE_IDSTATENEW on CAMS_SUBSCRIPTION_RULE ( ID_STATE_NEW ) tablespace &&tbspace_index;
create index IXFKSUBSCRRULE_IDSTATECURRENT on CAMS_SUBSCRIPTION_RULE ( ID_STATE_CURRENT ) tablespace &&tbspace_index;
create index IXFKSUBSCRRULE_IDEVENT on CAMS_SUBSCRIPTION_RULE ( ID_EVENT ) tablespace &&tbspace_index;
create index IXFKSUBSCRRULE_IDLIFECYCLE on CAMS_SUBSCRIPTION_RULE ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKSUPPLIER_IDADDRESS on CAMS_SUPPLIER ( ID_ADDRESS ) tablespace &&tbspace_index;
create index IXFKSUPPLIER_IDADNFILE on CAMS_SUPPLIER ( ID_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFKTARGETGROUP_IDISSUER on CAMS_TARGET_GROUP ( ID_ISSUER ) tablespace &&tbspace_index;
create index IXFKTARGETGROUP_IDLIFECYCLE on CAMS_TARGET_GROUP ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKTYPECARDLAYOUT on CAMS_TYPE_CARD ( ID_CARD_LAYOUT_IMAGE ) tablespace &&tbspace_index;
create index IXFKTC_IDEVENTCODEGENERATION on CAMS_TYPE_CARD ( ID_EVENT_CODE_GENERATION ) tablespace &&tbspace_index;
create index IXFKTC_IDLIFECYCLE on CAMS_TYPE_CARD ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKTC_IDSTOCKITEM on CAMS_TYPE_CARD ( ID_STOCK_ITEM ) tablespace &&tbspace_index;
create index IXFKTC_IDNUMBERING on CAMS_TYPE_CARD ( ID_NUMBERING ) tablespace &&tbspace_index;
create index IXFKTC_IDCARDFAM on CAMS_TYPE_CARD ( ID_CARD_FAMILY ) tablespace &&tbspace_index;
create index IXFKTCHOLDER_IDLIFECYCLE on CAMS_TYPE_CARDHOLDER ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFKTCHOLDER_IDNUMBERING on CAMS_TYPE_CARDHOLDER ( ID_NUMBERING ) tablespace &&tbspace_index;
create index IXFK_TYPE_IN_STOCK_LIFECYCLE on CAMS_TYPE_CARD_IN_STOCK ( ID_LIFECYCLE ) tablespace &&tbspace_index;
create index IXFK_TYPE_IN_STOCK_SUPPLIER on CAMS_TYPE_CARD_IN_STOCK ( ID_SUPPLIER ) tablespace &&tbspace_index;
create index IXFK_TYPE_IN_STOCK_CARD_FAMILY on CAMS_TYPE_CARD_IN_STOCK ( ID_CARD_FAMILY ) tablespace &&tbspace_index;
create index IXFKBINARYVALUE on CAMS_VARIABLE_VALUE ( ID_BINARY_VALUE ) tablespace &&tbspace_index;
create index IXFKXAPPVCARDPRG_IDCARDPROGRAM on CAMS_X_APPV_CARDPRG ( ID_CARDPROGRAM ) tablespace &&tbspace_index;
create index IXFKXAPPVCARDPRG_IDAPPVER on CAMS_X_APPV_CARDPRG ( ID_APPLICATION_VERSION ) tablespace &&tbspace_index;
create index IXFKXAPPVCARDPRG_IDOPTIONALITY on CAMS_X_APPV_CARDPRG ( ID_OPTIONALITY ) tablespace &&tbspace_index;
create index IXFKXAPPVERADNFILE_IDADNFILE on CAMS_X_APP_VER_ADDON_FILE ( ID_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFKXAPPVERCRYPREQ_IDCRYPREQ on CAMS_X_APP_VER_CRYPTO_REQ ( ID_CRYPTO_REQ ) tablespace &&tbspace_index;
create index IXFKXAPPVERCRYPREQ_IDCRYPREQT on CAMS_X_APP_VER_CRYPTO_REQ ( ID_CRYPTO_REQUIREMENT ) tablespace &&tbspace_index;
create index IXFK_BIOMETRIC_EVENT_TYPE on CAMS_X_BIOMETRIC_EVENT ( ID_TYPE_BIOMETRIC_DATA ) tablespace &&tbspace_index;
--create index IDXBIOMETRICEVENT_IDEVENT on CAMS_X_BIOMETRIC_EVENT ( ID_EVENT ) tablespace &&tbspace_index;
create index IXFKXCARDFAMADNFILE_IDADNFILE on CAMS_X_CARD_FAMILY_ADDON_FILE ( ID_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFKXCARDORG_IDORG on CAMS_X_CARD_ORGANIZATION ( ID_ORGANIZATION ) tablespace &&tbspace_index;
create index IXFKXCARDPROGRAMTC_IDTC on CAMS_X_CARDPROGRAM_TYPE_CARD ( ID_TYPE_CARD ) tablespace &&tbspace_index;
create index IXFKXCRYPREQCHIPDEF_IDCHIPDEF on CAMS_X_CRYPTO_REQ_CHIP_DEF ( ID_CHIP_DEF ) tablespace &&tbspace_index;
create index IXFKXCRYPREQCHIPDEF_IDCRYPREQT on CAMS_X_CRYPTO_REQ_CHIP_DEF ( ID_CRYPTO_REQUIREMENT ) tablespace &&tbspace_index;
create index IXFKXCRYPREQCHIPDEF_IDCHIPDEFN on CAMS_X_CRYPTO_REQ_CHIP_DEF ( ID_CHIP_DEFINITION ) tablespace &&tbspace_index;
create index IXFKXEXTAPPSUBSCRRULE_SUBSCRRU on CAMS_X_EXTAPP_SUBSCRRULE ( ID_SUBSCRIPTION_RULE ) tablespace &&tbspace_index;
create index IXFK_CAMS_TG_ADDON_FILE_ID_AOF on CAMS_X_TG_ADDON_FILE ( ID_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFK_CAMS_TG_EMV_TEMPLATE_ID_E on CAMS_X_TG_EMV_TEMPLATE ( ID_EMV_TEMPLATE ) tablespace &&tbspace_index;
create index IXFKXTYPEAPPLCHIPOS_IDTYPEAPP on CAMS_X_TYPE_APPL_CHIP_OS ( ID_TYPE_APPLICATION ) tablespace &&tbspace_index;
create index IXFKXTCADNFILE_IDADNFILE on CAMS_X_TYPE_CARD_ADDON_FILE ( ID_ADDON_FILE ) tablespace &&tbspace_index;
create index IXFKXTCCHIPDEF_IDTC on CAMS_X_TYPE_CARD_CHIP_DEF ( ID_TYPE_CARD ) tablespace &&tbspace_index;
create index IXFKXTCCREASON_IDTC on CAMS_X_TYPE_CARD_C_REASON ( ID_TYPE_CARD ) tablespace &&tbspace_index;
create index IXFKXTCTARGETGROUP_IDTARGETGRO on CAMS_X_TYPE_CARD_TARGET_GROUP ( ID_TARGET_GROUP ) tablespace &&tbspace_index;
create index IXFKXTCTYPEORG_IDTYPEORG on CAMS_X_TYPE_CARD_TYPE_ORG ( ID_TYPE_ORGANIZATION ) tablespace &&tbspace_index;
create index CODE_CARDHOLDER_EXTERNAL on CAMS_CARDHOLDER (CODE_CARDHOLDER_EXTERNAL) tablespace &&tbspace_index;
create index creation_date on CAMS_FAILED_MESSAGE (DT_CREATED) tablespace &&tbspace_index;
create index external_appl on CAMS_FAILED_MESSAGE (ID_EXTERNAL_APPL) tablespace &&tbspace_index;
create index IXFK_CPO_STOCK_ORDER on CAMS_CARD_PRODUCTION_ORDER (ID_STOCK_ORDER) tablespace &&tbspace_index.;
create index IXFK_STOCK_ITEM_LIFECYCLE on CAMS_C_STOCK_ITEM_TYPE (ID_LIFECYCLE) tablespace &&tbspace_index.;
create index IXFK_EMV_TMPLATE_PERSO_BUREAU on CAMS_EMV_TEMPLATE (ID_PERSO_BUREAU) tablespace &&tbspace_index.;
create index IXFK_SEC_CONFIG_APP_VER on CAMS_SECURITY_CONFIG (ID_APPLICATION_VERSION) tablespace &&tbspace_index.;
create index IXFK_SECURITY_CONFIG_SC_TYPE on CAMS_SECURITY_CONFIG (SECURITY_CONFIG_TYPE) tablespace &&tbspace_index.;
create index IXFK_SECURITY_CONFIG_TG on CAMS_SECURITY_CONFIG (ID_TARGET_GROUP) tablespace &&tbspace_index.;
create index IXFK_STOCKITEM_STOCKITEMTYPE on CAMS_STOCK_ITEM (ID_STOCK_ITEM_TYPE) tablespace &&tbspace_index.;
create index IXFK_STOCKITEM_STATE on CAMS_STOCK_ITEM (ID_STATE) tablespace &&tbspace_index.;
create index IXFK_STOCKITEM_AT_STOCK_ITEM on CAMS_STOCK_ITEM_AT (ID_STOCK_ITEM) tablespace &&tbspace_index.;
create index IXFK_STOCKLOCATION_LOCTYPE on CAMS_STOCK_LOCATION (ID_STOCK_LOCATION_TYPE) tablespace &&tbspace_index.;
create index IXFK_STOCKLCTN_AT_LCTN on CAMS_STOCK_LOCATION_AT (ID_STOCK_LOCATION) tablespace &&tbspace_index.;
create index IXFK_STOCKORDER_SUPPLIER on CAMS_STOCK_ORDER (ID_SUPPLIER) tablespace &&tbspace_index.;
create index IXFK_X_BIOMETRIC_EVENT_EVENT on CAMS_X_BIOMETRIC_EVENT (ID_EVENT) tablespace &&tbspace_index.;
create index IXKF_X_SEC_CONFIG_RTPE_CARD on CAMS_X_SEC_CONFIG_TYPE_CARD (ID_TYPE_CARD) tablespace &&tbspace_index.;
create index IXFK_XSTOCKITEMSUPPLR_SUPPLR on CAMS_X_STOCK_ITEM_SUPPLIER (ID_SUPPLIER) tablespace &&tbspace_index.;
create index IXFK_X_TYPE_CSTOCK_ADDON_FILE on CAMS_X_TYPE_CSTOCK_ADDON_FILE (ID_ADDON_FILE) tablespace &&tbspace_index.;


prompt
prompt Creating sequences
prompt ===================================================
prompt

create sequence CAMS_ADDON_FILE_SEQ start with 10000 increment by 1;
create sequence CAMS_ADDRESS_SEQ start with 10000 increment by 50;
create sequence CAMS_APPLICATION_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_APPLICATION_ON_CARD_SEQ start with 10000 increment by 50;
create sequence CAMS_APPLICATION_PROVIDER_SEQ start with 10000 increment by 1;
create sequence CAMS_APPLICATION_SEQ start with 10000 increment by 1;
create sequence CAMS_APPLICATION_VERSION_SEQ start with 10000 increment by 1;
create sequence CAMS_CARDHOLDER_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_CARDHOLDER_SEQ start with 10000 increment by 50;
create sequence CAMS_CARD_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_CARD_FAMILY_SEQ start with 10000 increment by 1;
create sequence CAMS_TYPE_CARD_IN_STOCK_SEQ start with 10000 increment by 1;
create sequence CAMS_CARD_IN_STOCK_SEQ start with 10000 increment by 50;
create sequence CAMS_CARD_PRODUCTION_ORDER_SEQ start with 10000 increment by 50;
create sequence CAMS_CARD_SEQ start with 10000 increment by 50;
create sequence CAMS_CARD_STOCK_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_CHIP_DEFINITION_SEQ start with 10000 increment by 1;
create sequence CAMS_C_APP_OPTIONALITY_SEQ start with 10000 increment by 1;
create sequence CAMS_C_CARDPROGRAM_SEQ start with 10000 increment by 1;
create sequence CAMS_C_CHIP_OS_SEQ start with 10000 increment by 1;
create sequence CAMS_C_CRYPTO_REQ_SEQ start with 10000 increment by 1;
create sequence CAMS_C_EVENT_SEQ start with 10000 increment by 1;
create sequence CAMS_C_LIFECYCLE_SEQ start with 10000 increment by 1;
create sequence CAMS_C_NAME_PREFERENCE_SEQ start with 10000 increment by 1;
create sequence CAMS_C_REASON_SEQ start with 10000 increment by 1;
create sequence CAMS_C_STATE_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_ADDON_FILE_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_APPLICATION_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_CPO_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_DATA_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_IDENTIFICATION_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_NUMBER_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_ORGANIZATION_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_REASON_SEQ start with 10000 increment by 1;
create sequence CAMS_C_TYPE_VARIABLE_SEQ start with 10000 increment by 1;
create sequence CAMS_DEFINITION_VARIABLE_SEQ start with 10000 increment by 1;
create sequence CAMS_EMV_TEMPLATE_SEQ start with 10000 increment by 1;
create sequence CAMS_EXTERNAL_APPL_SEQ start with 10000 increment by 1;
create sequence CAMS_FAILED_MESSAGE_SEQ start with 10000 increment by 1;
create sequence CAMS_ISSUER_SEQ start with 10000 increment by 1;
create sequence CAMS_KEYSTORE_SEQ start with 10000 increment by 1;
create sequence CAMS_MEMBERSHIP_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_MEMBERSHIP_SEQ start with 10000 increment by 50;
create sequence CAMS_NUMBERING_SEQ start with 10000 increment by 1;
create sequence CAMS_ORGANIZATION_SEQ start with 10000 increment by 1;
create sequence CAMS_PERSO_BUREAU_SEQ start with 10000 increment by 1;
create sequence CAMS_CARD_PRINTER_SEQ start with 10000 increment by 1;
create sequence CAMS_CARD_PRINTER_LOG_SEQ start with 10000 increment by 50;
create sequence CAMS_PRODUCTION_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_STOCK_ORDER_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_STOCK_ORDER_SEQ start with 10000 increment by 50;
create sequence CAMS_C_STOCK_ITEM_TYPE_SEQ start with 10000 increment by 1;
create sequence CAMS_STOCK_LOCATION_SEQ start with 10000 increment by 1;
create sequence CAMS_STOCK_LOCATION_AT_SEQ start with 10000 increment by 1;
create sequence CAMS_STOCK_ITEM_AT_SEQ start with 10000 increment by 50;
create sequence CAMS_C_STOCK_LOCATION_TYPE_SEQ start with 10000 increment by 1;
create sequence CAMS_STOCK_ITEM_SEQ start with 10000 increment by 50;
create sequence CAMS_STOCK_SEQ start with 10000 increment by 50;
create sequence CAMS_C_TYPE_DIO_SEQ start with 10000 increment by 1;
create sequence CAMS_RECORD_CODE_TABLE_SEQ start with 10000 increment by 1;
create sequence CAMS_REQUEST_SEQ start with 10000 increment by 50;
create sequence CAMS_STATE_TRANSITION_SEQ start with 10000 increment by 1;
create sequence CAMS_STOCK_BATCH_SEQ start with 10000 increment by 50;
create sequence CAMS_SUBSCRIPTION_RULE_SEQ start with 10000 increment by 1;
create sequence CAMS_SUPPLIER_SEQ start with 10000 increment by 1;
create sequence CAMS_TARGET_GROUP_SEQ start with 10000 increment by 1;
create sequence CAMS_TYPE_CARDHOLDER_SEQ start with 10000 increment by 1;
create sequence CAMS_TYPE_CARD_SEQ start with 10000 increment by 1;
create sequence CAMS_TYPE_CODE_TABLE_SEQ start with 10000 increment by 1;
create sequence CAMS_VARIABLE_CONFIG_SEQ start with 10000 increment by 1;
create sequence CAMS_VARIABLE_VALUE_SEQ start with 10000 increment by 50;
create sequence CAMS_X_APP_CARDPROGRAM_SEQ start with 10000 increment by 1;
create sequence CAMS_X_TYPE_CARD_C_REASON_SEQ start with 10000 increment by 1;
create sequence CAMS_X_TYPE_CARD_TYPE_ORG_SEQ start with 10000 increment by 1;
create sequence CAMS_ICC_CERT_SERIAL_NBR_SEQ start with 10000 increment by 50;
create sequence CAMS_C_TYPE_BIOMETRIC_DATA_SEQ start with 10000 increment by 1;
create sequence CAMS_X_BIOMETRIC_EVENT_SEQ  start with 10000 increment by 1;
create sequence CAMS_CARDHOLDER_BIOMETRIC_SEQ start with 10000 increment by 50;
create sequence CAMS_C_BIOM_APPLET_PARAM_SEQ start with 10000 increment by 1;
create sequence CAMS_SECURITY_CONFIG_SEQ start with 10000 increment by 1;
create sequence CAMS_SEC_CONFIG_TYPE_SEQ start with 10000 increment by 1;
create sequence CAMS_SRV_ENTITY_SEQ    start with 10000 increment by 50;
create sequence CAMS_SRV_ENTITY_AT_SEQ  start with 10000 increment by 50;
