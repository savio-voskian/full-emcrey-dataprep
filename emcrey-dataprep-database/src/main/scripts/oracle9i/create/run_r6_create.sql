--===================================================================================
--Description     : Script to create and prepare Release 6 database
--Version	  : $Id: $
--Version date    : $Date$
--Last changed by : $Author$
--Usage						:
--	Run this script using following command:
--		sqlplus /nolog script_file prameter_file
--		where:
--			script_file - the fullpath file name to run_r6_create
--			parameter_file  - the file name used for  input parameters
--			r6_create_param.sql is a interactive script for input
--			r6_create_param_silent.sql is used for silent instalation
--		  e.g.
--		  - interactive installation
--		go to your local Development directory, most likely d:\Development
--		cd database\src\main\scripts\oracle9i\create
--		  sqlplus /nolog @run_r6_create.sql r6_create_param.sql
--       	- silent instalation
--
--===================================================================================
--                               (c) 2009 Bell ID B.V.
--===================================================================================

SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERRor  EXIT
WHENEVER SQLERRor EXIT SQL.SQLCODE

prompt ;
prompt ;
prompt ==================================================================================;
prompt C R E A T E    C R E A T E    R E L E A S E  6     D A T A B A S E;
prompt ==================================================================================;
prompt ;


-- define default schema placeholder
DEFINE BELLID_R6_SCHEMA = 'TO_BE_REPLACED_BY_PARAM';
-- define default password placeholders
DEFINE BELLID_R6_SCHEMA_PASSWORD = '';
DEFINE BELLID_R6_APP_USER_PASSWORD = '';
DEFINE BELLID_R6_MI_USER_PASSWORD = '';
-- define flag to say whether the MI Role is needed. Default is yes. This will be overridden by Jenkins only
DEFINE CREATE_MI_ROLE = 'Y';

-- define placeholder for the list of solution flavours to install. This will override the default settings for the project installation
DEFINE OVERRIDE_SOLUTION_FLAVOURS = 'DEFAULT';

-- defining these parameters for back-ward compatibility with solutions/projects.
DEFINE create_acs='Y';
DEFINE create_kms='Y';
DEFINE create_tm='Y';

--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line
--***********************************************************************************
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1;


--**********************************************************************************
-- start creating database objects
--***********************************************************************************
@@connect_dba_user.sql
@@override_passwords.sql;

spool &&logdrive/run_r6_create.log;

prompt
prompt Drop existing &&BELLID_R6_SCHEMA. and related objects
prompt ===================================================
prompt


--clean up first
declare mysql varchar2(2000);
begin
for c in (select role from dba_roles where role IN (upper('&&RUN_BELLID_R6_SCHEMA' ),upper('&&READ_BELLID_R6_MI' )))
loop
   mysql:= 'DROP ROLE '|| c.role;
   execute immediate mysql;
end loop;
for c in (select username from dba_users where username in (upper('&&BELLID_R6_APP_USER'),upper('&&BELLID_R6_MI_USER'),upper('&&BELLID_R6_SCHEMA')) order by user_id desc)
loop
   mysql:= 'DROP USER ' ||  c.username || ' CASCADE ' ;
   execute immediate mysql;
end loop;
end;
/

prompt
prompt Create user &&BELLID_R6_SCHEMA. and related objects
prompt ===================================================
prompt

-- Create the user
create user &&BELLID_R6_SCHEMA.
  identified by "&&BELLID_R6_SCHEMA_PASSWORD."
  default tablespace &&tbspace_data
  temporary tablespace &&tbspace_temp
  profile DEFAULT
  quota unlimited on &&tbspace_data
  quota unlimited on &&tbspace_index;
  
column create_acs_col NEW_VALUE create_acs1 NOPRINT
column create_kms_col NEW_VALUE create_kms1 NOPRINT
column create_tm_col NEW_VALUE create_tm1 NOPRINT
 
select case when upper('&&create_acs.')='Y' or upper('&&create_tm.')='Y' or upper('&&create_kms.')='Y' then 'common_acs' else 'common_no' end as create_acs_col,
       case when upper('&&create_kms.')='Y' or upper('&&create_tm.')='Y' then 'common_kms' else 'common_no' end as create_kms_col,
       case when upper('&&create_tm.')='Y' then 'common_tm' else 'common_no' end as create_tm_col
from dual;

prompt
prompt Create user &&BELLID_R6_SCHEMA. common schema and related objects
prompt ===================================================
prompt
@@common_core_schema.sql

@@&create_acs1._schema.sql
@@&create_kms1._schema.sql
@@&create_tm1._schema.sql

prompt
prompt Create user &&BELLID_R6_SCHEMA. quartz schema and related objects
prompt ===================================================
prompt
@@quartz_schema.sql

prompt
prompt Create user &&BELLID_R6_SCHEMA. solution schema and related objects
prompt ===================================================
prompt
@@solution_schema.sql

prompt
prompt Create solution flavour schema scripts
prompt ===================================================
prompt
@@solution_flavours.sql
@@optional_solution_flavour_scripts.sql
@@solution_flavour_schema.sql

prompt
prompt Old solution flavour overriding changes
prompt ===================================================
prompt
@@override_solution_flavours.sql

prompt
prompt Create user &&BELLID_R6_SCHEMA. poject schema and related objects
prompt ===================================================
prompt
@@project_schema.sql

--create role and application user
@@create_app_user_r6.sql
--insert blob data
prompt inserting blob data;
@@insert_blobdata
--insert config data
prompt inserting config data;
@@insert_configdata

--insert configuration data
spool &&logdrive/r6_insert.log;
ALTER SESSION SET CURRENT_SCHEMA = &&BELLID_R6_SCHEMA.;
@@common_core_insert.sql;
@@&create_acs1._insert.sql
@@&create_kms1._insert.sql
@@&create_tm1._insert.sql

@@solution_insert.sql;
@@solution_flavour_insert.sql;
@@project_insert.sql;
commit;
spool off;

spool &&logdrive/run_r6_create.log APPEND;
prompt advancing sequences;
@@advanceSequencesBeyondDataValues

--Insert Database Version details to Audit Table
prompt
prompt 'Creating new database of version &&MY_DB_VERSION';
prompt ===================================================
prompt
INSERT INTO CMN_DB_SCRIPT_AUDIT ( ID, TARGET_DB, DB_VERSION, SCRIPT, DESCRIPTION, REVISION, DT_EXECUTION, EXECUTOR ) 
VALUES
( CMN_DB_SCRIPT_AUDIT_SEQ.NEXTVAL, '&&BELLID_R6_SCHEMA', '&&MY_DB_VERSION', 'run_r6_create.sql', 'Setting Master Create info', '&&MY_REVISION', SYSDATE, '&&MY_USER_NAME' );
COMMIT;

prompt ;
prompt ;
prompt ==================================================================================;
prompt E N D   C R E A T E    R E L E A S E  6     D A T A B A S E;
prompt ==================================================================================;
prompt ;
spool off;
exit;
