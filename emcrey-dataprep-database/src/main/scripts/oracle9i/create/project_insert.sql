--===================================================================================
--
--Description     : Script to insert project specific conguration data for Release 6 database
--                  This script is a place holder. It will be overwritten by project-specific
--                  script in the future
--
--Version date    : August 29, 2011
--
--===================================================================================
--                               (c) 2011 Bell ID B.V.
--===================================================================================
