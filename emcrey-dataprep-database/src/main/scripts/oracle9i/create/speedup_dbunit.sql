--*********************************************************************************
-- call input parameters screen &1 the first parameter from command line
--***********************************************************************************
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1;

SPOOL &&logdrive/speedup_dbunit.log;

@@connect_dba_user.sql

----- all_cons_columns
drop table &&BELLID_R6_APP_USER..all_cons_columns;
create table &&BELLID_R6_APP_USER..all_cons_columns_temp as
select * from sys.all_cons_columns c where owner in upper('&&BELLID_R6_SCHEMA.');
---- all_constraints
drop table &&BELLID_R6_APP_USER..all_constraints;
create table &&BELLID_R6_APP_USER..all_constraints_temp as
select OWNER,CONSTRAINT_NAME,CONSTRAINT_TYPE,TABLE_NAME,'x' as SEARCH_CONDITION,R_OWNER,R_CONSTRAINT_NAME ,DELETE_RULE,
STATUS,DEFERRABLE,DEFERRED , VALIDATED , GENERATED,BAD, RELY,LAST_CHANGE ,INDEX_OWNER, INDEX_NAME , INVALID ,VIEW_RELATED
from sys.all_constraints k where owner = upper('&&BELLID_R6_SCHEMA.');
---- all_tab_columns
drop table  &&BELLID_R6_APP_USER..all_tab_columns;
create table &&BELLID_R6_APP_USER..all_tab_columns_temp as
select OWNER,TABLE_NAME,COLUMN_NAME,DATA_TYPE,DATA_TYPE_MOD,DATA_TYPE_OWNER,DATA_LENGTH,DATA_PRECISION,DATA_SCALE,NULLABLE,
COLUMN_ID,DEFAULT_LENGTH,'x' as DATA_DEFAULT,NUM_DISTINCT,LOW_VALUE,HIGH_VALUE,	DENSITY,	NUM_NULLS,
NUM_BUCKETS,LAST_ANALYZED,SAMPLE_SIZE,CHARACTER_SET_NAME,CHAR_COL_DECL_LENGTH ,GLOBAL_STATS ,USER_STATS ,AVG_COL_LEN ,
CHAR_LENGTH ,CHAR_USED ,V80_FMT_IMAGE ,DATA_UPGRADED ,HISTOGRAM from sys.all_tab_columns t where owner = upper('&&BELLID_R6_SCHEMA.');
---- all_objects
drop table  &&BELLID_R6_APP_USER..all_tab_columns;
create table &&BELLID_R6_APP_USER..all_objects_temp as
select OWNER,OBJECT_NAME ,'x' as SUBOBJECT_NAME,OBJECT_ID,DATA_OBJECT_ID,OBJECT_TYPE,CREATED,LAST_DDL_TIME,TIMESTAMP,STATUS,
TEMPORARY,GENERATED,SECONDARY from sys.all_objects t where owner = upper('&&BELLID_R6_SCHEMA.');

create table &&BELLID_R6_APP_USER..all_cons_columns as
select * from &&BELLID_R6_APP_USER..all_cons_columns_temp;
drop table &&BELLID_R6_APP_USER..all_cons_columns_temp;

create table &&BELLID_R6_APP_USER..all_constraints as
select * from &&BELLID_R6_APP_USER..all_constraints_temp;
drop table &&BELLID_R6_APP_USER..all_constraints_temp;

create table &&BELLID_R6_APP_USER..all_tab_columns as
select * from &&BELLID_R6_APP_USER..all_tab_columns_temp;
drop table &&BELLID_R6_APP_USER..all_tab_columns_temp;

create table &&BELLID_R6_APP_USER..all_objects as
select * from &&BELLID_R6_APP_USER..all_objects_temp;
drop table &&BELLID_R6_APP_USER..all_objects_temp;

create index &&BELLID_R6_APP_USER..idx_temp_cols_table_name on &&BELLID_R6_APP_USER..all_cons_columns (table_name,constraint_name);
create index &&BELLID_R6_APP_USER..idx_temp_cons_table_name on &&BELLID_R6_APP_USER..ALL_CONSTRAINTS (table_name,constraint_name);
create index &&BELLID_R6_APP_USER..idx_temp_alltabcols_name on &&BELLID_R6_APP_USER..all_tab_columns (owner,table_name,column_name);
create index &&BELLID_R6_APP_USER..idx_temp_allobj1 on &&BELLID_R6_APP_USER..all_objects (owner,object_name ,object_type );
create index &&BELLID_R6_APP_USER..idx_temp_allobj2 on &&BELLID_R6_APP_USER..all_objects (object_type );

ALTER TABLE &&BELLID_R6_APP_USER..all_cons_columns CACHE;
ALTER TABLE &&BELLID_R6_APP_USER..ALL_CONSTRAINTS CACHE;
ALTER TABLE &&BELLID_R6_APP_USER..all_tab_columns CACHE;
ALTER TABLE &&BELLID_R6_APP_USER..all_objects CACHE;
exit;
