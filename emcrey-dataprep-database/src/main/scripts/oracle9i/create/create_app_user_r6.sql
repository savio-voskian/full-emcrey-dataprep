--===================================================================================
--create_app_user_r6.sql
--Description     : Script to create the user and synonyms the ro run Release 6 database 
--
--Version date    : February 26, 2009
--
--===================================================================================
--                               --(c) 2009 Bell ID B.V.
--===================================================================================



SPOOL &&logdrive/create_app_user_r6.log;
PROMPT ===========================================================================================;
PROMPT CREATE      &&BELLID_R6_APP_USER     USER;
PROMPT ===========================================================================================;


create user &&BELLID_R6_APP_USER.
  identified by "&&BELLID_R6_APP_USER_PASSWORD."
  default tablespace &&tbspace_data
  temporary tablespace &&tbspace_temp
  profile DEFAULT
  quota unlimited on &&tbspace_data
  quota unlimited on &&tbspace_index;

grant connect to &&BELLID_R6_APP_USER. ;
grant create session to &&BELLID_R6_APP_USER. ;

PROMPT ===========================================================================================;
PROMPT CREATE      &&BELLID_R6_MI_USER     USER;
PROMPT ===========================================================================================;

create user &&BELLID_R6_MI_USER.
  identified by "&&BELLID_R6_MI_USER_PASSWORD."
  default tablespace &&tbspace_data
  temporary tablespace &&tbspace_temp
  profile DEFAULT
  quota unlimited on &&tbspace_data
  quota unlimited on &&tbspace_index;

grant connect to &&BELLID_R6_MI_USER. ;
grant create session to &&BELLID_R6_MI_USER. ;

PROMPT ===========================================================================================;
PROMPT CREATE   synonyms   for  &&BELLID_R6_APP_USER     USER  AND GRANT &&RUN_BELLID_R6_SCHEMA ROLE;
PROMPT ===========================================================================================;


declare mysql varchar2(2000);
begin 
for c in (select * from all_objects where owner = upper( '&&BELLID_R6_SCHEMA.') and object_type in ('SEQUENCE','TABLE','VIEW','FUNCTION','PROCEDURE','PACKAGE'))
loop
   mysql:= 'CREATE OR REPLACE SYNONYM &&BELLID_R6_APP_USER..' || c.object_name || ' for &&BELLID_R6_SCHEMA..' || c.object_name; 
   execute immediate mysql;
   mysql:= 'CREATE OR REPLACE SYNONYM &&BELLID_R6_MI_USER..' || c.object_name || ' for &&BELLID_R6_SCHEMA..' || c.object_name; 
   execute immediate mysql;
end loop;
end;
/

SPOOL &&logdrive/create_role_run_r6.log;
PROMPT ===========================================================================================;
PROMPT CREATE     &&RUN_BELLID_R6_SCHEMA     ROLE ;
PROMPT ===========================================================================================;

CREATE ROLE &&RUN_BELLID_R6_SCHEMA;

PROMPT ===========================================================================================;
PROMPT CREATE     &&READ_BELLID_R6_MI     ROLE ;
PROMPT ===========================================================================================;
declare mysql varchar2(2000);
begin 
 IF ('&&CREATE_MI_ROLE' = 'Y') THEN
   MYSQL:= 'CREATE ROLE &&READ_BELLID_R6_MI';
   execute immediate mysql;
 END IF;
end;
/

declare mysql varchar2(2000);
begin 
for c in (select * from all_objects where owner = upper( '&&BELLID_R6_SCHEMA.') and object_type in ('SEQUENCE','TABLE','VIEW','FUNCTION','PROCEDURE','PACKAGE'))
loop
   
   IF (C.object_type = 'SEQUENCE') THEN 
      MYSQL:= 'GRANT SELECT  ON  &&BELLID_R6_SCHEMA..'|| c.object_name  ||' TO &&RUN_BELLID_R6_SCHEMA';
   ELSIF (C.object_type IN ('FUNCTION','PROCEDURE','PACKAGE')) THEN 
      MYSQL:= 'GRANT EXECUTE ON &&BELLID_R6_SCHEMA..'|| c.object_name  ||' TO &&RUN_BELLID_R6_SCHEMA';
   ELSE
      MYSQL:= 'GRANT SELECT, INSERT, UPDATE, DELETE ON  &&BELLID_R6_SCHEMA..'|| c.object_name  || ' TO &&RUN_BELLID_R6_SCHEMA';
   END IF;
   execute immediate mysql;
   IF (C.object_type NOT IN ( 'SEQUENCE','FUNCTION','PROCEDURE','PACKAGE') AND '&&CREATE_MI_ROLE' = 'Y') THEN
      MYSQL:= 'GRANT SELECT ON &&BELLID_R6_SCHEMA..'|| c.object_name  || ' TO &&READ_BELLID_R6_MI';
      execute immediate mysql;
   END IF;
end loop;
end;
/


PROMPT ===========================================================================================;
PROMPT GRANT   &&RUN_BELLID_R6_SCHEMA ROLE to   &&BELLID_R6_APP_USER ;
PROMPT ===========================================================================================;

GRANT &&RUN_BELLID_R6_SCHEMA TO &&BELLID_R6_APP_USER.;

PROMPT ===========================================================================================;
PROMPT GRANT   &&READ_BELLID_R6_MI ROLE to   &&BELLID_R6_MI_USER ;
PROMPT ===========================================================================================;
declare mysql varchar2(2000);
begin 
 IF ('&&CREATE_MI_ROLE' = 'Y') THEN
   MYSQL:= 'GRANT &&READ_BELLID_R6_MI TO &&BELLID_R6_MI_USER';
   execute immediate mysql;
 END IF;
end;
/
SPOOL OFF;
