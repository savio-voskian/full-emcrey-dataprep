
prompt
prompt Adding KMS Tables
prompt ===================================================
prompt

prompt
prompt Create table KMS_CHAINING
prompt ===================================================
prompt
create table KMS_CHAINING (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_IODIRECTION
prompt ===================================================
prompt
create table KMS_IODIRECTION (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEY_ALGORITHM
prompt ===================================================
prompt
create table KMS_KEY_ALGORITHM (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEY_ROLE
prompt ===================================================
prompt
create table KMS_KEY_ROLE (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEY_TYPE
prompt ===================================================
prompt
create table KMS_KEY_TYPE (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEY_SOURCE
prompt ===================================================
prompt
create table KMS_KEY_SOURCE (
        ID number(2,0) not null,
        NAME varchar2(50 char) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_HISTORYLOG
prompt ===================================================
prompt
create table KMS_HISTORYLOG (
        ID number(10,0) not null,
        DT_ACTION timestamp not null,
        OPERATOR varchar2(100 char) not null,
        INFORMATION varchar2(400 char) not null,
        ACTION varchar2(100 char) not null,
        MAC_VALUE raw(32),
        IND_SUCCESSFUL number(1,0) not null,
        IND_VALID number(1,0),
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_IOKEYPROFILE
prompt ===================================================
prompt
create table KMS_IOKEYPROFILE (
        ID number(10,0) not null,
        ID_IOKEYSETPROFILE number(10,0) not null,
        ID_KEYPROFILE number(10,0) not null,
        EXTERNAL_NAME varchar2(50 char),
        AID raw(255),
        ADDITIONAL_INFO varchar2(255 char),
        KEY_INDEX varchar2(255 char),
        CHAINING number(2,0),
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_IOKEYSETPROFILE
prompt ===================================================
prompt
create table KMS_IOKEYSETPROFILE (
        ID number(10,0) not null,
        ID_EXCH_KEYPROFILE number(10,0),
        ID_IOPROCEDURE number(10,0) not null,
        ID_IOSITE number(10,0) not null,
        primary key (ID) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_IOPROCEDURE
prompt ===================================================
prompt
create table KMS_IOPROCEDURE (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        IND_PINREQURED number(1,0) default 0,
        IMPLEMENTATION varchar2(255 char) not null,
        DIRECTION number(2,0) not null,
        IMPLEMENTATION_AVAILABLE number(1) default 1,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_IOPROCEDURE_APPLICABILITY
prompt ===================================================
prompt
create table KMS_IOPROCEDURE_APPLICABILITY (
        ID number(10,0) not null,
        ID_IOPROCEDURE number(10,0) not null,
        KEY_TYPE number(2,0) not null,
        KEY_ALGORITHM number(2,0) not null,
        KEY_ROLE number(2,0) not null,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (ID_IOPROCEDURE, KEY_TYPE, KEY_ALGORITHM, KEY_ROLE) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_IOSITE
prompt ===================================================
prompt
create table KMS_IOSITE (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        ID_DFLT_EXCH_KEYPROFILE number(10,0),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEY
prompt ===================================================
prompt
create table KMS_KEY (
        ID number(10,0) not null,
        KEY_TYPE number(2,0) not null,
        NAME varchar2(50 char) not null,
        VERSION number(10,0) default -1 not null,
        DESCRIPTION varchar2(400 char),
        ID_KEYSET number(10,0),
        VARIANT varchar2(255 char),
        VALUE raw (2000),
        AID raw(255),
        ADDITIONAL_INFO varchar2(255 char),
        KEY_INDEX varchar2(255 char),
        LENGTH number(5,0) not null,
        KEY_ALGORITHM number(2,0) not null,
        KEY_ROLE number(2,0) not null,
        KEY_SOURCE number(2,0) not null,
        IND_DECRYPT number(1,0),
        IND_DERIVE number(1,0),
        IND_ENCRYPT number(1,0),
        IND_EXTRACTABLE number(1,0),
        IND_MODIFIABLE number(1,0),
        IND_PERMANENT number(1,0),
        IND_PRIVATE number(1,0),
        IND_SENSITIVE number(1,0),
        IND_SIGN number(1,0),
        IND_SIGNRECOVER number(1,0),
        IND_UNWRAP number(1,0),
        IND_VERIFY number(1,0),
        IND_VERIFYRECOVER number(1,0),
        IND_WRAP number(1,0),
        DT_INITIAL timestamp not null,           -- see NIST 800-57
        DT_ACTIVATION timestamp,                 -- see NIST 800-57
        DT_PROCESS_START timestamp,              -- see NIST 800-57
        DT_PROTECT_STOP timestamp,               -- see NIST 800-57
        DT_DEACTIVATION timestamp,               -- see NIST 800-57
        DT_DESTROY timestamp,                    -- see NIST 800-57
        DT_COMPROMISE_OCCURENCE timestamp,       -- see NIST 800-57
        DT_COMPROMISE timestamp,                 -- see NIST 800-57
        ID_KEYPROFILE number(10,0) not null,
        ID_STORAGEKEY number(10,0),
        ID_KEYTYPECODE number(3,0),
        CHAINING number(2,0),
        KCV raw(32),
        CERTIFICATE_REQUEST raw(2000),
        TRACKING_NUMBER varchar2(255),
        ID_ISSUERCERTIFICATE number(10,0),
        IE_COMPONENTS raw(500),                  -- ASN.1 structure
        CURVE_NAME varchar2(32),
            ID_SIGNERPUBLICKEY number(10,0),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME, VERSION) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEYPROFILE
prompt ===================================================
prompt
create table KMS_KEYPROFILE (
        ID number(10,0) not null,
        KEY_TYPE number(2,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        ID_KEYSETPROFILE number(10,0),
        VARIANT varchar2(255 char),
        IND_MULTIPLE_KEY number(1,0) default 0 not null,
        LENGTH number(6,0) not null,
        KEY_ALGORITHM number(2,0) not null,
        KEY_ROLE number(2,0) not null,
        KEY_SOURCE number(2,0) not null,
        PUBLIC_EXPONENT number(10,0),
        DT_ACTIVATION timestamp,
        DT_PROCESS_START timestamp,
        DT_PROTECT_STOP timestamp,
        DT_DEACTIVATION timestamp,
        IND_DECRYPT number(1,0),
        IND_DERIVE number(1,0),
        IND_ENCRYPT number(1,0),
        IND_EXTRACTABLE number(1,0),
        IND_MODIFIABLE number(1,0),
        IND_PERMANENT number(1,0),
        IND_PRIVATE number(1,0),
        IND_SENSITIVE number(1,0),
        IND_SIGN number(1,0),
        IND_SIGNRECOVER number(1,0),
        IND_UNWRAP number(1,0),
        IND_VERIFY number(1,0),
        IND_VERIFYRECOVER number(1,0),
        IND_WRAP number(1,0),
        ID_STORAGEKEYPROFILE number(10,0),
        ID_KEYTYPECODE number(3,0),
        DFLT_CHAINING number(2,0) default 1,
        ID_ORIGINALKEYPROFILE number(10,0),
        ID_MASTERKEYPROFILE number(10,0),
        ID_SOURCEKEYPROFILE number(10,0),
        ID_ISSUERCERTPROFILE number(10,0),
        NAME_GENERATOR varchar2(256 char),
        CURVE_NAME varchar2(32),
        ID_SIGNERPKPROFILE number(10,0),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEYSET
prompt ===================================================
prompt
create table KMS_KEYSET (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        VERSION number(10,0) not null,
        DESCRIPTION varchar2(400 char),
        ID_KEYSETPROFILE number(10,0) not null,
        ADDITIONAL_INFO varchar2(255 char),
        KEY_INDEX varchar2(255 char),
        DT_INITIAL timestamp not null,
        DT_ACTIVATION timestamp,
        DT_PROCESS_START timestamp,
        DT_PROTECT_STOP timestamp,
        DT_DEACTIVATION timestamp,
        DT_DESTROY timestamp,
        DT_COMPROMISE_OCCURENCE timestamp,
        DT_COMPROMISE timestamp,
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME, VERSION) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEYSETPROFILE
prompt ===================================================
prompt
create table KMS_KEYSETPROFILE (
        ID number(10,0) not null,
        NAME varchar2(50 char) not null,
        DESCRIPTION varchar2(400 char),
        DT_ACTIVATION timestamp,
        DT_PROCESS_START timestamp,
        DT_PROTECT_STOP timestamp,
        DT_DEACTIVATION timestamp,
        NAME_GENERATOR varchar2(256 char),
        primary key (ID) using index tablespace &&tbspace_index,
        unique (NAME) using index tablespace &&tbspace_index
);

prompt
prompt Create table KMS_KEY_TYPE_CODE
prompt ===================================================
prompt
create table KMS_KEY_TYPE_CODE (
        ID number(3,0) not null,
        KEYTYPECODE varchar2(5),
        NAME varchar2(255 char) not null,
        primary key (ID) USING INDEX TABLESPACE &&tbspace_index,
        unique (NAME) USING INDEX TABLESPACE &&tbspace_index
);


prompt
prompt Create view KMS_MAX_KEYSET_VERSION_VIEW
prompt ===================================================
prompt
create view KMS_MAX_KEYSET_VERSION_VIEW as
        select K.ID_KEYSETPROFILE as ID, K.ADDITIONAL_INFO, max(K.VERSION) as MAX_VERSION
        from KMS_KEYSET K
        group by K.ID_KEYSETPROFILE, K.ADDITIONAL_INFO;


prompt
prompt Create Procedure GETKEYSET
prompt ===================================================
prompt

CREATE OR REPLACE PROCEDURE GETKEYSET(KEYSETPROFILE_ID IN NUMBER,PUBLIC_KEY OUT RAW, PRIVATE_KEY OUT RAW, STORAGE_KEY_ID OUT NUMBER, CERTIFICATE_REQUEST OUT RAW) AS
	KEYSET_ID NUMBER;
  COUNTER NUMBER;
BEGIN
 COUNTER :=0;
  <<LOOPER>>
   DELETE FROM KMS_KEY k WHERE k.ID_KEYSET = (SELECT ks.ID FROM KMS_KEYSET ks	WHERE ks.ID_KEYSETPROFILE = KEYSETPROFILE_ID AND ROWNUM = 1)  AND k.KEY_TYPE = (select kt.ID FROM KMS_KEY_TYPE kt WHERE kt.NAME='Public Key') returning k.VALUE,k.ID_KEYSET,k.CERTIFICATE_REQUEST INTO PUBLIC_KEY,KEYSET_ID,CERTIFICATE_REQUEST;
   DELETE FROM KMS_KEY k WHERE k.ID_KEYSET = KEYSET_ID  AND k.KEY_TYPE = (select kt.ID FROM KMS_KEY_TYPE kt WHERE kt.NAME='Private Key') returning k.VALUE,k.ID_STORAGEKEY INTO PRIVATE_KEY,STORAGE_KEY_ID;
   DELETE FROM KMS_KEY k WHERE k.ID_KEYSET = KEYSET_ID;
	 DELETE FROM KMS_KEYSET ks WHERE ks.ID = KEYSET_ID;
   IF (KEYSET_ID is null or PRIVATE_KEY is null or PUBLIC_KEY is null) AND COUNTER<50  then 
    COUNTER :=COUNTER+1;
    GOTO LOOPER;
   END IF;
END;
/


prompt
prompt Adding KMS Foreign Key Constraints
prompt ===================================================
prompt

alter table KMS_IOKEYPROFILE add constraint FK_KMS_IOKEYPROF_CHAINING foreign key (CHAINING) references KMS_CHAINING(ID);
alter table KMS_IOKEYPROFILE add constraint FK_KMS_IOKEYPROF_ID_KEYRPOF foreign key (ID_KEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_IOKEYPROFILE add constraint FK_KMS_IOKEYPROF_ID_KEYSETPROF foreign key (ID_IOKEYSETPROFILE) references KMS_IOKEYSETPROFILE(ID);
alter table KMS_IOKEYSETPROFILE add constraint FK_KMS_IOKSPROF_ID_EXCHKPROF foreign key (ID_EXCH_KEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_IOKEYSETPROFILE add constraint FK_KMS_IOKEYSETPROF_ID_IOSITE foreign key (ID_IOSITE) references KMS_IOSITE(ID);
alter table KMS_IOKEYSETPROFILE add constraint FK_KMS_IOKEYSETPROF_ID_IOPROC foreign key (ID_IOPROCEDURE) references KMS_IOPROCEDURE(ID);
alter table KMS_IOSITE add constraint FK_KMS_IOSITE_ID_DFLTEXKPROF foreign key (ID_DFLT_EXCH_KEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_IOPROCEDURE add constraint FK_KMS_IOPROC_DIRECTION foreign key (DIRECTION) references KMS_IODIRECTION(ID);
alter table KMS_IOPROCEDURE_APPLICABILITY add constraint FK_KMS_IOPROC_APPLIC_IOPROC foreign key (ID_IOPROCEDURE) references KMS_IOPROCEDURE(ID);
alter table KMS_IOPROCEDURE_APPLICABILITY add constraint FK_KMS_IOPROC_APPLIC_KEY_ROLE foreign key (KEY_ROLE) references KMS_KEY_ROLE(ID);
alter table KMS_IOPROCEDURE_APPLICABILITY add constraint FK_KMS_IOPROC_APPLIC_KEY_TYPE foreign key (KEY_TYPE) references KMS_KEY_TYPE(ID);
alter table KMS_IOPROCEDURE_APPLICABILITY add constraint FK_KMS_IOPROC_APPLIC_KEY_ALG foreign key (KEY_ALGORITHM) references KMS_KEY_ALGORITHM(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_ID_KEYSET foreign key (ID_KEYSET) references KMS_KEYSET(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_ID_KEYPROFILE foreign key (ID_KEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_ID_ISSUERCERT foreign key (ID_ISSUERCERTIFICATE) references KMS_KEY(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_ID_STORAGEKEY foreign key (ID_STORAGEKEY) references KMS_KEY(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_CHAINING foreign key (CHAINING) references KMS_CHAINING(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_KEY_SORUCE foreign key (KEY_SOURCE) references KMS_KEY_SOURCE(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_KEY_ROLE foreign key (KEY_ROLE) references KMS_KEY_ROLE(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_KEY_TYPE foreign key (KEY_TYPE) references KMS_KEY_TYPE(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_KEY_ALGORITHM foreign key (KEY_ALGORITHM) references KMS_KEY_ALGORITHM(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_ID_SIGNERPK foreign key (ID_SIGNERPUBLICKEY) references KMS_KEY(ID);
alter table KMS_KEY add constraint FK_KMS_KEY_ID_KEYTYPECODE foreign key (ID_KEYTYPECODE) references KMS_KEY_TYPE_CODE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_ISSUERCRTPR foreign key (ID_ISSUERCERTPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_STORAGEKEYPR foreign key (ID_STORAGEKEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_MASTERKERYPR foreign key (ID_MASTERKEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_KEYSETPR foreign key (ID_KEYSETPROFILE) references KMS_KEYSETPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_ORIGKEYPR foreign key (ID_ORIGINALKEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_SRCKEYPR foreign key (ID_SOURCEKEYPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_DFLT_CHAINING foreign key (DFLT_CHAINING) references KMS_CHAINING(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_KEY_SORUCE foreign key (KEY_SOURCE) references KMS_KEY_SOURCE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_KEY_ROLE foreign key (KEY_ROLE) references KMS_KEY_ROLE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_KEY_TYPE foreign key (KEY_TYPE) references KMS_KEY_TYPE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_KEY_ALGORITHM foreign key (KEY_ALGORITHM) references KMS_KEY_ALGORITHM(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_SIGKEYPR foreign key (ID_SIGNERPKPROFILE) references KMS_KEYPROFILE(ID);
alter table KMS_KEYPROFILE add constraint FK_KMS_KEYPR_ID_KEYTYPECODE foreign key (ID_KEYTYPECODE) references KMS_KEY_TYPE_CODE(ID);
alter table KMS_KEYSET add constraint FK_KMS_KEYSET_ID_KEYSETPROF foreign key (ID_KEYSETPROFILE) references KMS_KEYSETPROFILE(ID);


create index ix_kms_historylog_actionDate on KMS_HISTORYLOG (DT_ACTION) tablespace &&tbspace_index;
create index ix_kms_historylog_mac on KMS_HISTORYLOG(MAC_VALUE) tablespace &&tbspace_index;
create index ix_kms_iokp_additonalInfo on KMS_IOKEYPROFILE (ADDITIONAL_INFO) tablespace &&tbspace_index;
create index ix_kms_iokp_keyIndex on KMS_IOKEYPROFILE (KEY_INDEX) tablespace &&tbspace_index;
create index ix_kms_iokp_aid on KMS_IOKEYPROFILE (AID) tablespace &&tbspace_index;
create index ix_kms_iopa_iop on KMS_IOPROCEDURE_APPLICABILITY (ID_IOPROCEDURE) tablespace &&tbspace_index;
create index ix_kms_k_name on KMS_KEY (NAME) tablespace &&tbspace_index;
create index ix_kms_k_deactivationDate on KMS_KEY (DT_DEACTIVATION) tablespace &&tbspace_index;
create index ix_kms_k_cmprmsOccurenceDate on KMS_KEY (DT_COMPROMISE_OCCURENCE) tablespace &&tbspace_index;
create index ix_kms_k_compromiseDate on KMS_KEY (DT_COMPROMISE) tablespace &&tbspace_index;
create index ix_kms_k_additonalInfo on KMS_KEY (ADDITIONAL_INFO) tablespace &&tbspace_index;
create index ix_kms_k_keyIndex on KMS_KEY (KEY_INDEX) tablespace &&tbspace_index;
create index ix_kms_k_aid on KMS_KEY (AID) tablespace &&tbspace_index;
create index ix_kms_k_initialDate on KMS_KEY (DT_INITIAL) tablespace &&tbspace_index;
create index ix_kms_k_destroyDate on KMS_KEY (DT_DESTROY) tablespace &&tbspace_index;
create index ix_kms_k_processStopDate on KMS_KEY (DT_PROTECT_STOP) tablespace &&tbspace_index;
create index ix_kms_k_processStartDate on KMS_KEY (DT_PROCESS_START) tablespace &&tbspace_index;
create index ix_kms_k_activationDate on KMS_KEY (DT_ACTIVATION) tablespace &&tbspace_index;
create index ix_kms_kp_activationDate on KMS_KEYPROFILE (DT_ACTIVATION) tablespace &&tbspace_index;
create index ix_kms_kp_deactivationDate on KMS_KEYPROFILE (DT_DEACTIVATION) tablespace &&tbspace_index;
create index ix_kms_kp_processStartDate on KMS_KEYPROFILE (DT_PROCESS_START) tablespace &&tbspace_index;
create index ix_kms_kp_processStopDate on KMS_KEYPROFILE (DT_PROTECT_STOP) tablespace &&tbspace_index;
create index ix_kms_ks_name on KMS_KEYSET (NAME) tablespace &&tbspace_index;
create index ix_kms_ks_additonalInfo on KMS_KEYSET (ADDITIONAL_INFO) tablespace &&tbspace_index;
create index ix_kms_ks_keyIndex on KMS_KEYSET (KEY_INDEX) tablespace &&tbspace_index;
create index ix_kms_ks_initialDate on KMS_KEYSET (DT_INITIAL) tablespace &&tbspace_index;
create index ix_kms_ks_compromiseDate on KMS_KEYSET (DT_COMPROMISE) tablespace &&tbspace_index;
create index ix_kms_ks_processStopDate on KMS_KEYSET (DT_PROTECT_STOP) tablespace &&tbspace_index;
create index ix_kms_ks_processStartDate on KMS_KEYSET (DT_PROCESS_START) tablespace &&tbspace_index;
create index ix_kms_ks_destroyDate on KMS_KEYSET (DT_DESTROY) tablespace &&tbspace_index;
create index ix_kms_ks_cmprmsOccurenceDate on KMS_KEYSET (DT_COMPROMISE_OCCURENCE) tablespace &&tbspace_index;
create index ix_kms_ks_deactivationDate on KMS_KEYSET (DT_DEACTIVATION) tablespace &&tbspace_index;
create index ix_kms_ks_activationDate on KMS_KEYSET (DT_ACTIVATION) tablespace &&tbspace_index;
create index ix_kms_ksp_activationDate on KMS_KEYSETPROFILE (DT_ACTIVATION) tablespace &&tbspace_index;
create index ix_kms_ksp_deactivationDate on KMS_KEYSETPROFILE (DT_DEACTIVATION) tablespace &&tbspace_index;
create index ix_kms_ksp_processStartDate on KMS_KEYSETPROFILE (DT_PROCESS_START) tablespace &&tbspace_index;
create index ix_kms_ksp_processStopDate on KMS_KEYSETPROFILE (DT_PROTECT_STOP) tablespace &&tbspace_index;

create index IXFK_KMS_IOKEYPROF_ID_KEYRPOF on KMS_IOKEYPROFILE ( ID_KEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOKEYPROF_ID_KEYSETPR on KMS_IOKEYPROFILE ( ID_IOKEYSETPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOKEYPROF_CHAINING on KMS_IOKEYPROFILE ( CHAINING ) tablespace &&tbspace_index;
create index IXFK_KMS_IOKEYSETPROF_ID_IOPRO on KMS_IOKEYSETPROFILE ( ID_IOPROCEDURE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOKEYSETPROF_ID_IOSIT on KMS_IOKEYSETPROFILE ( ID_IOSITE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOKSPROF_ID_EXCHKPROF on KMS_IOKEYSETPROFILE ( ID_EXCH_KEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOPROC_DIRECTION on KMS_IOPROCEDURE ( DIRECTION ) tablespace &&tbspace_index;
create index IXFK_KMS_IOPROC_APPLIC_KEY_TYP on KMS_IOPROCEDURE_APPLICABILITY ( KEY_TYPE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOPROC_APPLIC_KEY_ROL on KMS_IOPROCEDURE_APPLICABILITY ( KEY_ROLE ) tablespace &&tbspace_index;
create index IXFK_KMS_IOPROC_APPLIC_KEY_ALG on KMS_IOPROCEDURE_APPLICABILITY ( KEY_ALGORITHM ) tablespace &&tbspace_index;
create index IXFK_KMS_IOSITE_ID_DFLTEXKPROF on KMS_IOSITE ( ID_DFLT_EXCH_KEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_CHAINING on KMS_KEY ( CHAINING ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_KEY_ALGORITHM on KMS_KEY ( KEY_ALGORITHM ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_ID_ISSUERCERT on KMS_KEY ( ID_ISSUERCERTIFICATE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_ID_SIGNERPKP on KMS_KEY (ID_SIGNERPUBLICKEY) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_KEY_TYPE on KMS_KEY ( KEY_TYPE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_KEY_SORUCE on KMS_KEY ( KEY_SOURCE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_ID_KEYSET on KMS_KEY ( ID_KEYSET ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_ID_STORAGEKEY on KMS_KEY ( ID_STORAGEKEY ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_KEY_ROLE on KMS_KEY ( KEY_ROLE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_ID_KEYPROFILE on KMS_KEY ( ID_KEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEY_ID_KEYTYPECODE on KMS_KEY ( ID_KEYTYPECODE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_KEY_TYPE on KMS_KEYPROFILE ( KEY_TYPE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_ORIGKEYPR on KMS_KEYPROFILE ( ID_ORIGINALKEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_DFLT_CHAINING on KMS_KEYPROFILE ( DFLT_CHAINING ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_STORAGEKEYPR on KMS_KEYPROFILE ( ID_STORAGEKEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_KEY_ALGORITHM on KMS_KEYPROFILE ( KEY_ALGORITHM ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_SRCKEYPR on KMS_KEYPROFILE ( ID_SOURCEKEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_KEY_SORUCE on KMS_KEYPROFILE ( KEY_SOURCE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_KEY_ROLE on KMS_KEYPROFILE ( KEY_ROLE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_ISSUERCRTPR on KMS_KEYPROFILE ( ID_ISSUERCERTPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_MASTERKERYPR on KMS_KEYPROFILE ( ID_MASTERKEYPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_SIGNERPKPR on KMS_KEYPROFILE (ID_SIGNERPKPROFILE) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_KEYSETPR on KMS_KEYPROFILE ( ID_KEYSETPROFILE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYPR_ID_KEYTYPECODE on KMS_KEYPROFILE ( ID_KEYTYPECODE ) tablespace &&tbspace_index;
create index IXFK_KMS_KEYSET_ID_KEYSETPROF on KMS_KEYSET ( ID_KEYSETPROFILE ) tablespace &&tbspace_index;

prompt
prompt Creating KMS sequences
prompt ===================================================
prompt

create sequence KMS_IOKEYPROFILE_SEQ start with 10000 increment by 1;
create sequence KMS_IOKEYSETPROFILE_SEQ start with 10000 increment by 1;
create sequence KMS_IOPROCEDURE_SEQ start with 10000 increment by 1;
create sequence KMS_IOPROCEDURE_APPLIC_SEQ start with 10000 increment by 1;
create sequence KMS_IOSITE_SEQ start with 10000 increment by 1;
create sequence KMS_KEYPROFILE_SEQ start with 10000 increment by 1;
create sequence KMS_KEYSETPROFILE_SEQ start with 10000 increment by 1;
create sequence KMS_KEYSET_SEQ start with 10000 increment by 50;
create sequence KMS_KEY_SEQ start with 10000 increment by 50;
create sequence KMS_VERSION_SEQ start with 10000 increment by 1;
create sequence KMS_HISTORYLOG_SEQ start with 10000 increment by 50;