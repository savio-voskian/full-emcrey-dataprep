--===================================================================================
--
--Description     : Script to determine whether a solution 'flavour' is configured
--                  to run on this installation. If it is, the script named
--                     solution_flavour_xxx_yyy.sql
--                  will be run, where xxx is the module name and yyy the flavour name (e.g. ID and PIV).
--                  If the flavour is not configured, then the stub script solution_flavour_stub.sql will be run.
--
--Version date    : September 29, 2011
--
--===================================================================================
SET PAUSE OFF;
SET HEADING ON;
SET LINESIZE 300;

SET TERMOUT ON;
SET ECHO OFF;
SET FEEDBACK ON;
SET VERIFY OFF;
SET SERVEROUT ON;
WHENEVER OSERRor  EXIT
WHENEVER SQLERRor EXIT SQL.SQLCODE

DEFINE BELLID_R6_SCHEMA_PASSWORD = '';
DEFINE BELLID_R6_APP_USER_PASSWORD = '';
DEFINE BELLID_R6_MI_USER_PASSWORD = '';

DEFINE CREATE_MI_ROLE = 'Y';
prompt
prompt Running parameters file;
prompt ===========================
prompt
@@&1;

@@connect_dba_user.sql
@@override_passwords.sql;

spool &&logdrive/r6_single_flavour_insert.log;
ALTER SESSION SET CURRENT_SCHEMA = &&BELLID_R6_SCHEMA.;

BEGIN
INSERT INTO cmn_module_flavour(module_name, flavour_name, ind_active) VALUES ('&2', '&3', 1);
EXCEPTION 
WHEN OTHERS THEN
dbms_output.put_Line('The Solution flavour &2 &3 is already present!');
END;
/

COLUMN module_name_col NEW_VALUE moduleName
COLUMN flavour_name_col NEW_VALUE flavourName
COLUMN flavour_script_col NEW_VALUE flavourScript
COLUMN flavour_script_col NEW_VALUE flavourScript
 
SELECT '&2' AS module_name_col,
       '&3' AS flavour_name_col,
	   CASE
         COUNT (*)
         WHEN 0 THEN  'solution_flavour_stub'
         ELSE  'solution_flavour_' || lower('&2') || '_' || lower('&3')
       END AS flavour_script_col
FROM cmn_module_flavour
WHERE lower(module_name) = lower('&2')
AND lower(flavour_name) = lower('&3')
AND ind_active = 1;


@&flavourScript

spool off;

exit;