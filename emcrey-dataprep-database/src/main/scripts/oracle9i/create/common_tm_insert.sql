
prompt
prompt   ===================================================
prompt   C A M S
prompt   ===================================================
prompt

prompt
prompt   Insert into CAMS_C_STATE
prompt   ===================================================
prompt
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (0, 'New', 'New', 16383, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (1, 'Ready for Authorization', 'Ready for Authorization', 1033, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (2, 'High Priority', 'High Priority', 33, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (3, 'Ready for Production', 'Ready for Production', 1059, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (4, 'Failed', 'Failed', 1059, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (5, 'Ready for Check', 'Ready for Check', 9, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (6, 'Re-produce', 'Re-produce', 33, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (7, 'Do not Produce', 'Do not Produce', 1059, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (8, 'Produced', 'Produced', 35, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (9, 'In Use', 'In Use', 11, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (10, 'Under Investigation', 'Under Investigation', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (11, 'Logically Withdrawn', 'Logically Withdrawn', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (12, 'Chips Blocked', 'Chips Blocked', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (13, 'Physically Withdrawn', 'Physically Withdrawn', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (14, 'De-activated', 'De-activated', 1, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (15, 'Registered', 'Registered', 5, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (16, 'Refused', 'Refused', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (17, 'Rejected', 'Rejected', 1547, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (18, 'Cancelled', 'Cancelled', 1579, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (19, 'In Production', 'In Production', 35, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (20, 'Waiting for Input', 'Waiting for Input', 1036, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (21, 'Requested', 'Requested', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (22, 'Installed', 'Installed', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (23, 'Selectable', 'Selectable', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (24, 'Personalized', 'Personalized', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (25, 'Blocked', 'Blocked', 11, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (26, 'Locked', 'Locked', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (27, 'Logically Deleted', 'Logically Deleted', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (28, 'Physically Deleted', 'Physically Deleted', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (29, 'Active', 'Active', 12340, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (30, 'In-active', 'In-active', 12341, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (31, 'Unwanted', 'Unwanted', 64, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (32, 'Logically Blocked', 'Logically Blocked', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (33, 'Request Renewed', 'Request Renewed', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (34, 'Not In Use', 'Not In Use', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (35, 'Parked', 'Parked', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (36, 'Ready for Photo', 'Ready for Photo', 4, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (37, 'Ready for Signature', 'Ready for Signature', 4, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (38, 'Deselected', 'Deselected', 64, 0);

--ID44 Removed insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (38, 'Ready for Photo/Signature', 'Ready for Photo/Signature', 1, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (39, 'Suspension Requested', 'Suspension Requested', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (40, 'Suspension Authorized', 'Suspension Authorized', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (41, 'Suspension in Progress', 'Suspension in Progress', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (42, 'Suspended', 'Suspended', 3, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (43, 'Resumption Requested', 'Resumption Requested', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (44, 'Resumption Authorized', 'Resumption Authorized', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (45, 'Resumption in Progress', 'Resumption in Progress', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (46, 'In Progress', 'In Progress', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (47, 'Revocation Requested', 'Revocation Requested', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (48, 'Revocation Authorized', 'Revocation Authorized', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (49, 'Revocation in Progress', 'Revocation in Progress', 3, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (50, 'Revoked', 'Revoked', 11, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (51, 'Renewed', 'Renewed', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (52, 'Replicated', 'Replicated', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (54, 'Request Sent', 'Request Sent', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (55, 'Marked for Block', 'Marked for Block', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (56, 'Marked for Unblock', 'Marked for Unblock', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (57, 'Marked for Terminate', 'Marked for Terminate', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (58, 'Terminated', 'Terminated', 35, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (59, 'Lost / Stolen', 'Lost / Stolen', 1025, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (60, 'Revoked All', 'Revoked All', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (61, 'Revoked by CRL', 'Revoked by CRL', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (62, 'In Stock', 'In Stock', 513, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (63, 'Ready for Update', 'Ready for Update', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (64, 'Ready for Secure Update', 'Ready for Secure Update', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (65, 'Removed', 'Removed', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (66, 'Finished', 'Finished', 33, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (67, 'Update Request Accepted', 'Update Request Accepted', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (68, 'Update Request Rejected', 'Update Request Rejected', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (69, 'Ready for Installation', 'Ready for Installation', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (70, 'Expired', 'Expired', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (71, 'Ready for Fingerprint', 'Ready for Fingerprint', 4, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (72, 'Rekey Needed', 'Rekey Needed', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (73, 'Suspended Rekey Needed', 'Suspended Rekey Needed', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (74, 'Recertification Needed', 'Recertification Needed', 2, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (75, 'Suspended Recert Needed', 'Suspended Recert Needed', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (76, 'Accept Policy Needed', 'Accept Policy Needed', 4, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (77, 'New Password Needed', 'New Password Needed', 4, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (78, 'Ready for Issuance', 'Ready for Issuance', 1025, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (79, 'Issued', 'Issued', 1025, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (80, 'Keys Erased', 'Keys Erased', 1, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (81, 'Suspended by AR', 'Suspended by AR', 3, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (82, 'Ready for Initialization', 'Ready for Initialization', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (83, 'External', 'External', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (84, 'Extracted', 'Extracted', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (85, 'Marked for Cleanup', 'Marked for Cleanup', 4, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (86, 'Rekeyed', 'Rekeyed', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (87, 'Recertified', 'Recertified', 2, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (88, 'Partially Produced', 'Partially Produced', 35, 1);
--- Stock Management
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (172, 'Rejected stock order', 'Rejected stock order', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (173, 'Shipped', 'Shipped', 1025, 1);
-- translate-to-mysql-as: -- SELECT 'Skip inserting the second Waiting for input state' as ACTION;
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (175, 'Waiting for input', 'Waiting for input', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (176, 'Correction Order created', 'Correction order created', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (177, 'Transfer Order created', 'Transfer order created', 1024, 1);

insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (180, 'Waiting for Approval', 'Waiting for Approval', 1028, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (181, 'Waiting for Letter', 'Waiting for Letter', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (182, 'Waiting for Transfer', 'Waiting for Transfer', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (184, 'Returned', 'Returned', 1025, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (185, 'Waiting for Delivery', 'Waiting for Delivery', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (186, 'Closed', 'Closed', 1024, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (187, 'DeliveryCancelled', 'DeliveryCancelled', 1024, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (188, 'Waiting for Correction Approval', 'Waiting for Correction Approval', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (190, 'Waiting for supply Approval', 'Waiting for approval of supply order', 1024, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (191, 'Waiting for Escrow Auth (revoked)', 'Waiting for Escrow Auth (revoked)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (192, 'Waiting for Escrow Auth (rekeyed)', 'Waiting for Escrow Auth (rekeyed)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (193, 'Waiting for Escrow Auth (expired)', 'Waiting for Escrow Auth (expired)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (194, 'Escrow Approved (revoked)', 'Escrow Approved (revoked)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (195, 'Escrow Approved (rekeyed)', 'Escrow Approved (rekeyed)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (196, 'Escrow Approved (expired)', 'Escrow Approved (expired)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (197, 'Ready for Report Generation', 'Ready for Report Generation', 33, 0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (198, 'Waiting for Escrow Auth (erased)', 'Waiting for Escrow Auth (key erased)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (199, 'Escrow Approved (erased)', 'Escrow Approved (key erased)', 2050, 1);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (200, 'Ready for Dataprep', 'Ready for Dataprep',3,0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (201, 'Ready for Export','Ready for Export',3,0);
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (202,'Ready for Activation','Ready for Activation',3,1 );
insert into CAMS_C_STATE (ID, NAME, DESCRIPTION, CODE_STATE_BIT, IND_ACTIVE) values (203,'Ready for Distribution','Ready for Distribution',3,0 );

prompt
prompt   Insert into CAMS_C_TYPE_DATA
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_DATA (ID, NAME) values (1, 'Card data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (2, 'Application data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (4, 'Cardholder data');
-- organisation no longer have lifecycle: reuse 8
insert into CAMS_C_TYPE_DATA (ID, NAME) values (8, 'Organisation data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (16, 'Key data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (32, 'Card Production Order data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (64, 'Membership data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (128, 'Photo data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (256, 'Signature data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (512, 'Card in Stock data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (1024, 'Stock Order data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (2048, 'Escrow state data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (4096, 'Stock Item data');
insert into CAMS_C_TYPE_DATA (ID, NAME) values (8192, 'Stock Location data');


prompt   Insert into CAMS_C_LIFECYCLE
prompt   ===================================================
prompt
-- FIN-123: Finance related Lifecycles are moved to Finance (ID: 60, 61, 70)
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (1, 'Generic Membership Life Cycle', NULL, 64, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (2, 'Generic Card Life Cycle', NULL, 1, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (3, 'Generic Application Life Cycle', NULL, 2, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (13, 'Generic Cardholder Life Cycle', NULL, 4, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (21, 'Generic Card Production Life Cycle', NULL, 32, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (70, 'Card In Stock Order Life Cycle', 'Card In Stock for known token serial numbers', 512, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (71, 'Stock Order Life Cycle', 'Inventory of stock items', 1024, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (100, 'Generic Service Entity Life Cycle', NULL,4, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (101, 'Escrow Lifecycle', NULL,2048, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (102, 'Stock Item type Lifecycle', NULL,4096, NULL);
insert into CAMS_C_LIFECYCLE (ID, NAME, DESCRIPTION, ID_TYPE_LIFECYCLE, ID_GRAPH_LAYOUT) values (103, 'Stock location type Lifecycle', NULL,8192, NULL);

prompt
prompt   Insert into CAMS_C_TYPE_REASON
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('0','PHYSICAL_WITHDRAWAL','Physical Card Withdrawal');
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('1','LOGICAL_WITHDRAWAL','Logical Card Withdrawal');
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('2','REQUEST','Card Request');
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('3','PRODUCTION','Production');
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('4','MANUAL','Manually');
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('5','RENEWAL','Card Renewal');
insert into CAMS_C_TYPE_REASON (ID,NAME,DESCRIPTION) values ('6','SKIP_BIOMETRIC_CAPTURE','Biometric data capture skipped');

prompt
prompt   Insert into CAMS_C_REASON
prompt   ===================================================
prompt
insert into CAMS_C_REASON (ID,NAME,ID_TYPE_REASON,DESCRIPTION) values (1,'FIRST_REQUEST',2,'First Request');
insert into CAMS_C_REASON (ID, NAME, ID_TYPE_REASON, DESCRIPTION) values(22, 'LOST', 2, 'Token is lost');
insert into CAMS_C_REASON (ID, NAME, ID_TYPE_REASON, DESCRIPTION) values(23, 'STOLEN', 2, 'Token is stolen');

insert into CAMS_C_REASON (ID, NAME, ID_TYPE_REASON, DESCRIPTION) values(40, 'DEVICE_UNAVAILABLE', 6, 'Capture device unavailable');

prompt
prompt   Insert into CAMS_C_EVENT
prompt   ===================================================
prompt
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (1, 'ACTIVATE', 'Activate', 12359, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (2, 'ARM_REQ_SU', 'ARM Request Success', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (3, 'ARM_REQ_UN', 'ARM Request Failure', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (4, 'ARM_RES_SU', 'ARM Resumption Success', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (5, 'ARM_RES_UN', 'ARM Resumption Failure', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (6, 'ARM_REV_SU', 'ARM Revocation Success', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (7, 'ARM_REV_UN', 'ARM Revocation Failure', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (8, 'ARM_SUS_SU', 'ARM Suspension Success', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (9, 'ARM_SUS_UN', 'ARM Suspension Failure', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (10, 'AUTHACCEPT', 'Authorize Request', 1037, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (11, 'AUTHREJECT', 'Reject Request', 1037, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (12, 'BLOCK', 'Block', 12359, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (13, 'BL_PROC', 'Block Proc.', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (14, 'BL_TECH', 'Block Technically', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (15, 'CANCEL', 'Cancel', 1067, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (16, 'CANC_PROD', 'Do not Produce', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (17, 'CERT_REQ', 'Certificate Request', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (18, 'CHECK', 'Check', 9, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (19, 'CONNECT', 'Connect Card to Cardholder', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (20, 'CONTINUE', 'Continue', 33, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (21, 'DATACOL_SU', 'Data Collation Successful', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (22, 'DATACOL_UN', 'Data Collation Unsuccessful', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (23, 'DELETE', 'Delete Card', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (24, 'DISTR', 'Distribute', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (25, 'DISTR_KEY', 'Distribute Keys', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (26, 'INST', 'Install', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (27, 'INTF_CA_SU', 'Interface CA Successful', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (28, 'INTF_CA_UN', 'Interface CA Unsuccessful', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (29, 'KEY_ROT', 'Key Rotation', 16, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (30, 'LOST_STOLEN', 'Lost / Stolen', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (31, 'L_PRIO', 'Lower Priority', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (32, 'MAN', 'Manual State Transition', 127, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (33, 'MODIFY', 'Modify', 1032, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (34, 'PERSO', 'Personalize', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (35, 'PHOTO', 'Photo Input', 4, 1);
-- ID44 Not used any more insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (36, 'PHOTO_SIGN', 'Photo / Signature Input', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (37, 'PREP_REVOC', 'Prepare Revocation', 8, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (38, 'PROD', 'Generate Prod. Order', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (39, 'PROD_STOP', 'Stop Production', 33, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (40, 'PROD_SUC', 'Successful Production', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (41, 'PROD_UNSUC', 'Failed Production', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (42, 'RECEIVE', 'Receive Card', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (43, 'RE_CHECK', 'Retry Check', 8, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (44, 'REGISTR', 'Register', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (45, 'REJECT', 'Reject', 9, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (46, 'REJ_REVOC', 'Reject Revocation', 8, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (47, 'REM_LOG', 'Delete Logically', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (48, 'REM_PHY', 'Delete Physically', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (49, 'RENEWAL', 'Renewal', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (50, 'REPLICA', 'Replicate', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (51, 'REPROD', 'Re Produce', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (52, 'REQ_REQUES', 'Request Request', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (53, 'REQ_RESUM', 'Request Resumption', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (54, 'REQ_REVOC', 'Request Revocation', 11, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (55, 'REQ_SUSP', 'Request Suspension', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (56, 'REQ_SYNC', 'Request Synchronize', 0, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (57, 'REQUEST', 'Request', 16383, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (58, 'RE_REQUEST', 'Request Again', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (59, 'RESP_SUC', 'Response Successful', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (60, 'RESP_UNSUC', 'Response UnSuccessful', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (61, 'RES_SUC', 'Response File; Prod. Success', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (62, 'RESUME', 'Resume', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (63, 'RES_UNSUC', 'Response File; Prod. Failure', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (64, 'RETRY', 'Retry', 33, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (65, 'REVOKE', 'Revoke', 11, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (66, 'REVOKE_CRL', 'Revoke by CRL', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (67, 'R_PRIO', 'Raise Priority', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (68, 'SIGNATURE', 'Signature Input', 4, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (69, 'SUSPEND', 'Suspend', 71, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (70, 'SYNC_ACT', 'Synchronize Activate', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (71, 'SYNC_BLK', 'Synchronize Block', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (72, 'SYNC_REQ', 'Synchronize Request', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (73, 'SYNC_TERM', 'Synchronize Terminate', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (74, 'TERM', 'Terminate Card and Apps', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (75, 'UNBLOCK', 'Unblock', 71, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (76, 'UNLOCK', 'Unlock', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (77, 'UPD_CAN', 'Cancel Update', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (78, 'UPD_NOTIFY', 'Notification Received', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (79, 'UPD_REQ', 'Update Request', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (80, 'UPDS_REQ', 'Secure Update', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (81, 'UPD_SUC', 'Update Success', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (82, 'WITHDR', 'Withdraw', 517, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (83, 'ISSUE', 'Issue', 517, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (84, 'UNSUSPEND', 'Unsuspend', 7, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (85, 'SUSPEND_AR', 'Suspend by AR', 5, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (86, 'FINGERPRINT', 'Fingerprint Input', 4, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (87, 'REQ_PASSWD', 'Request Password', 4, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (88, 'CHANGE_PWD', 'Change Password', 4, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (89, 'DEACTIVATE', 'Deactivate', 4, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (90, 'MARK_CLEAN', 'Mark for Cleanup', 4, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (91, 'EXPIRE', 'Expire', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (92, 'CHANGE_DAT', 'Change Data', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (93, 'CHANGE_POL', 'Change Policy', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (94, 'RECERT_NEEDED', 'Recertification Needed', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (95, 'REKEY_NEEDED', 'Rekey Needed', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (96, 'RECERTIFY', 'Recertify', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (97, 'REKEY', 'Rekey', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (98, 'INITIALIZE', 'Initialize', 512, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (99, 'INIT_EXTRN', 'Initialize External', 512, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (100, 'INIT_FAIL', 'Initialize Failed', 512, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (101, 'EXTRACT', 'Extract', 512, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (102, 'ERASE_KEYS', 'Erase Keys', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (103, 'SETINSTALL', 'Set to Ready for Installation', 3, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (104, 'ACTIVATE_TOKEN', 'Activate Token', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (105, 'REPLACE', 'Replace Token', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (106, 'REPL_REUSE', 'Replace Reuse', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (107, 'REPL_RECERT', 'Replace Recertification', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (108, 'REPL_REKEY', 'Replace Rekey', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (109, 'RECOVER', 'Recover', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (110, 'REUSE', 'Reuse', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (111, 'EXP_REMINDER', 'Send Expire Reminder', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (112, 'UNSUSPEND_AR', 'Unsuspend by AR', 5, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (113, 'RE-ISSUE', 'Re-Issue', 1, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (114, 'DESELECT', 'De-select', 64, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (115, 'RESELECT', 'Re-select', 64, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (116, 'IMPORT_FAILED', 'Import Failed', 127, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (117, 'PROD_PARTIAL', 'Partial Production', 35, 1);

insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (195, 'CREATE_CORRECTION_ORDER', 'Order created', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (196, 'CREATE_SUPPLY_ORDER', 'Order created', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (197, 'CREATE_TRANSFER_ORDER', 'Order created', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (202, 'ORDER_REJECT', 'Reject', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (203, 'ORDER_CANCEL', 'Cancel', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (204, 'ORDER_SHIP', 'Shipped', 1024, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (205, 'ORDER_ACCEPT', 'Accept', 1024, 0);

insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (206, 'RETURN', 'Return', 1025, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (207, 'Close', 'Close', 1024, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (208, 'LOST', 'Lost', 1024, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (209, 'ORDER_CORRECT', 'Correction', 1024, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (210, 'ORDER_CLEANUP', 'Cleanup', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (211, 'BACKORDER', 'Back Order', 1024, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (212, 'ORDER_CORRECT_CREATE', 'Correction Order created', 1024, 0);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (215, 'ORDER_PRINT_LETTER', 'Print Letter', 1024, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (216, 'WAIT_ESCROW_AUTH', 'Wait for Escrow Authorization', 2, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (217, 'APPROVE', 'Approve', 1026, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (218, 'REPORT_GEN_SUC', 'Report Generation Successful', 33, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (219, 'REPORT_GEN_UNSUC', 'Report Generation Unsuccessful', 33, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (220, 'PROD_CONFIRM', 'User confirms the card Production', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (221, 'PROD_RETRY', 'The card Production needs to be retried', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (222, 'PROD_CANCEL', 'User cancels the card Production', 35, 1);
insert into CAMS_C_EVENT (ID, NAME, DESCRIPTION, CODE_EVENT_BIT, IND_READONLY) values (223, 'REQUEST_PERMIT', 'Token request is possible', 4, 1);



prompt
prompt   Insert into CAMS_C_TYPE_NUMBER
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_NUMBER (ID,NAME) values ('1','Card Number');
insert into CAMS_C_TYPE_NUMBER (ID,NAME) values ('2','Cardholder Number');
insert into CAMS_C_TYPE_NUMBER (ID,NAME) values ('3','Registration Code');
insert into CAMS_C_TYPE_NUMBER (ID,NAME) values ('4','Card Request Number');
insert into CAMS_C_TYPE_NUMBER (ID,NAME) values ('5','Application Variable Number');


prompt
prompt   Insert into CAMS_C_TYPE_QUOTA_REDUCTION
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_QUOTA_REDUCTION (ID,NAME,DESCRIPTION) values ('0','001','Quota Management functionality is not used');
insert into CAMS_C_TYPE_QUOTA_REDUCTION (ID,NAME,DESCRIPTION) values ('1','002','Subtract card from quota at card request');
insert into CAMS_C_TYPE_QUOTA_REDUCTION (ID,NAME,DESCRIPTION) values ('2','003','Subtract card from quota at card production');
insert into CAMS_C_TYPE_QUOTA_REDUCTION (ID,NAME,DESCRIPTION) values ('3','004','Subtract card from quota at card distribution');

prompt
prompt   Insert into CAMS_C_TYPE_ADDON_FILE
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values(1,'Stock Item (De-)Activate','Stock Item (De-)Activate (class)',408,'com.bellid.cams.services.event.EventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values(2,'Stock Location (De-)Activate','Stock Location (De-)Activate (class)',410,'com.bellid.cams.services.event.EventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (27, '001', 'Card Event Handler (class)', 17, 'com.bellid.cams.event.CardEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (28, '002', 'Application Event Handler (class)', 6, 'com.bellid.cams.event.ApplicationEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (29, '029', 'Card InStock Event Handler (class)', 6, 'com.bellid.cams.event.CardInStockEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (31, '003', 'Certificate Policy (class)', NULL, 'com.bellid.cams.pki.CertificateRequestPolicy');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (32, '004', 'CardLayout (XML)', 32, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (33, '005', 'Cardholder Data Provider (class)', 95, 'com.bellid.cams.data.CardholderDataProvider');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (34, '006', 'GlobalPlatform Application Perso Container', 32, 'com.bellid.cams.cardos.CommandGenerator');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (35, '007', 'Multos Application Perso Container', 32, 'com.bellid.cams.cardos.CommandGenerator');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (36, '008', 'PrintPerso Handler (class)', 17, 'com.bellid.cams.printperso.PrintPersoHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (37, '009', 'Application Production Command Generator (class)', 6, 'com.bellid.cams.cardos.CommandGenerator');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (38, '010', 'VSDC Application Template', 90, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (39, '011', 'XSL Stylesheet', NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (40, '012', 'Application Activation Command Generator (class)', 6, 'com.bellid.cams.cardos.CommandGenerator');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (41, '013', 'Project Package Name', NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (42, '014', 'Organization Event Handler (class)', 14, 'com.bellid.cams.event.OrganizationEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (43, '015', 'Card Production Order Event Handler (class)', 91, 'com.bellid.cams.event.CardProductionOrderEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (45, '017', 'Notification Service Data Serializer (class)', 202, 'com.bellid.cams.services.notification.serializer.Serializer');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (46, '018', 'Notification Service Transport Provider (class)', 202, 'com.bellid.cams.services.notification.transport.Transport');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (48, '020', 'Multos Enablement Data File', NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (49, '021', 'Multos Application Registration Data File', NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (50, '022', 'Multos Load Certificate File', NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (51, '023', 'Cardholder Event Handler (class)', 95, 'com.bellid.cams.event.CardholderEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (52, '024', 'Membership Event Handler (class)', 31, 'com.bellid.cams.event.MemebershipEventHandler');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (57, '030', 'Card Batch Import Parser (class)', 85, 'com.bellid.cams.data.CardBatchImportDataProvider');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (58, 'Application Perso Script', 'Application Perso Script', 6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (59, 'SERVICE_ENTITY_EVENT_HANDLER', 'Event Handler for ServiceEntity', NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values(63, 'X509 Certificate', 'X509 certificate(s)', 6, 'xxx');
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (66, 'CAP File', 'CAP File', 6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (69, 'PKI CA Configuration', 'PKI CA Configuration', 31, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (70, 'PKI Certificate Profile', 'PKI Certificate Profile', 6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (72, 'Card Perso Script', 'Card Perso Script', 32, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (73, 'CardInStock Perso Script', 'CardInStock Perso Script', 502, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (74, 'MULTOS ADC/ADR', 'MULTOS Application Delete Certificate or ADC response', 6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (75, 'MULTOS ALC/ALR', 'MULTOS Application Load Certificate or ALC response',   6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (76, 'MULTOS ALT', 'MULTOS Application Load Template',                          6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (77, 'MULTOS ALU/AUR', 'MULTOS Application Load Unit or ALU response',          6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (78, 'KeyProfile names','The names of all KeyProfiles',               NULL, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (79, 'MULTOS dictionary','MULTOS TLV dictionary',                     NULL, NULL);
-- TLV Data dictionaries: Name formed as Entity.attribute.dictionary
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (100, 'Card.tlvData', 'TLV Dictionary for token', 16, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (101, 'Cardholder.tlvData', 'TLV Dictionary for tokenholder', 22, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (102, 'CardholderType.tlvData', 'TLV Dictionary for tokenholder type', 95, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (103, 'Organization.tlvData', 'TLV Dictionary for organization', 27, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (104, 'TargetGroup.tlvData', 'TLV Dictionary for target group', 31, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (105, 'Rule List', 'Rule list', 6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (106, 'CPS Mapping File','Card Personalization Specification Mapping File', 6, NULL);
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values(107, 'Card Printer type', 'Card Printer type (class)', 115, 'com.bellid.common.iis.printer.ICardPrinterTypeHandler');
--108 is ADDONFILE_IJCFILE see CAMSConstants
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values(109, 'Card Layout Server Metadata type', 'Card Layout Server Metadata type (xml)', 32, null);
-- EQABN-185
insert into CAMS_C_TYPE_ADDON_FILE (ID, NAME, DESCRIPTION, ID_TABLE_NAME, INTERFACE) values (110, 'Notification Encryption (class)', 'Notification Encryption (class)', 202, 'com.bellid.cams.services.notification.encryption.Encryption');


prompt
prompt   Insert into CAMS_ADDON_FILE
prompt   ===================================================
prompt
--insert into CAMS_ADDON_FILE (ID,NAME,ID_TYPE_ADDON_FILE,DESCRIPTION,RESRC) values(1,'StockItemActivateEventHandler',1,'Stock Item update','bean:stockItemActivateEventHandler');
--insert into CAMS_ADDON_FILE (ID,NAME,ID_TYPE_ADDON_FILE,DESCRIPTION,RESRC) values(2,'StockLocationActivateEventHandler',2,'stockLocationActivateEventHandler','bean:stockLocationActivateEventHandler');

prompt
prompt   Insert into CAMS_C_TYPE_VARIABLE
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (1, 'Boolean');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (2, 'Text');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (3, 'Number');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (4, 'DateTime');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (5, 'Password');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (6, 'Codetable');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (7, 'Binary object');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (8, 'TextAsRaw');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (9, 'Jpg');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (10, 'P10');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (11, 'Certificate');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (12, 'TLV');
insert into CAMS_C_TYPE_VARIABLE (ID, NAME) values (13, 'Date');

-- FIN-123: Removed insertions into CAMS_TYPE_CODE_TABLE: There were two insertions
--          1) the first one with id=0, was not used,and 2) the second one with id=61 was
--          related to Scripting.

prompt
prompt   Insert into CAMS_C_TYPE_APPLICATION
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_APPLICATION(ID,NAME,DESCRIPTION)  values (1,'GP_SECURITY_DOMAIN', 'Globalplatform Security domain');
insert into CAMS_C_TYPE_APPLICATION(ID,NAME,DESCRIPTION)  values (2,'GP_APPLICATION',     'Globalplatform Application');
insert into CAMS_C_TYPE_APPLICATION(ID,NAME,DESCRIPTION)  values (3,'X509CERT',           'X509 Certificate');
insert into CAMS_C_TYPE_APPLICATION(ID,NAME,DESCRIPTION)  values (4,'PROPRIETARY',        'Proprietary');
insert into CAMS_C_TYPE_APPLICATION(ID,NAME,DESCRIPTION)  values (5,'MULTOS',             'The type of an application that is designated for MULTOS smart card');
insert into CAMS_C_TYPE_APPLICATION(ID,NAME,DESCRIPTION)  values (6,'GENERIC-APPLICATION','The generic type of application');

--FIN-123: Removed insertions into CAMS_RECODE_TABLE: all insertions were related to Scripting, which
--         is not supported in Finance 6.3.0.4

prompt
prompt   Insert into CAMS_STATE_TRANSITION
prompt   ===================================================
prompt
--- FIN-123: State-Transitions related to Finance are moved to Finance project
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (1, 1, 1, 30, 29, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (2, 1, 1, 31, 29, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (3, 1, 13, 30, 29, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (4, 1, 13, 31, 29, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (5, 114,1, 29, 31, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (11, 12, 1, 29, 31, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (12, 12, 1, 30, 31, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (13, 12, 13, 29, 31, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (14, 12, 13, 30, 31, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (17, 15, 21, 2, 18, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (18, 15, 21, 3, 18, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (19, 15, 21, 19, 18, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (34, 32, 1, 29, 30, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (35, 115, 1, 38, 29, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (43, 38, 21, 3, 19, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (47, 40, 21, 19, 8, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (51, 41, 21, 3, 4, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (64, 57, 1, 0, 29, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (67, 57, 13, 0, 29, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (68, 57, 21, 0, 3, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (81, 67, 21, 3, 2, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (82, 69, 1, 29, 30, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (83, 69, 13, 29, 30, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (86, 75, 1, 31, 30, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (87, 75, 13, 31, 30, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (88, 117, 21, 19, 88, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (89, 40, 21, 88, 8, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (90, 117, 21, 88, 88, 0);

-- Generic Card Life Cycle
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (100,  57, 2,  0,  3, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (101,  38, 2,  3, 19, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (102, 221, 2, 19,  3, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (103,  15, 2,  3, 18, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (104,  41, 2, 19,  4, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (105, 221, 2,  4,  3, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (106,  15, 2,  4, 18, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (107,  40, 2, 19,  8, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (108, 221, 2,  8,  3, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (109,  83, 2,  8,  9, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (110,  30, 2,  9, 59, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (111,  69, 2,  9, 42, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (112,  84, 2, 42,  9, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (113,  82, 2,  9, 13, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (114,  82, 2,  8, 13, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (115,  82, 2, 42, 13, 0);

-- Generic Application Life Cycle
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (130,  57, 3,  0, 21, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (131,  40, 3, 21, 22, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (132,  15, 3, 21, 18, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (133,  83, 3, 22,  9, 0);

-- Service-Entity Life Cycle
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (200, 57, 100,  0, 15, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (201, 1,  100, 15, 29, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (202, 87, 100, 29, 77, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (203, 87, 100, 77, 77, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (204, 88, 100, 77, 76, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (205, 1,  100, 76, 29, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (206, 33, 100, 29, 76, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (207, 69, 100, 77, 42, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (208, 87, 100, 76, 77, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (209, 69, 100, 76, 42, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (210, 87, 100, 42, 77, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (211, 33, 100, 42, 76, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (212, 84, 100, 42, 29, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (213, 69, 100, 29, 42, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (214, 89, 100, 42, 30, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (215, 44, 100, 30, 15, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (216, 90, 100, 30, 85, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (217, 90, 100, 15, 85, 1);

insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (507, 1,  101, 0, 1, 0);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (508, 88, 101, 29, 30, 1);
insert into CAMS_STATE_TRANSITION (ID, ID_EVENT, ID_LIFECYCLE, ID_STATE_CURRENT, ID_STATE_NEW, IND_MAN_PERMIT) values (509, 1, 101, 30, 29, 1);

-- Stock order Life Cycle
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2001,195,71,0,176,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2002,196,71,0,20,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2004,197,71,0,177,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2005,217,71,177,180,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2006,207,71,176,186,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2007,10,71,180,182,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2008,11,71,180,17,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2010,204,71,182,185,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2011,15,71,182,18,1);

-- insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2013,206,71,185,184,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2014,207,71,185,186,1);
-- insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2017,217,71,184,182,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2018,15,71,177,18,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2020,15,71,185,187,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2021,217,71,20,1,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2022,11,71,1,172,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2023,10,71,1,185,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2024,217,71,172,1,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2025,15,71,172,18,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2026,211,71,185,185,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2027,208,71,185,59,0);
-- RETURN: to be discussed
-- insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2028,206,71,0,184,0);
--- Stock item lifecycle
insert into CAMS_STATE_TRANSITION(ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2041,1,102,0,29,0);
insert into CAMS_STATE_TRANSITION(ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2042,12,102,29,30,1);
insert into CAMS_STATE_TRANSITION(ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2043,1,102,30,29,1);

--- Card in stock
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2050,98,70,0,62,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2051,82,70,62,17,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(2052,100,70,62,18,1);

--prompt stock location lifecycle
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(3000,1,103,0,29,0);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(3001,12,103,29,30,1);
insert into CAMS_STATE_TRANSITION (ID,ID_EVENT,ID_LIFECYCLE,ID_STATE_CURRENT,ID_STATE_NEW,IND_MAN_PERMIT) values(3002,1,103,30,29,1);


prompt
prompt   Insert into CAMS_C_TYPE_BIOMETRIC_DATA
prompt   ===================================================
prompt
insert into CAMS_C_TYPE_BIOMETRIC_DATA (ID, NAME, DESCRIPTION, ID_CAPTURE_APPLET_DEFINITION ) values (1, 'PHOTO','Photograph',1);
insert into CAMS_C_TYPE_BIOMETRIC_DATA (ID, NAME, DESCRIPTION, ID_CAPTURE_APPLET_DEFINITION ) values (2, 'SIGNATURE','Signature',2);
insert into CAMS_C_TYPE_BIOMETRIC_DATA (ID, NAME, DESCRIPTION, ID_CAPTURE_APPLET_DEFINITION ) values (3, 'FINGERPRINT','Fingerprint',3);

prompt
prompt   Insert into CAMS_X_BIOMETRIC_EVENT
prompt   ===================================================
prompt
insert into CAMS_X_BIOMETRIC_EVENT (ID, ID_EVENT,ID_TYPE_BIOMETRIC_DATA, IND_SKIP_CAPTURE) values (1, 35,1, 0);
insert into CAMS_X_BIOMETRIC_EVENT (ID, ID_EVENT,ID_TYPE_BIOMETRIC_DATA, IND_SKIP_CAPTURE) values (2, 68,2, 0);
insert into CAMS_X_BIOMETRIC_EVENT (ID, ID_EVENT,ID_TYPE_BIOMETRIC_DATA, IND_SKIP_CAPTURE) values (3, 86,3, 0);

prompt
prompt   Insert into CAMS_C_APP_OPTIONALITY
prompt   ===================================================
prompt
insert into CAMS_C_APP_OPTIONALITY (ID,NAME) values ('0','Required');
insert into CAMS_C_APP_OPTIONALITY (ID,NAME) values ('1','Selected');
insert into CAMS_C_APP_OPTIONALITY (ID,NAME) values ('2','Optional');

prompt
prompt   Insert into CAMS_SEC_CONFIG_TYPE
prompt   ===================================================
prompt
insert into CAMS_SEC_CONFIG_TYPE (ID, NAME) values (0, 'SCP02_SECURITY_CONFIG');
insert into CAMS_SEC_CONFIG_TYPE (ID, NAME) values (1, 'SCP80_SECURITY_CONFIG');
insert into CAMS_SEC_CONFIG_TYPE (ID, NAME) values (2, 'GSM348_SECURITY_CONFIG');
insert into CAMS_SEC_CONFIG_TYPE (ID, NAME) values (3, 'SCP81_SECURITY_CONFIG');
insert into CAMS_SEC_CONFIG_TYPE (ID, NAME) values (4, 'SCP_SECURITY_CONFIG');
insert into CAMS_SEC_CONFIG_TYPE (ID, NAME) values (5, 'EMV_SECURITY_CONFIG');


prompt
prompt   Insert into CAMS_C_STOCK_ITEM_TYPE and CAMS_C_STOCK_LOCATION_TYPE
prompt   ===================================================
prompt
insert into CAMS_C_STOCK_ITEM_TYPE(ID, ID_LIFECYCLE, DESCRIPTION, NAME)
    values(1 , 102, 'Stock Item type 1', 'Stock Item type 1');

insert into CAMS_C_STOCK_LOCATION_TYPE(ID, ID_LIFECYCLE, DESCRIPTION, NAME)
    values(1, 103, 'Stock location type', 'Stock location-type');


