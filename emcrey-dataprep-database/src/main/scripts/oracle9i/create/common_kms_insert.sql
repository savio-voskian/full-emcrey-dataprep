
prompt
prompt   ===================================================
prompt   K M S
prompt   ===================================================
prompt

prompt
prompt   Insert into KMS_CHAINING
prompt   ===================================================
prompt
insert into KMS_CHAINING (ID, NAME) values (1, 'ECB');
insert into KMS_CHAINING (ID, NAME) values (2, 'CBC');

prompt
prompt   Insert into KMS_IODIRECTION
prompt   ===================================================
prompt
insert into KMS_IODIRECTION (ID, NAME) values (1, 'Import');
insert into KMS_IODIRECTION (ID, NAME) values (2, 'Export');

prompt
prompt   Insert into KMS_KEY_ALGORITHM
prompt   ===================================================
prompt
insert into KMS_KEY_ALGORITHM (ID, NAME) values (1, 'DES');
insert into KMS_KEY_ALGORITHM (ID, NAME) values (2, 'DESede');
insert into KMS_KEY_ALGORITHM (ID, NAME) values (3, 'AES');
insert into KMS_KEY_ALGORITHM (ID, NAME) values (4, 'GENERIC SECRET');
insert into KMS_KEY_ALGORITHM (ID, NAME) values (5, 'PIN');
insert into KMS_KEY_ALGORITHM (ID, NAME) values (6, 'RSA');
insert into KMS_KEY_ALGORITHM (ID, NAME) values (7, 'EC');

prompt
prompt   Insert into KMS_KEY_ROLE
prompt   ===================================================
prompt
insert into KMS_KEY_ROLE (ID, NAME) values (1, 'MSK');
insert into KMS_KEY_ROLE (ID, NAME) values (2, 'KEK');
insert into KMS_KEY_ROLE (ID, NAME) values (3, 'Exchange Key');
insert into KMS_KEY_ROLE (ID, NAME) values (4, 'Working Key');

prompt
prompt   Insert into KMS_KEY_SOURCE
prompt   ===================================================
prompt
insert into KMS_KEY_SOURCE (ID, NAME) values (1, 'New');
insert into KMS_KEY_SOURCE (ID, NAME) values (2, 'Derived');
insert into KMS_KEY_SOURCE (ID, NAME) values (3, 'Imported');
insert into KMS_KEY_SOURCE (ID, NAME) values (4, 'Copy of Existing Key');

prompt
prompt   Insert into KMS_KEY_TYPE
prompt   ===================================================
prompt
insert into KMS_KEY_TYPE (ID, NAME) values (1, 'Secret Key');
insert into KMS_KEY_TYPE (ID, NAME) values (2, 'Private Key');
insert into KMS_KEY_TYPE (ID, NAME) values (3, 'Public Key');
-- Note: The EMV Certificate value is in here rather than the Finance module because the cmn-api KeyTypeEnum depends on this value.
insert into KMS_KEY_TYPE (ID, NAME) values (4, 'EMV Certificate');
insert into KMS_KEY_TYPE (ID, NAME) values (5, 'X509 Certificate');
insert into KMS_KEY_TYPE (ID, NAME) values (6, 'GP Certificate');

prompt
prompt   Insert into KMS_KEY_TYPE_CODE
prompt   ===================================================
prompt
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (1, '000', 'ZMK - Zone Master Key (000)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (11, '001', 'ZPK- Zone PIN Encryption Key (001)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (21, '002', 'PVK - PIN Verification Key (002)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (25, '402', 'CVK - Card Verification Key (402)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (72, '107', 'KEK - Key Encryption Key (107)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (73, '207', 'KMC/CMK (207)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (8, '008', 'ZAK (008)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (92, '109', 'MK-AC - Master Key for Application Cryptograms (109)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (93, '209', 'MK-SMI - Master Key for Secure Messaging (for Integrity) (209)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (94, '309', 'MK-SMC - Master Key for Secure Messaging (for Confidentiality) (309)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (96, '509', 'MK-DN - Master Key for Dynamic Numbers (509)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (98, '709', 'MK-CVC3 - Master Key for CVC3 (Contactless) (709)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (101, '00A', 'ZEK - Zone Encryption key (00A)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (121, '00C', 'RSA-SK - RSA Private Key (00C)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (122, '10C', 'Variant HMAC (10C)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (131, '00D', 'RSA-PK - RSA Public Key (00D)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (150, 'D0', 'AES Data Encryption Key (Generic) (D0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (151, '21', 'AES Data Encryption Key (DEK) (21)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (152, '22', 'AES Data Encryption Key (ZEK) (22)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (153, '23', 'AES Data Encryption Key (TEK) (23)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (171, 'M5', 'AES CBC CMAC (M5)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (172, 'M6', 'AES CMAC (M6)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (173, 'K0', 'AES ZMK (K0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (174, 'P0', 'AES ZPK (P0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (175, '61', 'KeyBlock HMAC (61)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (176, 'M0', 'AES ZAK (M0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (177, 'E0', 'KeyBlock MK-AC (E0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (178, 'E1', 'KeyBlock MK-SMC (E1)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (179, 'E2', 'KeyBlock MK-SMI (E2)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (180, 'E3', 'KeyBlock MK-DAC (E3)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (181, 'E4', 'KeyBlock MK-DN (E4)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (182, '03', 'AES RSA-SK (03)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (183, '04', 'AES RSA-PK (04)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (184, 'B0', 'AES Base Derivation Key (BDK-1) (B0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (185, '41', 'AES Base Derivation Key (BDK-2) (41)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (186, '42', 'AES Base Derivation Key (BDK-3) (42)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (187, '43', 'AES Base Derivation Key (BDK-4) (43)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (188, 'C0', 'AES CVK - Card Verification Key (C0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (189, 'B1', 'AES IMK - Initial Key (DUKPT) (B1)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (190, '73', 'AES TKR (73)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (191, '51', 'AES TMK (51)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (192, '71', 'AES TPK (71)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (193, '01', 'AES WWK (01)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (194, 'E6', 'AES KML (E6)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (195, '32', 'AES MK-CVC3 (32)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (196, 'V0', 'AES PVK (V0)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (198, '62', 'KeyBlock HMAC_SHA-224 (62)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (199, '63', 'KeyBlock HMAC_SHA-256 (63)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (200, '64', 'KeyBlock HMAC_SHA-384 (64)');
insert into KMS_KEY_TYPE_CODE (ID, KEYTYPECODE, NAME) values (201, '65', 'KeyBlock HMAC_SHA-512 (65)');

