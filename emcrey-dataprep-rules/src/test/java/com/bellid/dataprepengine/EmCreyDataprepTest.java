package com.bellid.dataprepengine;

import com.bellid.common.services.kms.emv.ExpirationDate;
import com.bellid.common.services.kms.emv.Pan;
import com.bellid.common.services.kms.emv.Psn;
import com.bellid.dataprepengine.api.v1_0.*;
import com.bellid.jackson.objectmapper.CoreObjectMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bellid.util.StringUtilities.fromHexString;
import static org.junit.Assert.*;

public class EmCreyDataprepTest extends AbstractDataprepTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmCreyDataprepTest.class);

    private final static String PAYMENT_APP_ID = "MADA";
    private final static String PAN = "3795280000000043";
    private final static String PSN = "01";
    private final static String EXPIRY_DATE = "1226";

    @Override
    public String[] getTestDataLocations() {
        return new String[]{
                "/dbunit/cmn-core-data.xml",
                "/dbunit/cmn-blob-data.xml",
                "/dbunit/kms-core-data.xml",
                "/dbunit/cams-core-data.xml",
                "/dbunit/lifecycles.xml",

                "/local/common/core-data.xml",
                "/local/emcrey/mada-kms-config-data.xml",
                "/local/emcrey/mada-kms-test-data.xml",

                "/local/emcrey/mada-hce-config.xml",
        };
    }

    @Test
    public void testDgis() throws Exception {
        AddCardRequest addCardRequest = new AddCardRequest();
        addCardRequest.setMessageId(MESSAGE_ID);
        addCardRequest.setPan(new Pan(PAN));
        addCardRequest.setPsn(new Psn(PSN));
        addCardRequest.setPanExpiryDate(ExpirationDate.fromMMYY(EXPIRY_DATE));
        addCardRequest.setPaymentAppId(PAYMENT_APP_ID);
        addCardRequest.setBinLength(6);
        addCardRequest.setPersoData(fromHexString("8C24000000000000000000000000000000000000000000000000000000000000000000000000")); //Tag:8C Length:24 Value: 000000000000000000000000000000000000000000000000000000000000000000000000

        String s = new CoreObjectMapper().writer().writeValueAsString(addCardRequest);
        System.out.println("request: " + s);

        AddCardResponse addCardResponse = tkmApiService.prepareDataNoInsert(addCardRequest, null);
        assertNotNull(addCardResponse);
        assertEquals("000", addCardResponse.getStatus());
        System.out.println("DGIs in Hex: " + bundleToHex(addCardResponse.getDgis()));

        s = new CoreObjectMapper().writer().writeValueAsString(addCardResponse);
        System.out.println("reponse: " + s);
    }
}
