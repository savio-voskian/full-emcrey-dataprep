package com.bellid.dataprepengine;

import com.bellid.common.tlv.api.TLVFactory;
import com.bellid.common.tlv.impl.TLVFactoryImpl;
import com.bellid.dataprepengine.api.v1_0.AbstractResponse;
import com.bellid.dataprepengine.api.v1_0.DGIs;
import com.bellid.dataprepengine.services.TkmApiService;
import com.bellid.jackson.objectmapper.CoreObjectMapper;
import com.bellid.mobile.seitc.model.SeitcCard;
import com.bellid.mobile.seitc.tm.services.CryptoSupport;
import com.bellid.mobile.seitc.tm.services.SeitcCardService;
import com.bellid.test.common.kms.jce.AbstractITHarness;
import com.bellid.util.StringUtilities;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

public abstract class AbstractDataprepTest extends AbstractITHarness {

    protected static final ObjectMapper MAPPER = new CoreObjectMapper().enableDefaultTyping();

	protected static final long MESSAGE_ID = 54321;//dummy message id..

	protected final TLVFactory TLV_FACTORY = new TLVFactoryImpl();

	@Inject
    protected TkmApiService tkmApiService;
    @Inject
    private CryptoSupport cryptoSupport;
    @Inject
    protected SeitcCardService seitcCardService;
    @Inject
    private ApplicationContext applicationContext;
	
    @Before
    @Override
    public void setUpHarness() throws Exception {
        setupDatabase();
        super.setUpHarness();
    }

    @Override
    protected void setUp() throws Exception {
    }

    @Override
    protected void tearDown() throws Exception {
    }

    protected String bundleToString(DGIs dgis) throws JsonProcessingException {
    	return dgis.getDgis() == null ? null : StringUtilities.toUtf8String(dgis.getDgis());
    }

    protected String bundleToHex(DGIs dgis) throws JsonProcessingException {
        return dgis.getDgis() == null ? null : StringUtilities.toHexString(dgis.getDgis(), false);
    }

    private byte[] extractPreparedDataFromDatabase(String codeCard) {
        SeitcCard seitcCard = seitcCardService.getSeitcCardByCodeCard(codeCard);
    	return cryptoSupport.decryptChipData(seitcCard.getSeitcApplicationOnCard().getChipdataTlv(), seitcCard);
    }

    protected void assertSuccessResponse(AbstractResponse resp) {
    	assertEquals("000", resp.getStatus());
    }
}

