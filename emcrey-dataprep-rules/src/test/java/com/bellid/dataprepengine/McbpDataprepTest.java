package com.bellid.dataprepengine;

import com.bellid.common.services.kms.emv.ExpirationDate;
import com.bellid.common.services.kms.emv.Pan;
import com.bellid.common.services.kms.emv.Psn;
import com.bellid.dataprepengine.api.v1_0.AddCardRequest;
import com.bellid.dataprepengine.api.v1_0.AddCardResponse;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class McbpDataprepTest extends AbstractDataprepTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(McbpDataprepTest.class);

    private final static String PAYMENT_APP_ID = "MCBP";
    private final static String PAN = "3795280000000043";
    private final static String PSN = "01";
    private final static String EXPIRY_DATE = "1226";

    @Override
    public String[] getTestDataLocations() {
        return new String[]{
                "/dbunit/cmn-core-data.xml",
                "/dbunit/cmn-blob-data.xml",
                "/dbunit/kms-core-data.xml",
                "/dbunit/cams-core-data.xml",
                "/dbunit/lifecycles.xml",

                "/local/common/core-data.xml",
                "/local/common/kms-config-data.xml",
                "/local/common/kms-test-data.xml",
                "/local/mcbp/mcbp-kms-test-data-extension.xml",

                "/local/mcbp/mcbp-config.xml"
        };
    }

    @Test
    public void testDgis() throws Exception {
        AddCardRequest addCardRequest = new AddCardRequest();

        addCardRequest.setMessageId(MESSAGE_ID);
        addCardRequest.setPan(new Pan(PAN));
        addCardRequest.setPsn(new Psn(PSN));
        addCardRequest.setPanExpiryDate(ExpirationDate.fromMMYY(EXPIRY_DATE));
        addCardRequest.setPaymentAppId(PAYMENT_APP_ID);
        addCardRequest.setBinLength(6);

        AddCardResponse addCardResponse = tkmApiService.prepareDataNoInsert(addCardRequest, null);
        assertNotNull(addCardResponse);
        assertEquals("000", addCardResponse.getStatus());
        System.out.println("DGIs in Hex: " + bundleToHex(addCardResponse.getDgis()));

        byte[] dgis = addCardResponse.getDgis().getDgis();
        assertNotNull(dgis);
    }
}
