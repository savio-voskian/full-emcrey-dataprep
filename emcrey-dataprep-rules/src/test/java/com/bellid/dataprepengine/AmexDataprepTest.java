package com.bellid.dataprepengine;

import org.junit.Test;

import com.bellid.common.services.kms.emv.ExpirationDate;
import com.bellid.common.services.kms.emv.Pan;
import com.bellid.common.services.kms.emv.Psn;
import com.bellid.dataprepengine.api.v1_0.AddCardRequest;
import com.bellid.dataprepengine.api.v1_0.AddCardResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.bellid.util.StringUtilities.fromHexString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AmexDataprepTest extends AbstractDataprepTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AmexDataprepTest.class);

    private final static String PAYMENT_APP_ID = "AMEX_HCE";
    private final static String PAN = "3795280000000033";
    private final static String PSN = "01";
    private final static String EXPIRY_DATE = "1225";

    @Override
    public String[] getTestDataLocations() {
        return new String[]{
                "/dbunit/cmn-core-data.xml",
                "/dbunit/cmn-blob-data.xml",
                "/dbunit/kms-core-data.xml",
                "/dbunit/cams-core-data.xml",
                "/dbunit/lifecycles.xml",

                "/local/common/core-data.xml",
                "/local/common/kms-config-data.xml",
                "/local/common/kms-test-data.xml",
                "/local/amex/amex-kms-test-data-extension.xml",

                "/local/amex/amex-hce-config.xml"
        };
    }

    @Test
    public void tesDgis() throws Exception {
        AddCardRequest addCardRequest = new AddCardRequest();
        addCardRequest.setMessageId(MESSAGE_ID);
        addCardRequest.setPan(new Pan(PAN));
        addCardRequest.setPsn(new Psn(PSN));
        addCardRequest.setPanExpiryDate(ExpirationDate.fromMMYY(EXPIRY_DATE));
        addCardRequest.setPaymentAppId(PAYMENT_APP_ID);
        addCardRequest.setBinLength(6);
        //addCardRequest.setCardholderName("FamilyName/Surname");
        addCardRequest.setPersoData(fromHexString("5F201246616D696C794E616D652F5375726E616D65")); //Tag:5F20 Length:12 Value: FamilyName/Surname

        AddCardResponse addCardResponse = tkmApiService.prepareDataNoInsert(addCardRequest, null);
        assertNotNull(addCardResponse);
        assertEquals("000", addCardResponse.getStatus());
        System.out.println("DGIs in Hex: " + bundleToHex(addCardResponse.getDgis()));
    }
}
