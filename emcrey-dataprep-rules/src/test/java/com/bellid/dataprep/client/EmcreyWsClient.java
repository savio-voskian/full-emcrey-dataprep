package com.bellid.dataprep.client;

import static com.bellid.util.StringUtilities.*;

import com.bellid.dataprepengine.api.v1_0.PaymentBundle;
import com.bellid.util.StringUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.bellid.common.services.kms.emv.ExpirationDate;
import com.bellid.common.services.kms.emv.Pan;
import com.bellid.common.services.kms.emv.Psn;
import com.bellid.dataprepengine.api.v1_0.AddCardRequest;
import com.bellid.dataprepengine.api.v1_0.AddCardResponse;
import com.bellid.jackson.objectmapper.CoreObjectMapper;

public class EmcreyWsClient {
    private static final String DATAPREP_URL = "http://nrxsv-pprjemc01:99/dataprep/servlets/dataprep/";

    private static final ObjectMapper MAPPER = new CoreObjectMapper().enableDefaultTyping();

    private static final long MESSAGE_ID = 123456789;
    private final static String PAYMENT_APP_ID = "MADA";
    private final static String PAN = "4433550000000043";
    private final static String PSN = "01";
    private final static String EXPIRY_DATE = "1226";

    private static RestTemplate createRestTemplate () {
        RestTemplate result = new RestTemplate();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();

        messageConverter.setObjectMapper(new CoreObjectMapper());
        result.getMessageConverters().add(0, messageConverter);

        return result;
    }

    private static void dataprepDataRequest(RestTemplate restTemplate) throws Exception {
        AddCardRequest addCardRequest = new AddCardRequest();
        addCardRequest.setMessageId(MESSAGE_ID);
        addCardRequest.setPan(new Pan(PAN));
        addCardRequest.setPsn(new Psn(PSN));
        addCardRequest.setPanExpiryDate(ExpirationDate.fromMMYY(EXPIRY_DATE));
        addCardRequest.setPaymentAppId(PAYMENT_APP_ID);
        addCardRequest.setBinLength(6);
        System.out.println("addCardResponse.DGIs: " + addCardRequest);

        AddCardResponse addCardResponse = restTemplate.postForObject(DATAPREP_URL + "dataprep", addCardRequest, AddCardResponse.class);
        if (!addCardResponse.getStatus().equals("000")) {
            throw new Exception("ERROR: " + addCardResponse.getStatus() + ", " + addCardResponse.getStatusMsg());
        }
        //System.out.println("addCardResponse.DGIs: " + toHexString(addCardResponse.getDgis().getDgis(), false));

        //Testing against TSP Emcrey environment for mobile -> "dgis" contains bundle in place of DGIs
        System.out.println("Bundle: " + StringUtilities.toUtf8String(addCardResponse.getDgis().getDgis()));
        PaymentBundle paymentBundle = MAPPER.readValue(addCardResponse.getDgis().getDgis(), PaymentBundle.class);
        System.out.println("DGIs: " + toHexString(paymentBundle.getStaticCardData().getChipdata(), false));
    }

    public static void main(String[] args) throws Exception {

        RestTemplate restTemplate = createRestTemplate();
        dataprepDataRequest(restTemplate);
    }
}