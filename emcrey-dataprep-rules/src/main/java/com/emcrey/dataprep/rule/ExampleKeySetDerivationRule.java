package com.emcrey.dataprep.rule;

import com.bellid.cams.model.PersoBureau;
import com.bellid.common.date.AndisDate;
import com.bellid.common.services.kms.emv.Pan;
import com.bellid.common.services.kms.emv.Psn;
import com.bellid.dataprep.dao.emv.KmsEmvDAO;
import com.bellid.dataprep.emv.rule.mchip.McbpDerivedKeySet;
import com.bellid.dataprep.seitc.SeitcRule;
import com.bellid.dataprep.seitc.model.SeitcDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import static com.bellid.dataprep.DataSet.Index.*;
import static com.bellid.dataprep.model.emv.EmvDataSet.Index.*;


/**
 * 
 * Derive ICC symmetric keys (e.g. CMK_CL_UMD, CMK_RP_UMD etc).
 * A KeySet object will be derived from the IMK (Issuer Master Key) KeySet.
 */
@Service("exampleKeySetDerivationRule")
public class ExampleKeySetDerivationRule implements SeitcRule {
    private static final Logger logger = LoggerFactory.getLogger(ExampleKeySetDerivationRule.class);

    @Inject
    private KmsEmvDAO dpKmsDao;

    /**
     * To derived ICC symmetric keys.
     *
     * @param   dataset  containing various data that are needed to derive
     *                   keys.
     *
     * @return It derives the ICC symmetric keys. An instance of
     *         <code>IccDerivedKeySet</code> will represent a the derived keyset,
     *          and it stored in the dataset under 
     *           <code>EmvDataSet.MCBP_DERIVED_KEYSET</code>
     *          the provided dataset is returned unchanged.
     */
    @Override
    public SeitcDataSet process(SeitcDataSet dataset) {
        String appName = dataset.getString(APP_NAME);
        AndisDate effDt = dataset.getDate(EFFECTIVE_DATE);
        AndisDate expryDt = dataset.getDate(EXPIRATION_DATE);

        PersoBureau perso = (PersoBureau) dataset.getObject(PERSO_BUREAU);

        Pan pan       = (Pan) dataset.getObject(PAN);
        Psn psn       = (Psn) dataset.getObject(PSN);
        String keysetRef = dataset.getString(KEYSET_REF);
        McbpDerivedKeySet mcbpDerivedKeySet = dpKmsDao.getMcbpDerivedKeySet(pan,
                                                                         psn, 
                                                                         appName, 
                                                                         keysetRef, 
                                                                         effDt, 
                                                                         expryDt, 
                                                                         perso);

        dataset.update(MCBP_DERIVED_KEYSET, mcbpDerivedKeySet);
        return dataset;
    }
}