package com.emcrey.dataprep.rule;

import com.bellid.dataprep.seitc.SeitcRule;
import com.bellid.dataprep.seitc.model.SeitcDataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service("exampleRule")
public class ExampleRule implements SeitcRule {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleRule.class);

    @Override
    public SeitcDataSet process(SeitcDataSet dataset) {

        return dataset;
    }
}
